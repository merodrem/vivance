import { Platform } from 'react-native'
import { Dimensions } from 'react-native';
export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const BG_IMAGE = require('src/assets/img/background.jpg');

export type Gender = "female"|"male"|"otherGender";
export type ExerciseFilter = "new" | "short" | "top" | "official";
export const DEFAULT_EXERCISE_FILTER: ExerciseFilter = "new"
export const EXERCISE_PAGE_SIZE = 20;


// export const IAP_SKUS = Platform.select({
//     ios: [
//       '1MONTH',
//       '12MONTHS'
//     ],
//     android: [
//         '1MONTH',
//         '12MONTHS'
//       ],
//   });

export  const IAP_SUBS: string[] = Platform.select({
    ios: [
    ],
    android: [
      'annual_subscription',
      'monthly_subscription',
    ],
    default:[
      'annual_subscription',
      'monthly_subscription',
    ]
   });