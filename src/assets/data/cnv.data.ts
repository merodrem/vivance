export const CNV_SITUATIONS = [
    "work",
    "school",
    "nature",
    "home",
    "relation",
    "family",
    "fellow",
    "medias",
    "music",
    "selfTime",
    "sleep",
    "sport",
    "health",
    "food",
    "activity",
    "other",
] as const;

export const CNV_NEEDS = [
    { category: "survival", need: "air" },
    { category: "survival", need: "water" },
    { category: "survival", need: "healthyEnvironment" },
    { category: "survival", need: "exercise" },
    { category: "survival", need: "light" },
    { category: "survival", need: "movement" },
    { category: "survival", need: "protection" },
    { category: "survival", need: "care" },
    { category: "survival", need: "security" },
    { category: "wellbeing", need: "autonomy" },
    { category: "wellbeing", need: "calm" },
    { category: "wellbeing", need: "consistency" },
    { category: "wellbeing", need: "trust" },
    { category: "wellbeing", need: "comfort" },
    { category: "wellbeing", need: "diversity" },
    { category: "wellbeing", need: "entertainment" },
    { category: "wellbeing", need: "lightness" },
    { category: "wellbeing", need: "serenity" },
    { category: "wellbeing", need: "presenceToOneself" },
    { category: "wellbeing", need: "fluidity" },
    { category: "wellbeing", need: "order" },
    { category: "rea", need: "assertiveness" },
    { category: "rea", need: "selfPowerApprobation" },
    { category: "rea", need: "freedom" },
    { category: "rea", need: "completeness" },
    { category: "rea", need: "development" },
    { category: "rea", need: "discovery" },
    { category: "rea", need: "fulfillment" },
    { category: "rea", need: "balanced" },
    { category: "rea", need: "expression" },
    { category: "com", need: "collaboration" },
    { category: "com", need: "confidentiality" },
    { category: "com", need: "cooperation" },
    { category: "com", need: "discretion" },
    { category: "com", need: "interdependence" },
    { category: "com", need: "recognition" },
    { category: "com", need: "respect" },
    { category: "com", need: "presence" },
    { category: "com", need: "fluidity" },
    { category: "com", need: "contribute" },
    { category: "rel", need: "recognition" },
    { category: "rel", need: "affection" },
    { category: "rel", need: "love" },
    { category: "rel", need: "membership" },
    { category: "rel", need: "authenticity" },
    { category: "rel", need: "warning" },
    { category: "rel", need: "complicity" },
    { category: "rel", need: "empathy" },
    { category: "rel", need: "trust" },
    { category: "rel", need: "connection" },
    { category: "rel", need: "sweetness" },
    { category: "rel", need: "exchange" },
    { category: "rel", need: "sexualExpression" },
    { category: "spi", need: "consciousness" },
    { category: "spi", need: "equity" },
    { category: "spi", need: "meaning" },
    { category: "spi", need: "silence" },
    { category: "spi", need: "transcendence" },
    { category: "spi", need: "resourcing" },
    { category: "spi", need: "harmony" },
    { category: "spi", need: "peace" },
    { category: "spi", need: "beauty" },
    { category: "spi", need: "inspiration" },
    { category: "spi", need: "celebration" },
] as const;

export const EMOTION_CATEGORIES = Object.freeze(["fear", "surprise", "disgust", "anger", "sadness", "vigilence", "joy", "peace"]);
export const NEED_CATEGORIES = Object.freeze(["survival","wellbeing","rea","com","rel","spi"]);



export const CNV_EMOTIONS = [
    { category: "fear", emotion: "alarmed" },
    { category: "fear", emotion: "anguish" },
    { category: "fear", emotion: "terrified" },
    { category: "fear", emotion: "shocked" },
    { category: "fear", emotion: "disoriented" },
    { category: "fear", emotion: "worried" },
    { category: "fear", emotion: "edgy" },
    { category: "fear", emotion: "distressed" },
    { category: "fear", emotion: "mistrustful" },
    { category: "fear", emotion: "scared" },
    { category: "surprise", emotion: "distracted" },
    { category: "surprise", emotion: "surprised" },
    { category: "surprise", emotion: "surprise" },
    { category: "surprise", emotion: "awake" },
    { category: "surprise", emotion: "interested" },
    { category: "surprise", emotion: "stunned" },
    { category: "surprise", emotion: "amused" },
    { category: "surprise", emotion: "amazed" },
    { category: "surprise", emotion: "exited" },
    { category: "surprise", emotion: "hilarious" },
    { category: "disgust", emotion: "disilused" },
    { category: "disgust", emotion: "destroyed" },
    { category: "disgust", emotion: "tired" },
    { category: "disgust", emotion: "disgusted" },
    { category: "disgust", emotion: "nauseate" },
    { category: "disgust", emotion: "detached" },
    { category: "disgust", emotion: "horrified" },
    { category: "disgust", emotion: "bored" },
    { category: "disgust", emotion: "uncomfortable" },
    { category: "disgust", emotion: "illAtEase" },
    { category: "anger", emotion: "upset" },
    { category: "anger", emotion: "angry" },
    { category: "anger", emotion: "frustrated" },
    { category: "anger", emotion: "enraged" },
    { category: "anger", emotion: "tense" },
    { category: "anger", emotion: "nervous" },
    { category: "anger", emotion: "mad" },
    { category: "anger", emotion: "annoyed" },
    { category: "anger", emotion: "vexed" },
    { category: "anger", emotion: "bitter" },
    { category: "sadness", emotion: "melancholy" },
    { category: "sadness", emotion: "discouraged" },
    { category: "sadness", emotion: "depressed" },
    { category: "sadness", emotion: "concerned" },
    { category: "sadness", emotion: "destroyed" },
    { category: "sadness", emotion: "tornUp" },
    { category: "sadness", emotion: "unfortunate" },
    { category: "sadness", emotion: "sad" },
    { category: "sadness", emotion: "demui" },
    { category: "sadness", emotion: "sorry" },
    { category: "vigilence", emotion: "interested" },
    { category: "vigilence", emotion: "vigilant" },
    { category: "vigilence", emotion: "waiting" },
    { category: "vigilence", emotion: "inHope" },
    { category: "vigilence", emotion: "optimistic" },
    { category: "vigilence", emotion: "curious" },
    { category: "vigilence", emotion: "confident" },
    { category: "vigilence", emotion: "powerful" },
    { category: "vigilence", emotion: "awake" },
    { category: "vigilence", emotion: "attentive" },
    { category: "joy", emotion: "happy" },
    { category: "joy", emotion: "goodMood" },
    { category: "joy", emotion: "playful" },
    { category: "joy", emotion: "living" },
    { category: "joy", emotion: "merry" },
    { category: "joy", emotion: "enthusiastic" },
    { category: "joy", emotion: "euphoric" },
    { category: "joy", emotion: "grateful" },
    { category: "joy", emotion: "calm" },
    { category: "joy", emotion: "satisfied" },
    { category: "peace", emotion: "filled" },
    { category: "peace", emotion: "charm" },
    { category: "peace", emotion: "delighted" },
    { category: "peace", emotion: "emu" },
    { category: "peace", emotion: "peaceful" },
    { category: "peace", emotion: "tender" },
    { category: "peace", emotion: "reassured" },
    { category: "peace", emotion: "encouraged" },
    { category: "peace", emotion: "attracted" },
    { category: "peace", emotion: "ecstatic" },
] as const;

export const EXERCISE_CATEGORY = [
    "emotionsHandling",
    "needUnderstanding",
    "valuesRetrieval",
    "healthCare",
    "loveConnexion",
    "stayConnected",
    "selfEsteem",
    "takeAction",
    "instantConnection",
    "brainSchema",
] as const;

