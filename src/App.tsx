import { useNetInfo } from '@react-native-community/netinfo'
import React, { useEffect, useRef, useState } from 'react'
import { AppState, Image, ImageBackground, View } from 'react-native'
import deviceInfoModule from 'react-native-device-info'
import { Icon } from 'react-native-elements'
import RNIap from 'react-native-iap'
import * as RNLocalize from "react-native-localize"
import { Provider, useDispatch, useSelector } from 'react-redux'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import * as ParseServer from 'src/services/parse/parse.service'
import { BG_IMAGE, IAP_SUBS } from './assets/data/app.data'
import Navigation from './components/navigation/navigation.component'
import { AppText } from './components/util/app-text.component'
import { UserState } from './models/user.model'
import { setI18nConfig, translate } from './services/i18n.service'
import VersionLiveQuery from './services/parse/version-live-query'
import store from './store/configureStore'
import { SET_USER, UPDATE_LAST_ACTIVE_DAY } from './store/reducers/user/user.action'
import theme from './theme/theme'
import { PRIMARY_COLOR } from './theme/variables'


const ONE_DAY = 60 * 60 * 24 * 1000;

const WrappedApp: React.FC = () => {
  const user: UserState = useSelector(state => state.user)
  const { lastLanguageUpdate, minimalVersion } = useSelector(state => state.settings)
  const [isLoaded, setLoaded] = useState(false);
  const netInfo = useNetInfo();
  const dispatch = useDispatch();
  const checkNextDayTimeout = useRef(null);
  const lockOnServer = useRef(false);

  const _dayChangedSincePreviousUse = () => {
    const now = +new Date();
    return !user.lastDayActive || ((now - +new Date(user.lastDayActive)) > ONE_DAY);
  }
  const _checkDayChanged = () => {
    if (_dayChangedSincePreviousUse()) {

      if (!lockOnServer.current) {
        lockOnServer.current = true;
        ParseServer.incrementActiveDays()
          .then((result) => {
            if (result) {
              dispatch({ type: SET_USER, payload: { activeDays: result } })
              dispatch({ type: UPDATE_LAST_ACTIVE_DAY })
            }
          })
          .finally(() => lockOnServer.current = false)
      }
    }
    checkNextDayTimeout.current = setTimeout(() => {
      clearTimeout(checkNextDayTimeout.current)
      _checkDayChanged()
    },
      (!user.lastDayActive || ((+new Date() - +new Date(user.lastDayActive)) > ONE_DAY)
        ?
        ONE_DAY
        :
        (ONE_DAY - (+new Date() - +new Date(user.lastDayActive))))
    )

  }

  const _handleAppStateChange = (state: string) => {
    if (state == "active") {
      RNIap.getAvailablePurchases()
        .then(result => {
          const productIds = result.map(purchase => purchase.productId)

          if (IAP_SUBS.some(sub => productIds.includes(sub))) {
            if (!user.isPremium) { //if the user has a valid purchase but wasn't registered as premium
              ParseServer.changeUser({ isPremium: true }).
                then(() => {
                  dispatch({ type: SET_USER, payload: { isPremium: true } })
                })
                .catch((error) => console.log(error))
            }
          }
          else {
            if (user.isPremium) {//if the user was registered as premium but has no valid purchase
              ParseServer.changeUser({ isPremium: false }).
                then(() => {
                  dispatch({ type: SET_USER, payload: { isPremium: false } })
                })
                .catch((error) => console.log(error))
            }
          }
        });
      _checkDayChanged()
    }
    else {
      if (checkNextDayTimeout.current) clearTimeout(checkNextDayTimeout.current)
    }
  }


  useEffect(() => {
    setI18nConfig(lastLanguageUpdate).then(() => setLoaded(true));
    //I18N module setup
    RNLocalize.addEventListener("change", _handleLocalizationChange);

    //check if app version matches
    VersionLiveQuery.subscribeVersion();
    ParseServer.getMinimalVersion().then(version => dispatch({ type: "SET_MINIMAL_VERSION", payload: version }))

    if (user.logged) {
      //Server live query setup
      ParseServer.userUpdateSubscribe()

      //handle when the app goes to foreground
      _handleAppStateChange("active")
      AppState.addEventListener("change", _handleAppStateChange)
    }

    return () => {
      AppState.removeEventListener("change", _handleAppStateChange);
      RNLocalize.removeEventListener("change", _handleLocalizationChange);
      VersionLiveQuery.unsubscribe()
    }
  }, [])

  const _handleLocalizationChange = async () => {
    await setI18nConfig(lastLanguageUpdate).then(() => setLoaded(true));
  };

  return (
    <>
      {
        (minimalVersion && minimalVersion > deviceInfoModule.getVersion()) ?
          <ImageBackground source={BG_IMAGE} style={[theme.bgImage, { justifyContent: "center", alignItems: "center" }]}>
            <Icon
              name="update"
              type="material-community"
              size={42}
              color="white"
            />
            <AppText h4 style={{ textAlign: "center", width: "80%" }}>{`Nous sommes sincèrement désolés, mais votre version de l'application (${deviceInfoModule.getVersion()}) n'est plus compatible. Veuillez la mettre à jour.`}</AppText>
          </ImageBackground>
          :
          netInfo.isConnected ?
          isLoaded && <Navigation />
          :
          <View style={{ height: "100%", backgroundColor: PRIMARY_COLOR, justifyContent: "center", alignItems: 'center', padding: 2 }}>
            <Icon
                name="signal-cellular-connected-no-internet-4-bar"
                type="material"
                size={64}
                color="white"
              />
            <AppText h2 style={{ textAlign: "center" }}>{translate("noInternet")} :-(</AppText>
              <Image source={require('src/assets/img/logo-notext.png')} style={{maxWidth:100, maxHeight:158, marginTop:32}} />
  
          </View>
          
      }
    </>

  )
}

const App: React.FC = () => {
  const persistor = persistStore(store)
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <WrappedApp />
      </PersistGate>
    </Provider>
  )
}
export default App;
