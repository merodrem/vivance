import AsyncStorage from '@react-native-community/async-storage';
import { createStore } from 'redux';
import { persistCombineReducers } from 'redux-persist';
import exerciseReducer from './reducers/exercises/exercises.reducer';
import favReducer from './reducers/Favorites/favorites.reducer';
import globalSettingsReducer from './reducers/globalSettings/globalSettings.reducer';
import noteReducer from './reducers/notes/notes.reducer';
import somReducer from './reducers/StatesOfMind/states-of-mind.reducer';
import userReducer from './reducers/user/user.reducer';

const rootPersistConfig = {
    key: 'root',
    storage: AsyncStorage
  }

  
const store = createStore(persistCombineReducers(rootPersistConfig,{
    settings: globalSettingsReducer, //SOM =State Of Mind
    user: userReducer,
    exercises: exerciseReducer,
    notes: noteReducer,
    SOMs: somReducer,
    favs: favReducer
}))
export default store