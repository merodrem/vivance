import { StateOfMind } from "src/models/state-of-mind.model"

export const SET_SOMS = 'SET_SOMS'
export const ADD_SOM = 'ADD_SOM'

class SetStatesOfMindAction { 
    readonly type: string = SET_SOMS
    constructor(public payload: StateOfMind[]) { }
} 

class AddStateOfMindAction { 
    readonly type: string = ADD_SOM
    constructor(public payload: StateOfMind) { }
} 

export type StateOfMindAction = SetStatesOfMindAction | AddStateOfMindAction