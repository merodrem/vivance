import { StateOfMind } from 'src/models/state-of-mind.model'
import { StateOfMindAction, SET_SOMS, ADD_SOM } from './states-of-mind.action'

const initState: StateOfMind[] = []

function somReducer(state = initState, action: StateOfMindAction) {
    switch (action.type) {
        case SET_SOMS:
            return action.payload || state
        case ADD_SOM:
            return [...state, action.payload] || state
        default:
            return state
    }
}
export default somReducer