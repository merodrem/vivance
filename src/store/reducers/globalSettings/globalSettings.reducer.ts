import { GlobalSettingsState } from "./globalSettings.model"
import { Action, SET_INSPIRATION, SET_LANGUAGES, SET_MINIMAL_VERSION } from "./globalSettings.actions"

const initState: GlobalSettingsState = {
    firstLaunch: true,
    lastLanguageUpdate: undefined,
    language: {},
    minimalVersion: undefined
}

function globalSettingsReducer(state = initState, action: Action) {
    switch (action.type) {
        case 'APP_OPENED':
            return { ...state, firstLaunch: false } || state
        case SET_LANGUAGES:
            return { ...state, language: action.payload.values, lastLanguageUpdate: new Date().toISOString() } || state
        case SET_MINIMAL_VERSION:
            return { ...state, minimalVersion: action.payload } || state
        case SET_INSPIRATION:
            return { ...state, ...action.payload } || state
        default:
            return state
    }
}

export default globalSettingsReducer