
export const SET_LANGUAGES = 'SET_LANGUAGES'
export const SET_MINIMAL_VERSION = 'SET_MINIMAL_VERSION'
export const SET_INSPIRATION = 'SET_INSPIRATION'

class SetLanguagesAction { 
    readonly type: string = SET_LANGUAGES
    constructor(public payload: Object) { }
} 

class SetMinimalVersionAction { 
    readonly type: string = SET_MINIMAL_VERSION
    constructor(public payload: string) { }
} 

class SetInspiration { 
    readonly type: string = SET_INSPIRATION
    constructor(public payload: any) { }
} 

export type ExerciseAction = SetLanguagesAction | SetMinimalVersionAction | SetInspiration