
export interface GlobalSettingsState {
    firstLaunch: boolean,
    lastLanguageUpdate: string|undefined,
    language:Object,
    minimalVersion: string|undefined,
    inspirationDayOfYear: number|undefined,
    inspiration: string,

}