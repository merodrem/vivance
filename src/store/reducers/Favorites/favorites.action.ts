import { Exercise } from "../../../models/exercise.model"

export const SET_FAVS = 'SET_FAVS'
export const TOGGLE_FAVS = 'TOGGLE_FAVS'

class SetFavoritesAction { 
    readonly type: string = SET_FAVS
    constructor(public payload: Exercise[]) { }
} 

class ToggleFavoriteAction { 
    readonly type: string = TOGGLE_FAVS
    constructor(public payload: Exercise) { }
} 

export type FavoritesAction = SetFavoritesAction | ToggleFavoriteAction