import { Exercise } from '../../../models/exercise.model';
import { FavoritesAction, SET_FAVS, TOGGLE_FAVS } from './favorites.action'

const initState: Exercise[] = []

function favReducer(state = initState, action: FavoritesAction) {
    switch (action.type) {
        case SET_FAVS:
            return action.payload || state
        case TOGGLE_FAVS:
            if (state.find(ex => ex.objectId == action.payload.objectId) !== undefined) {
                return state.filter(ex => ex.objectId != action.payload.objectId) || state;
            }
            else {
                return [...state, action.payload] || state
            }
        default:
            return state
    }
}
export default favReducer