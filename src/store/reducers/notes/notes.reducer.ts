import { Note } from 'src/models/note.model'
import { ADD_NOTE, DELETE_NOTE, NoteAction, SET_NOTES, UPDATE_NOTE } from './notes.action'

const initState: Note[] = []

function noteReducer(state = initState, action: NoteAction) {
    switch (action.type) {
        case SET_NOTES:
            return action.payload || state
        case ADD_NOTE:
            return [...state, action.payload] || state
        case DELETE_NOTE:
            return state.filter(note => note.objectId !== action.payload) || state
        case UPDATE_NOTE:
            const newNotes = [...state]
            newNotes[newNotes.findIndex(note => note.objectId == action.payload.objectId)] = action.payload
            return [...newNotes] || state
        default:
            return state
    }
}

export default noteReducer