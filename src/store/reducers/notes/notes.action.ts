import { Note } from "src/models/note.model"

export const SET_NOTES = 'SET_NOTES'
export const ADD_NOTE = 'ADD_NOTE'
export const DELETE_NOTE = 'DELETE_NOTE'
export const UPDATE_NOTE = 'UPDATE_NOTE'

class SetNotesAction { 
    readonly type: string = SET_NOTES
    constructor(public payload: Note[]) { }
} 

class AddNoteAction { 
    readonly type: string = ADD_NOTE
    constructor(public payload: Note) { }
} 

class DeleteNoteAction { 
    readonly type: string = DELETE_NOTE
    constructor(public payload: string) { }
} 

class UpdateNoteAction {
    readonly type: string = UPDATE_NOTE
    constructor(public payload: Note) { }
}

export type NoteAction = SetNotesAction | AddNoteAction | DeleteNoteAction |UpdateNoteAction