import { Exercise } from "src/models/exercise.model"

export const SET_EXERCISES = 'SET_EXERCISES'
export const ADD_EXERCISE = 'ADD_EXERCISE'

class SetExercisesAction { 
    readonly type: string = SET_EXERCISES
    constructor(public payload: Exercise[]) { }
} 

class AddExercisesAction { 
    readonly type: string = ADD_EXERCISE
    constructor(public payload: Exercise) { }
} 


export type ExerciseAction = SetExercisesAction | AddExercisesAction