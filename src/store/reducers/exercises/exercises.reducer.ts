import { Exercise } from 'src/models/exercise.model'
import { ExerciseAction, SET_EXERCISES, ADD_EXERCISE } from './exercises.action'

const initState: Exercise[] = []

function exerciseReducer(state = initState, action: ExerciseAction) {
    switch (action.type) {
        case SET_EXERCISES:
            return action.payload || state
        case ADD_EXERCISE:
            return [...state, action.payload] || state
        default:
            return state
    }
}

export default exerciseReducer