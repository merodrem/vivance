import { StateOfMind } from "src/models/state-of-mind.model"
import { UserState } from "src/models/user.model"
import { Note } from "src/models/note.model"
import { SoulWounds } from "src/models/soul-wounds.model"

export const TOGGLE_LOGIN = 'TOGGLE_LOGIN'
export const SET_USER = 'SET_USER'
export const SET_SOULWOUNDS = 'SET_SOULWOUNDS'
export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE'
export const UPDATE_LAST_ACTIVE_DAY = 'UPDATE_LAST_ACTIVE_DAY'
export const INCREMENT_EXERCISE_COUNT = 'INCREMENT_EXERCISE_COUNT'
export const INCREMENT_SOM_COUNT = 'INCREMENT_SOM_COUNT'
export const INCREMENT_TAG_COUNT = 'INCREMENT_TAG_COUNT'

export class ToggleLoginAction {
    readonly type: string = TOGGLE_LOGIN
    constructor(public payload: UserState) { }
}
export class SetSoulWoundsAction {
    readonly type: string = SET_SOULWOUNDS
    constructor(public payload: SoulWounds) { }
}
export class SetUserAction {
    readonly type: string = SET_USER
    constructor(public payload: Object) { }
}
export class ToggleFavoriteAction {
    readonly type: string = TOGGLE_FAVORITE
    constructor(public payload: string) { }
}
export class UpdateLastActiveDayAction {
    readonly type: string = UPDATE_LAST_ACTIVE_DAY
}

export class IncrementExerciseCountAction {
    readonly type: string = INCREMENT_EXERCISE_COUNT
}

export class IncrementSOMCountAction {
    readonly type: string = INCREMENT_EXERCISE_COUNT
}

export class IncrementTagCountAction {
    readonly type: string = INCREMENT_TAG_COUNT
    constructor(public payload: number) { }

}

export type Action = ToggleLoginAction
    | ToggleLoginAction
    | SetSoulWoundsAction
    | ToggleFavoriteAction
    | SetSoulWoundsAction
    | UpdateLastActiveDayAction
    | IncrementExerciseCountAction
    | IncrementSOMCountAction
    | IncrementTagCountAction