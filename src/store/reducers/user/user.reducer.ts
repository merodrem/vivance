import { Action, TOGGLE_LOGIN, TOGGLE_FAVORITE, SET_SOULWOUNDS, INCREMENT_EXERCISE_COUNT, SET_USER, UPDATE_LAST_ACTIVE_DAY, INCREMENT_SOM_COUNT, INCREMENT_TAG_COUNT } from "./user.action"
import { UserState } from "src/models/user.model"

const initState: UserState = {
    logged: false,
    statesOfMind: [],
    isPremium: false,
    favoriteExercises: []
}

function userReducer(state = initState, action: Action) {
    let nextState
    switch (action.type) {
        case TOGGLE_LOGIN:
            nextState = action.payload ? { ...action.payload, logged: !state.logged } : initState
            return nextState || state;
        case SET_USER:
            return { ...state, ...action.payload } || state;
        case SET_SOULWOUNDS:
            return { ...state, soulWounds: action.payload } || state;
        case TOGGLE_FAVORITE:
            if (state.favoriteExercises.includes(action.payload))
                return { ...state, favoriteExercises: state.favoriteExercises.filter(fav => fav != action.payload) } || state;
            else
                return { ...state, favoriteExercises: [...state.favoriteExercises, action.payload] } || state;
        case UPDATE_LAST_ACTIVE_DAY:
            return { ...state, lastDayActive: new Date().toISOString() } || state;
        case INCREMENT_EXERCISE_COUNT:
            return { ...state, exercisesCount: state.exercisesCount + 1 } || state;
        case INCREMENT_SOM_COUNT:
            return { ...state, statesOfMindCount: state.statesOfMindCount + 1 } || state;
        case INCREMENT_TAG_COUNT:
                return { ...state, statesOfMindCount: state.statesOfMindCount + action.payload } || state;
        default:
            return state
    }
}

export default userReducer