export interface Avatar {
    filename?: string,
    base64: string
}