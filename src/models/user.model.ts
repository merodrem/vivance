import { Gender } from "src/assets/data/app.data";
import { Location } from "./location.model";
import { SoulWounds } from "./soul-wounds.model";
import { StateOfMind } from "./state-of-mind.model";

export interface UserState {
    logged: boolean,
    objectId: string,
    statesOfMind: StateOfMind[],
    isPremium: boolean,
    favoriteExercises: string[],
    username: string,
    email: string,
    dateOfBirth: string,
    gender: Gender,
    presentation: string,
    goldenBadges: number,
    silverBadges: number,
    bronzeBadges: number,
    location: Location ,
    soulWounds: undefined | SoulWounds,
    lastDayActive: Date,
    activeDays: number,
    statesOfMindCount: number,
    exercisesCount: number,
    tagCount: number,
    profileViewers: string[],
    impactedUsers: number,
    downVotedExercises: string[],
    upVotedExercises: string[],
    reputation: number
    avatar?: string|undefined,//base64 encoded image
}