import { Image } from "./image.model";

export interface Exercise {
    objectId?: string,
    title: string,
    category: string,
    text: string,
    duration: string,
    score: number,
    tags: string[],
    originalPoster:any,
    image: Image|string|undefined,//base64 encoded image
    isPremium: boolean
}