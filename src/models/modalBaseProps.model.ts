export type ModalBaseProps = {
    isVisible: boolean,
    onHide: () => void,
  }