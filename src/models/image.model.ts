export interface Image {
    filename: string|undefined,
    base64: string
}