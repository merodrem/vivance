
export interface SoulWounds {
    rejection: number, 
    abandonment: number, 
    injustice: number, 
    humiliation: number,
    betrayal: number
}