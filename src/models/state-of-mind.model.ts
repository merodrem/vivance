
//Represents a Need or an Emotion
export interface Feeling{
    name: string,
    intensity: number,
    category: string
}

export interface Situation {
    type: string,
    details: string,
}
export interface StateOfMind {
    situation:Situation,
    emotions: Array<Feeling>,
    need: Feeling|undefined,
    timestamp: string,
    tags: string[],
    note: string
}