
export interface Note {
    objectId?: string,
    tags: string[],
    exerciseId?: string,
    type: "exercise" | "free",
    content: string,
    timestamp: string
}