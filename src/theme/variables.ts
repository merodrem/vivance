// export const PRIMARY_COLOR = '#a6cfbd'
export const PRIMARY_COLOR = '#2f4664'
export const ACCENT_COLOR = "#4DA8DA"

export const GRADIENT = Object.freeze(['#08475c', '#347489']);
// const other = ['#a6cfbd', '#008037' ]