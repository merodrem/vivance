import { StyleSheet } from "react-native";
import { PRIMARY_COLOR } from "./variables";

const theme = StyleSheet.create({
    modal: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 16
    },
    bgImage: {
        // minHeight:SCREEN_HEIGHT,
        width: "100%",
        height: "100%",
    },
    panelContainer: {
        // maxHeight: '80%',
        // width: '90%',
        // alignSelf: 'center',
        justifyContent: 'center',
        padding: 8,
        marginVertical: 8,
        borderRadius: 15,
        backgroundColor: 'rgba(255,255,255,0.9)'
    },
    darkPanel: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginVertical: 8,
        borderRadius: 15,
        padding: 16,
        margin: 4,
        backgroundColor: "rgba(47, 70, 100, 0.8)",
    },
    outlineButton: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderRadius: 50,
        backgroundColor: PRIMARY_COLOR
    },
    nextbuttonContainer: {
        alignSelf: 'center',
        position: 'absolute',
        bottom: 8,
        width: '90%'
    },
    panelTitle: {
        textAlign: 'left',
        margin: 8,
        paddingLeft:8,
        color: 'black'
    },
    headerText: {
        textAlign: 'left',
        padding: 8,
        fontWeight: 'bold',
        color: 'black'
    },
    commonText: {
        paddingHorizontal: 8,
        paddingBottom: 8,
        textAlign: 'center',
        color: 'black'
    },
    infoText: {
        padding: 4,
        fontSize: 14,
        color: 'black'
    },
    quote: {
        textAlign: 'center',
        fontStyle: 'italic',
        padding: 8
    },
    sliderTrack: {
        height: 2,
    },
    textInput:{
        backgroundColor: 'white',
        width: '99%',
        maxHeight: '60%',
        borderWidth: 1,
        borderColor: PRIMARY_COLOR
    },
    sliderThumb: {
        width: 15,
        height: 15,
        backgroundColor: PRIMARY_COLOR,
        borderRadius: 50,
    },
    darkInputContainer: {
        borderRadius: 5,
        borderBottomWidth: 0,
        backgroundColor: "rgba(47, 70, 100, 0.8)",

    },
    darkInput: {
        marginHorizontal: 0,
        color: 'white',
        paddingLeft: 16,
    },

    badge: {
        paddingVertical: 16,
        paddingHorizontal: 12,
        borderRadius: 5,
        margin: 2
    }
})

export default theme;