import I18n from "i18n-js";
import { memoize } from "lodash";
import * as RNLocalize from "react-native-localize";
import * as ParseServer from 'src/services/parse/parse.service';
import store from "src/store/configureStore";
import { SET_LANGUAGES } from "src/store/reducers/globalSettings/globalSettings.actions";

export const translate = memoize(
    (key, config = undefined) => I18n.t(key, config),
    (key, config = undefined) => (config ? key + JSON.stringify(config) : key)
);

const available_translations = ["fr"];

export async function setI18nConfig(lastLanguageUpdate: string) {
    // fallback if no available language fits
    const fallback = { languageTag: "fr", isRTL: false };
    let translationGetters = {};

    const { languageTag } =
        RNLocalize.findBestAvailableLanguage(available_translations) ||
        fallback;

    try {
        const lang = await ParseServer.loadLanguages(lastLanguageUpdate, languageTag)
        
        if (!("language" in lang)) {
            translationGetters = { [languageTag]: store.getState().settings.language };
        }
        else {
            translationGetters = { [languageTag]: lang.values };
            store.dispatch({ type: SET_LANGUAGES, payload: lang })
        }
    } catch (error) {
        console.log("Error while fetching the translations:" + error);
        translationGetters = {
            fr: require("../assets/lang/fr.json"),
        };
    }


    // clear translation cache
    translate.cache.clear();

    I18n.defaultLocale = "fr";
    // set i18n-js config
    I18n.translations = { [languageTag]: translationGetters[languageTag] };
    I18n.locale = languageTag;
};

export function getCurrentLanguage() {
    return I18n.locale;
}