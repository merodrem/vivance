const STEPS = 9;

export const intensityTranslation = [
    {
        label: 'très peu',
        color: '#00C300'
    },
    {
        label: 'très légèrement',
        color: '#4EC300'
    },
    {
        label: 'légèrement',
        color: '#75C300'
    },
    {
        label: 'Moyennement',
        color: '#9CC300'
    },
    {
        label: '',
        color: '#9CC300'
    },
    {
        label: 'assez',
        color: '#C39C00'
    },
    {
        label: 'vraiment',
        color: '#C37500'
    },
    {
        label: 'beaucoup',
        color: '#C34E00'
    },
    {
        label: 'extrêmement',
        color: '#C32700'
    },
] as const;


