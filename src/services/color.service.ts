
export function getColor(label: string): string {
    switch (label) {
        case "fear":
            return '#B6D7A8';
        case "surprise":
            return '#6aa84f';
        case "disgust":
            return '#a64d79';
        case "anger":
            return '#cc0000';
        case "sadness":
            return '#6fa8dc';
        case "vigilence":
            return '#e69138';
        case "joy":
            return '#fbbc04';
        case "peace":
            return '#F4D487';
        case "survival":
            return '#980000';
        case "wellbeing":
            return '#F1C232';
        case "rea":
            return '#073763';
        case "com":
            return '#6D9EEB';
        case "rel":
            return '#34a853';
        case "spi":
            return '#5d50ae';
        default:
            return undefined;
    }
}

