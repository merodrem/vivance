const tresholds = {
    activeDays:[7,30,180],
    statesOfMind:[10,50,200],
    exercises:[5,10,25],
    tags:[50,100,200],
    impactedUsers:[50,250,1000],
    profileViewers:[50,250,1000],  
} as const;

export function getCompletion(count: number, indicator:"activeDays"|"statesOfMind"|"exercises"|"impactedUsers"|"profileViewers"|"tags") {
    count = count % tresholds[indicator][tresholds[indicator].length - 1];
    for (let i = 0; i < tresholds[indicator].length; i++) {
        if( count < tresholds[indicator][i]){
            return count*100 / tresholds[indicator][i]
        }
        
    }
}
