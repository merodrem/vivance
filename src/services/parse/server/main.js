

const { parse } = require("qs");
var cloudFunctions = require("./cloud-functions");


Parse.Cloud.define("signUp", cloudFunctions.signUp(Parse));
Parse.Cloud.define("addStateOfMind", cloudFunctions.addStateOfMind(Parse));
Parse.Cloud.define("loadUserStatesOfMind", cloudFunctions.loadUserStatesOfMind(Parse));
Parse.Cloud.define("addNote", cloudFunctions.addNote(Parse));
Parse.Cloud.define("editNote", cloudFunctions.editNote(Parse));
Parse.Cloud.define("getNotes", cloudFunctions.getNotes(Parse));
Parse.Cloud.define("deleteNote", cloudFunctions.deleteNote(Parse));
Parse.Cloud.define("addExercise", cloudFunctions.addExercise(Parse));
Parse.Cloud.define("addFavoriteExercise", cloudFunctions.addFavoriteExercise(Parse));
Parse.Cloud.define("getExercisesOverview", cloudFunctions.getExercisesOverview(Parse));
Parse.Cloud.define("getExercises", cloudFunctions.getExercises(Parse));
Parse.Cloud.define("removeFavoriteExercise", cloudFunctions.removeFavoriteExercise(Parse));
Parse.Cloud.define("viewProfile", cloudFunctions.viewProfile(Parse));
Parse.Cloud.define("incrementActiveDays", cloudFunctions.incrementActiveDays(Parse));
Parse.Cloud.define("voteForExercise", cloudFunctions.voteForExercise(Parse));
Parse.Cloud.define("deleteExerciseVote", cloudFunctions.deleteExerciseVote(Parse));
Parse.Cloud.define("changeUser", cloudFunctions.changeUser(Parse));
Parse.Cloud.afterSave("ExerciseVote", cloudFunctions.exerciseVoteAfterSave(Parse));
Parse.Cloud.define("changeAvatar", cloudFunctions.changeAvatar(Parse));
Parse.Cloud.define("loadLanguages", cloudFunctions.loadLanguages(Parse));
Parse.Cloud.define("loadDayInspiration", cloudFunctions.loadDayInspiration(Parse));
Parse.Cloud.define("getFavoriteExercises", cloudFunctions.getFavoriteExercises(Parse));

Parse.Cloud.afterDelete("ExerciseVote", async (request) => {
    const exerciseVote = request.object;
    const exercise = await exerciseVote.get('exercise').fetch();
    exercise.save(
        {
            score: exercise.get("score") - exerciseVote.get('value')
        },
        { useMasterKey: true }
    )
    if (await exerciseVote.get('user').exists()) {
        const user = await exerciseVote.get('user').fetch();
        switch (exerciseVote.get('value')) {
            case 1:
                user.set("upVotedExercises", user.get("upVotedExercises").filter(ex => ex != exercise.id))
                break;
            case -1:
                user.set("downVotedExercises", user.get("downVotedExercises").filter(ex => ex != exercise.id))
                break;
            default:
                throw 'Invalid vote value for the exercise!';
        }
        await user.save(null, { useMasterKey: true });
    }
    if (await exercise.get('originalPoster').exists()) {
        const exercisePoster = await exercise.get('originalPoster').fetch();
        if ((exerciseVote.get('value') == 1)) {
            exercisePoster.set("impactedUsers", exercisePoster.get('impactedUsers') -1)//for some reason decrement() not working
            exercisePoster.set("reputation", exercisePoster.get('reputation') -2)//for some reason decrement() not working
            exercisePoster.save(null, { useMasterKey: true });
        }
    }
});

Parse.Cloud.job("createAdmin", async () => {
    const Role = Parse.Object.extend('_Role')
    try {
        const acl = new Parse.ACL()
        acl.setPublicReadAccess(true)
        acl.setPublicWriteAccess(false)
        const adminRole = new Role()
        adminRole.set('name', 'admin')
        adminRole.setACL(acl)
        const role = await adminRole.save({}, { useMasterKey: true })
        return "successfully created role: " + role;
    } catch (error) {
        console.log(error)
        throw error
    }

});
