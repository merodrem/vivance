/********************************
*                               *
*          TEST CONFIG          *
*                               *
********************************/

const { result } = require("lodash");

// if (typeof Parse == 'undefined') {
//     var Parse = require('parse/node');
//     var constants = require("src/../spec/constants");
//     Parse.initialize(constants.APPLICATION_ID, null, constants.MASTER_KEY);
//     Parse.serverURL = 'https://parseapi.back4app.com'
//     Parse.Cloud.useMasterKey()
// }


/********************************
*                               *
*           CONSTANTS           *
*                               *
********************************/

const EXERCISE_CATEGORY = [
    "emotionsHandling",
    "needUnderstanding",
    "valuesRetrieval",
    "healthCare",
    "loveConnexion",
    "stayConnected",
    "selfEsteem",
    "takeAction",
    "instantConnection",
    "brainSchema",
];

const tresholds = {
    activeDays: [7, 30, 180],
    statesOfMind: [10, 50, 200],
    exercises: [5, 10, 25],
    tags: [50, 100, 200],
    impactedUsers: [50, 250, 1000],
    profileViewers: [50, 250, 1000],
};


/********************************
*                               *
*       HELPER FUNCTIONS        *
*                               *
********************************/

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateUser(user) {
    return true;
}

/**
 * 
 * @param {*} count 
 * @param {*} indicator "activeDays"|"statesOfMind"|"exercises"|"impactedUsers"|"profileViewers"|"tags"
 * @returns "bronzeBadges"|"silverBadges"|"goldenBadges"|undefined
 */
function checkForbadgeUpdates(count, indicator) {
    if (count == tresholds[indicator][0]) {
        return "bronzeBadges";
    }
    else if (count == tresholds[indicator][1]) {
        return "silverBadges";
    }
    else if (count == tresholds[indicator][2]) {
        return "goldenBadges";
    }
    else {
        return undefined;
    }
}
/********************************
*                               *
*        CLOUD FUNCTIONS        *
*                               *
********************************/

/**
 * @returns a promise resolving to the created user
 */
module.exports.signUp = function (Parse) {
    return async function (request) {
        if (!('username' in request.params) || !('password' in request.params) || !('email' in request.params)) {
            throw "email, password, and username are mandatory!"
        }
        else if (!validateEmail(request.params.email)) {
            throw 'invalid email address: ' + request.params.email;
        }

        const user = new Parse.User();
        user.set("username", request.params.username)
        user.set("password", request.params.password)
        user.set("email", request.params.email)
        user.set("goldenBadges", 0)
        user.set("silverBadges", 0)
        user.set("bronzeBadges", 0)
        user.set("reputation", 0)
        user.set("statesOfMindCount", 0)
        user.set("exercisesCount", 0)
        user.set("tagCount", 0)
        user.set("location", {})
        user.set("presentation", "")
        user.set("gender", "otherGender")
        user.set("isPremium", false)
        user.set("activeDays", 1)
        user.set("lastDayActive", new Date().toISOString())
        user.set("profileViewers", [])
        user.set("downVotedExercises", [])
        user.set("upVotedExercises", [])
        user.set("impactedUsers", 0)

        return user.signUp();


    }
}

module.exports.addStateOfMind = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const count = user.get('statesOfMindCount') % tresholds["statesOfMind"][tresholds["statesOfMind"].length - 1] + 1;
            const badgeToUpdate = checkForbadgeUpdates(count, "statesOfMind");
            if (badgeToUpdate) {
                user.increment(badgeToUpdate);
            }
            user.increment("statesOfMindCount");
            user.increment("reputation", 2 + request.params.tags.length)
            user.increment("tagCount", request.params.tags.length)
            const ParseStateOfMind = Parse.Object.extend('StateOfMind');
            const stateOfMind = new ParseStateOfMind();
            return await stateOfMind.save({ ...request.params, user: user }, { useMasterKey: true });
        }
    }
}

module.exports.loadUserStatesOfMind = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const query = new Parse.Query('StateOfMind');
            query.equalTo("user", user)
            return await query.find({ useMasterKey: true });
        }
    }
}

module.exports.addNote = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const Note = Parse.Object.extend('Note');
            const note = new Note();
            return await note.save({ ...request.params.note, originalPoster: user }, { useMasterKey: true });
        }
    }
}

module.exports.deleteNote = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const query = new Parse.Query('Note');
            const note = await query.get(request.params.noteId);
            if (note.get("originalPoster").id === user.id) {
                await note.destroy({ useMasterKey: true });
                return `note ${note.id} deleted`;
            }
            else {
                throw "Impossible to delete another user's note";
            }
        }
    }
}

module.exports.addExercise = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const isAdmin = user.get('isAdmin');
            const isPremium = user.get("isPremium");
            if (!isAdmin && !isPremium) {
                throw 'you must be an admin or premium to create an exercise';
            }
            const count = user.get('exercisesCount') % tresholds['exercises'][tresholds['exercises'].length - 1] + 1;
            const badgeToUpdate = checkForbadgeUpdates(count, "exercises");
            if (badgeToUpdate) {
                user.increment(badgeToUpdate);
            }
            user.increment("exercisesCount");
            user.increment("reputation", 10 + request.params.tags.length)
            user.increment("tagCount", request.params.tags.length)
            const Exercise = Parse.Object.extend("Exercise");
            const exercise = new Exercise();
            const file = request.params.image ? new Parse.File(request.params.image.filename, { base64: request.params.image.base64 }) : undefined;
            return await exercise.save({
                ...request.params,
                originalPoster: user,
                image: file,
                official: isAdmin
            },
                { useMasterKey: true }
            );
        }
    }
}

module.exports.addFavoriteExercise = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            user.addUnique("favoriteExercises", request.params.exerciseID)
            return await user.save(null, { useMasterKey: true });
        }
    }
}

module.exports.removeFavoriteExercise = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            user.set("favoriteExercises", user.get("favoriteExercises").filter(fav => fav != request.params.exerciseID))
            return await user.save(null, { useMasterKey: true });
        }
    }
}

module.exports.editNote = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const query = new Parse.Query("Note");
            const note = await query.get(request.params.noteId)
            if (note.get("originalPoster").id == user.id) {
                note.set("content", request.params.content)
                return await note.save(null, { useMasterKey: true })
            }
            else {
                throw "cannot edit someone else's note!";
            }
        }
    }
}

module.exports.getNotes = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const query = new Parse.Query("Note");
            query.equalTo("originalPoster", user)
            return await query.find({ useMasterKey: true })
        }
    }
}

module.exports.viewProfile = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            profileViewed = await new Parse.Query("User").get(request.params.profileViewedID);
            if (profileViewed) {
                const count = profileViewed.get('profileViewers') % tresholds['profileViewers'][tresholds['profileViewers'].length - 1] + 1;
                const badgeToUpdate = checkForbadgeUpdates(count, "profileViewers");
                if (badgeToUpdate) {
                    profileViewers.increment(badgeToUpdate);
                }
                profileViewed.addUnique('profileViewers', user.id)
                profileViewed.increment("reputation", 1)
                return profileViewed.save(null, { useMasterKey: true });
            }
        }
    }
}

module.exports.getExercisesOverview = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            let result = {};
            for (let index = 0; index < EXERCISE_CATEGORY.length; index++) {
                let query = new Parse.Query("Exercise");
                query.equalTo("category", EXERCISE_CATEGORY[index]);
                switch (request.params.filter) {
                    case "new":
                        query.descending("updatedAt")
                        break;
                    case "short":
                        query.ascending("duration")
                        break;
                    case "top":
                        query.descending("score")
                        break;
                    case "official":
                        query.equalTo("official", true);
                        break;
                    default:
                        break;
                }
                query.limit(5);
                const isPremium = user.get("isPremium");
                const exercises = await Parse.Object.fetchAllWithInclude(await query.find({ useMasterKey: true }), 'originalPoster',{ useMasterKey: true });
                result[EXERCISE_CATEGORY[index]] = exercises.map(ex => ({
                    ...ex.toJSON(), originalPoster: ex.get('originalPoster') ? {
                        objectId: ex.get('originalPoster').id,
                        username: ex.get('originalPoster').get("username"),
                        reputation: ex.get('originalPoster').get("reputation"),
                        bronzeBadges: ex.get('originalPoster').get("bronzeBadges"),
                        silverBadges: ex.get('originalPoster').get("silverBadges"),
                        goldenBadges: ex.get('originalPoster').get("goldenBadges"),
                        username: ex.get('originalPoster').get("username"),
                        presentation: ex.get('originalPoster').get("presentation"),
                        location: ex.get('originalPoster').get("location"),
                        reputation: ex.get('originalPoster').get("reputation"),
                        bronzeBadges: ex.get('originalPoster').get("bronzeBadges"),
                        silverBadges: ex.get('originalPoster').get("silverBadges"),
                        goldenBadges: ex.get('originalPoster').get("goldenBadges"),
                        activeDays: ex.get('originalPoster').get("activeDays"),
                        statesOfMindCount: ex.get('originalPoster').get("statesOfMindCount"),
                        exercisesCount: ex.get('originalPoster').get("exercisesCount"),
                        profileViewers: ex.get('originalPoster').get("profileViewers"),
                        impactedUsers: ex.get('originalPoster').get("impactedUsers"),
                        tagCount: ex.get('originalPoster').get("tagCount"),
                        avatar: ex.get('originalPoster').get("avatar") ? {
                            url: ex.get('originalPoster').get("avatar").url()
                        } : undefined,
                    }
                        : undefined,
                    image: ex.get('image') ? {
                        url: ex.get('image').url(),
                        name: ex.get('image').name()
                    }
                        : undefined
                    ,
                    text: (!isPremium && ex.get("isPremium")) ? "forbidden access" : ex.get("text")
                }))

            }
            return result;
        }
    }
}

module.exports.getExercises = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const { order, category, page, pageSize } = request.params;
            const query = new Parse.Query("Exercise");
            query.equalTo("category", category);
            query.skip(page * pageSize);
            query.limit(pageSize);
            switch (order) {
                case "new":
                    query.descending("updatedAt")
                    break;
                case "short":
                    query.ascending("duration")
                    break;
                case "top":
                    query.descending("score")
                    break;
                default:
                    break;
            }
            const isPremium = user.get("isPremium");
            const exercises = await Parse.Object.fetchAllWithInclude(await query.find({ useMasterKey: true }), 'originalPoster', { useMasterKey: true });
            return exercises.map(ex => ({
                ...ex.toJSON(), originalPoster: ex.get('originalPoster') ? {
                    objectId: ex.get('originalPoster').id,
                    username: ex.get('originalPoster').get("username"),
                    reputation: ex.get('originalPoster').get("reputation"),
                    bronzeBadges: ex.get('originalPoster').get("bronzeBadges"),
                    silverBadges: ex.get('originalPoster').get("silverBadges"),
                    goldenBadges: ex.get('originalPoster').get("goldenBadges"),
                    username: ex.get('originalPoster').get("username"),
                    presentation: ex.get('originalPoster').get("presentation"),
                    location: ex.get('originalPoster').get("location"),
                    reputation: ex.get('originalPoster').get("reputation"),
                    bronzeBadges: ex.get('originalPoster').get("bronzeBadges"),
                    silverBadges: ex.get('originalPoster').get("silverBadges"),
                    goldenBadges: ex.get('originalPoster').get("goldenBadges"),
                    activeDays: ex.get('originalPoster').get("activeDays"),
                    statesOfMindCount: ex.get('originalPoster').get("statesOfMindCount"),
                    exercisesCount: ex.get('originalPoster').get("exercisesCount"),
                    profileViewers: ex.get('originalPoster').get("profileViewers"),
                    impactedUsers: ex.get('originalPoster').get("impactedUsers"),
                    tagCount: ex.get('originalPoster').get("tagCount"),
                    avatar: ex.get('originalPoster').get("avatar") ? {
                        url: ex.get('originalPoster').get("avatar").url()
                    } : undefined,
                }
                    : undefined,
                image: ex.get('image') ? {
                    url: ex.get('image').url(),
                    name: ex.get('image').name()
                }
                    : undefined,
                text: (!isPremium && ex.get("isPremium")) ? "forbidden access" : ex.get("text")
            }));
        }
    }
}

module.exports.incrementActiveDays = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const oneDay = 60 * 60 * 24 * 1000;
            if ((+new Date() - +new Date(user.get("lastDayActive"))) > oneDay) {
                user.increment("activeDays");
                user.increment("reputation")
                const count = user.get('activeDays') % tresholds["activeDays"][tresholds["activeDays"].length - 1] + 1;
                const badgeToUpdate = checkForbadgeUpdates(count, "activeDays");
                if (badgeToUpdate) {
                    user.increment(badgeToUpdate);
                }
                await user.save({
                    lastDayActive: new Date().toISOString()
                }, { useMasterKey: true })
                return user.get("activeDays")
            }
            else {
                return;
            }
        }
    }
}

module.exports.voteForExercise = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const exercise = await new Parse.Query('Exercise').get(request.params.exerciseID);
            const query = new Parse.Query('ExerciseVote');
            const score = exercise.get('score')
            if ((score + request.params.value) < 0) {
                throw 'The score of an exercise cannot go below zero!';
            }
            query.equalTo('exercise', exercise)
            query.equalTo('user', user)
            const results = await query.find({ useMasterKey: true })
            if (results.length > 1) { //should never happen
                throw 'This user has already voted for this exercise';
            }
            else {
                const newScore = score + ((results.length == 1) ? 2 * request.params.value : request.params.value);
                if (newScore < 0) {
                    throw 'The score of an exercise cannot go below zero!';
                }
                if (results.length == 1) { //if a vote already exists from that user on this exercise
                    if (results[0].get('value') == request.params.value) {
                        throw 'This user has already voted for this exercise';
                    }
                    else {
                        await results[0].destroy()//await here to avoid concurrency problem in afterSave and afterDelete
                    }
                }
                var ExerciseVote = Parse.Object.extend('ExerciseVote');
                const exerciseVote = new ExerciseVote();
                exerciseVote.save(
                    {
                        exercise,
                        user,
                        value: request.params.value,
                    },
                    { useMasterKey: true }
                )
                return newScore
            }
        }
    }
}

module.exports.deleteExerciseVote = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw 'the user should be logged in';
        }
        else {
            const exercise = await new Parse.Query('Exercise').get(request.params.exerciseID);
            const query = new Parse.Query('ExerciseVote');
            query.equalTo('exercise', exercise)
            query.equalTo('user', user)
            const results = await query.find({ useMasterKey: true })
            if (results.length > 1) { //should never happen
                throw 'There should be only one vote for this exercise by this user';
            }
            else if (results.length == 0) {
                throw 'No vote for this exercise by this user';
            }
            else {
                results[0].destroy();
                return (exercise.get("score") - request.params.value)
            }
        }
    }
}


module.exports.changeUser = function (Parse) {
    return async function (request) {
        const user = request.user;
        if (!user) {
            throw ('you must be logged in to change your email');
        }
        if (!validateUser(request.params.payload)) {
            throw `Invalid modifications for user: ${request.params.payload}`
        }
        await user.save({ ...request.params.payload }, { useMasterKey: true });
        return { ...request.params.payload }
    }
}

// returns the empty object '{}' if languages are up to date
module.exports.loadLanguages = function (Parse) {
    return async function (request) {
        const { language, lastLanguageUpdate } = request.params;
        const query = new Parse.Query('I18N');
        query.equalTo("language", language)
        const values = await query.first();
        if (lastLanguageUpdate && values.get("updatedAt") <= new Date(lastLanguageUpdate)) {
            return {};
        }
        else {
            return values.toJSON();
        }
    }
}

module.exports.loadDayInspiration = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw ('you must be logged in to access the inspirations');
        }
        const { dayOfYear, lang } = request.params;
        const query = new Parse.Query('Inspirations');
        query.equalTo("language", lang)
        query.select("inspiration")
        const inspirations = await query.find({ useMasterKey: true });
        return inspirations[dayOfYear % inspirations.length].get("inspiration")
    }
}

module.exports.changeAvatar = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw ('you must be logged in to change your avatar');
        }
        const { filename, base64 } = request.params
        const userUpdated = await request.user.save(
            {
                avatar: new Parse.File(filename ? filename : "unnamed_file", { base64: base64 })
            },
            { useMasterKey: true }
        )
        return userUpdated.toJSON();
    }
}

module.exports.getFavoriteExercises = function (Parse) {
    return async function (request) {
        if (!request.user) {
            throw ('you must be logged in to load exercises');
        }
        const { exerciseIDs } = request.params;
        const result = [];
        exerciseIDs.forEach(async id => {

            result.push((await (new Parse.Query("Exercise")).get(id)).toJSON())
        });
        return result;
    }
}

/********************************
*                               *
*         SIDE EFFECTS          *
*                               *
********************************/


module.exports.exerciseVoteAfterSave = function (Parse) {
    return async function (request) {
        const exerciseVote = request.object;
        const exercise = await exerciseVote.get('exercise').fetch();
        exercise.save(
            {
                score: exercise.get("score") + exerciseVote.get('value')
            },
            { useMasterKey: true }
        )
        if (await exerciseVote.get('user').exists()) {
            const user = await exerciseVote.get('user').fetch();
            switch (exerciseVote.get('value')) {
                case 1:
                    user.addUnique("upVotedExercises", exercise.id)
                    break;
                case -1:
                    user.addUnique("downVotedExercises", exercise.id)
                    break;
                default:
                    throw 'Invalid vote value for the exercise!';
            }
            user.save(null, { useMasterKey: true });
        }
        if (await exercise.get('originalPoster').exists()) {
            const exercisePoster = await exercise.get('originalPoster').fetch();
            if ((exerciseVote.get('value') == 1)) {
                const count = exercisePoster.get('impactedUsers') % tresholds['impactedUsers'][tresholds['impactedUsers'].length - 1] + 1;
                const badgeToUpdate = checkForbadgeUpdates(count, "impactedUsers");
                if (badgeToUpdate) {
                    exercisePoster.increment(badgeToUpdate);
                }
                exercisePoster.increment("impactedUsers")
                exercisePoster.increment("reputation", 2)
                exercisePoster.save(null, { useMasterKey: true });
            }
        }
    }
}
