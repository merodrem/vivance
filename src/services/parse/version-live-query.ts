

import Parse from 'parse/react-native';
import { APP_ID, JAVASCRIPT_KEY, LIVE_QUERY_URL } from 'src/assets/data/conf';
import store from 'src/store/configureStore';
const VersionLiveQuery = new class {
    private subscription;
    private liveClient = new Parse.LiveQueryClient({
        applicationId: APP_ID,
        serverURL: 'wss://' + LIVE_QUERY_URL,
        javascriptKey: JAVASCRIPT_KEY,
    });
    private query = new Parse.Query('Settings');
    subscribeVersion = this.subscribeVersion.bind(this)
    unsubscribe = this.unsubscribe.bind(this)

    constructor() {
        this.liveClient.open();
    }

    async subscribeVersion() {
        this.query.get("6SKukD8ABi")
        this.query.select("minimalVersion");
        
        this.subscription = this.liveClient.subscribe(this.query);
        this.subscription.on('update', version => {
            store.dispatch({ type: "SET_MINIMAL_VERSION", payload: version.get('minimalVersion') })
        });

    }

    unsubscribe() {
        this.liveClient.close();
    }
}
export default VersionLiveQuery;
