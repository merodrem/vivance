
import Parse from 'parse/react-native';
import { APP_ID, JAVASCRIPT_KEY, LIVE_QUERY_URL } from 'src/assets/data/conf';
import store from 'src/store/configureStore';
const UserLiveQuery = new class {
    private subscription;
    private liveClient = new Parse.LiveQueryClient({
        applicationId: APP_ID,
        serverURL: 'wss://' + LIVE_QUERY_URL,
        javascriptKey: JAVASCRIPT_KEY,
    });
    private query = new Parse.Query('_User');
    subscribeUser = this.subscribeUser.bind(this)
    unsubscribe = this.unsubscribe.bind(this)

    constructor() {
        this.liveClient.open();
    }

    subscribeUser(userID: string) {
        this.query.get(userID)
        this.query.select(
            "goldenBadges",
            "silverBadges",
            "bronzeBadges",
            "reputation",
            "isPremium",
            "profileViewers",
            "impactedUsers",
        );
        this.subscription = this.liveClient.subscribe(this.query);
        this.subscription.on('update', user => {
            store.dispatch({ type: "SET_USER", payload: user.toJSON() })
        });

    }

    unsubscribe() {
        this.liveClient.unsubscribe(this.subscription);
    }
}
export default UserLiveQuery;
