import AsyncStorage from '@react-native-community/async-storage';
import Parse from 'parse/react-native';
import { ExerciseFilter, EXERCISE_PAGE_SIZE } from 'src/assets/data/app.data';
import { APP_ID, JAVASCRIPT_KEY, SERVER_URL } from 'src/assets/data/conf';
import { Avatar } from 'src/models/avatar.model';
import { Exercise } from 'src/models/exercise.model';
import { Note } from 'src/models/note.model';
import { UserState } from 'src/models/user.model';
import { StateOfMind } from '../../models/state-of-mind.model';
import UserLiveQuery from './user-live-query';



Parse.serverURL = SERVER_URL;
Parse.initialize(APP_ID, JAVASCRIPT_KEY);
Parse.setAsyncStorage(AsyncStorage)
Parse.User.enableUnsafeCurrentUser()


/**
 * 
 * @param {*} email 
 * @param {*} password 
 * @returns the new user in JSON
 */
async function signUp(username: string, email: string, password: string) {
    try {
        const result = await Parse.Cloud.run('signUp', { username, email, password });
        UserLiveQuery.subscribeUser(result.id)
        return result.toJSON();
    } catch (error) {
        return Promise.reject(error)
    }
}
/**
 * 
 * @param {*} email 
 * @param {*} password 
 * @returns the JSON encoded user logged in
 */
async function signIn(email: string, password: string) {

    try {
        const user = await Parse.User.logIn(email, password)
        UserLiveQuery.subscribeUser(user.id)
        return user.toJSON()
    } catch (error) {
        return Promise.reject(error)
    }
}


async function logout() {
    try {
        const result = await Parse.User.logOut();
        UserLiveQuery.unsubscribe();
        return result;
    } catch (error) {
        return Promise.reject(error)

    }
}

async function userUpdateSubscribe() {
    const currentUser = await Parse.User.currentAsync();
    if (currentUser) {
        UserLiveQuery.subscribeUser(currentUser.id)
    }
}

async function getProfile(userId: string): Promise<UserState> {
    const query = new Parse.Query("_User");
    return (await query.get(userId)).toJSON()
}

async function changeUser(payload: any) {
    return await Parse.Cloud.run('changeUser', { payload });
}

async function deleteUser() {
    const currentUser = await Parse.User.currentAsync();
    if (currentUser) {
        const result = currentUser.destroy();
        UserLiveQuery.unsubscribe();
        return result;
    }
    else return Promise.reject(new Error('no user logged in. Impossible to delete account'))
}

function changePassword(email: string) {
    return Parse.User.requestPasswordReset(email)
}

async function addStateOfMind(som: StateOfMind): Promise<StateOfMind> {
    return (await Parse.Cloud.run("addStateOfMind", som)).toJSON()
}

async function loadUserStatesOfMind(): Promise<StateOfMind[]> {
    const result = await Parse.Cloud.run("loadUserStatesOfMind")
    return result.map(som => som.toJSON())
}

async function addExercise(exercise: Exercise): Promise<Exercise> {
    return await Parse.Cloud.run("addExercise", { ...exercise })
}

async function getExercisesOverview(filter: ExerciseFilter): Promise<Object> {
    return await Parse.Cloud.run("getExercisesOverview", { filter })
}

async function changeAvatar(avatar: Avatar) {
    return await Parse.Cloud.run('changeAvatar', avatar);
}

async function getExercises(order: ExerciseFilter, category: string, page: number): Promise<Exercise[]> {
    return await Parse.Cloud.run("getExercises", { order, category, page, pageSize: EXERCISE_PAGE_SIZE })
}

async function getExercisesFromUser(): Promise<Exercise[]> {
    const query = new Parse.Query("Exercise");
    const result = await query.find()
    return result.map(exercise => exercise.toJSON())
}

async function getNotes(): Promise<Note[]> {
    const result = await Parse.Cloud.run("getNotes")
    return result.map(note => note.toJSON())
}
async function addNote(note: Note): Promise<Note> {
    return (await Parse.Cloud.run("addNote", { note })).toJSON()
}
async function deleteNote(noteId: string): Promise<string> {
    return (await Parse.Cloud.run("deleteNote", { noteId }))
}
async function editNote(noteId: string, content: string): Promise<Note> {
    return (await Parse.Cloud.run("editNote", { noteId, content })).toJSON()
}
function addFavoriteExercise(fav: string) {
    return Parse.Cloud.run("addFavoriteExercise", { exerciseID: fav })
}

function removeFavoriteExercise(fav: string) {
    return Parse.Cloud.run("removeFavoriteExercise", { exerciseID: fav })
}

async function getFavoriteExercises(exerciseIDs: string[]): Promise<Exercise[]> {
    return Parse.Cloud.run("getFavoriteExercises", { exerciseIDs })
}

function viewProfile(profileViewedID: string) {
    return Parse.Cloud.run("viewProfile", { profileViewedID })
}

function incrementActiveDays() {
    return Parse.Cloud.run("incrementActiveDays")
}

async function voteForExercise(value: -1 | 1, exerciseID: string): Promise<number> {
    return (await Parse.Cloud.run("voteForExercise", { value, exerciseID }));
}

async function deleteExerciseVote(value: -1 | 1, exerciseID: string): Promise<number> {
    return (await Parse.Cloud.run("deleteExerciseVote", { value, exerciseID }));
}

// the SKU is the product ID
async function subscribeToPremium(sku: string): Promise<any> {
    return (await Parse.Cloud.run("subscribeToPremium", { sku }));
}

// the SKU is the product ID
async function loadLanguages(lastLanguageUpdate: string, language: string): Promise<any> {
    return (await Parse.Cloud.run("loadLanguages", { lastLanguageUpdate, language }));
}

async function loadDayInspiration(dayOfYear: string, lang: string): Promise<string> {
    return await Parse.Cloud.run('loadDayInspiration', { dayOfYear: dayOfYear, lang });
}

async function getMinimalVersion(): Promise<string|undefined> {
    const query = new Parse.Query("Settings");
    return (await query.first())?.get("minimalVersion");
}

export {
    signUp,
    signIn,
    logout,
    deleteUser,
    changePassword,
    addStateOfMind,
    loadUserStatesOfMind,
    userUpdateSubscribe,
    addExercise,
    getProfile,
    getExercisesOverview,
    changeAvatar,
    getExercises,
    getExercisesFromUser,
    addNote,
    editNote,
    getNotes,
    deleteNote,
    addFavoriteExercise,
    removeFavoriteExercise,
    getFavoriteExercises,
    viewProfile,
    incrementActiveDays,
    voteForExercise,
    deleteExerciseVote,
    changeUser,
    subscribeToPremium,
    loadLanguages,
    loadDayInspiration,
    getMinimalVersion
};

