import { DrawerActions, useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, TouchableOpacity, View } from "react-native";
import { Icon } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from 'src/services/string.service';
import theme from "src/theme/theme";
import { SET_INSPIRATION } from '../../store/reducers/globalSettings/globalSettings.actions';
import { AppText } from '../util/app-text.component';


const DAYS_OF_WEEK = Object.freeze(["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]);
const MONTHS = Object.freeze(["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "décembre"] as const);


export const HomeComponent: React.FC = () => {
    const navigation = useNavigation();
    const username: string = useSelector(state => state.user.username)
    const now = new Date();
    const [inspiration, setInspiration] = useState(useSelector(state => state.settings.inspiration));
    const [inspirationLoading, setInspirationLoading] = useState(false);
    const inspirationDayOfYear = useSelector(state => state.settings.dayOfYear)
    const dispatch = useDispatch();
    useEffect(() => {
        const now = new Date();
        const dayOfYear = Math.floor((now - new Date(now.getFullYear(), 0, 0)) / 1000 / 60 / 60 / 24);
        
        if (dayOfYear > (inspirationDayOfYear ?? -1)) {
            setInspirationLoading(true)
            ParseServer.loadDayInspiration(dayOfYear, 'fr')
                .then(inspiration => {
                    dispatch({type: SET_INSPIRATION, payload: {dayOfYear, inspiration}})
                    setInspiration(inspiration)
                }
                )
                .catch(error => console.log(error))
                .finally(() => setInspirationLoading(false))
        }
    }, [])

    return (
        <View style={{ flex: 1, width: "90%", alignSelf: "center", justifyContent: 'space-between' }}>
            <View>
                <AppText h2 style={{ paddingBottom: 8, fontWeight: "bold" }}>Bonjour, {username}</AppText>
                <AppText h4 style={{ paddingBottom: 32, fontWeight: "bold" }}>{capitalizeFirstLetter(DAYS_OF_WEEK[now.getDay()]) + " " + now.getDate() + " " + MONTHS[now.getMonth()]}</AppText>
                <View>
                    <AppText h4 style={{ fontWeight: "bold" }}>Inspiration du jour</AppText>
                    <View style={[theme.panelContainer, { padding: 16 }]}>
                        {
                            inspirationLoading ?
                                <ActivityIndicator
                                    color="black"
                                />
                                :
                                <AppText style={{ color: "black" }}>
                                    {inspiration}
                                </AppText>
                        }
                    </View>
                </View>
            </View>

            <View style={{ marginBottom: 32 }}>
                <AppText h4 style={{ fontWeight: "bold" }}>Comment allez vous ?</AppText>
                {/* <ScrollView horizontal showsHorizontalScrollIndicator={false}> */}
                {/* <TouchableOpacity style={theme.darkPanel}>
                        <Icon
                            name="do-not-disturb"
                            type="material-community"
                            size={60}
                            color="black"
                        />
                        <AppText>Poser un question</AppText>
                    </TouchableOpacity> */}
                <TouchableOpacity style={[theme.darkPanel, { width: '100%' }]}
                    onPress={() => {
                        navigation.dispatch(DrawerActions.jumpTo('AddSOM'));
                    }}>
                    <Icon
                        name="emoticon-outline"
                        type="material-community"
                        size={50}
                        color="white"
                    />
                    <AppText style={{ textAlign: "center" }}>Enregistrer une humeur</AppText>
                </TouchableOpacity>
                {/* <TouchableOpacity style={theme.darkPanel}>
                        <Icon
                            name="do-not-disturb"
                            type="material-community"
                            size={60}
                            color="black"
                        />
                        <AppText>Chercher un exercice</AppText></TouchableOpacity> */}
                {/* </ScrollView> */}
            </View>
        </View>
    );
}
