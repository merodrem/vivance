import { View, FlatList, StyleSheet, TouchableOpacity, ActivityIndicator } from "react-native";
import React, { useRef, useState } from 'react';
import { StateOfMind } from "src/models/state-of-mind.model";
import theme from "src/theme/theme";
import { Icon, Header } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { AppText } from "../util/app-text.component";
import { AppButton } from "../util/app-button.component";
import { translate } from "src/services/i18n.service";
import { Note } from "src/models/note.model";
import * as ParseServer from 'src/services/parse/parse.service'
import { ADD_NOTE } from "src/store/reducers/notes/notes.action";
import Toast from 'react-native-simple-toast';
import { AppInput } from "../util/app-input.component";
import { SCREEN_HEIGHT } from "src/assets/data/app.data";
import { NoteCardComponent } from "../util/note-card.component";



type Props = {
    title: string,
    onAddNote: () => void
}
function HeaderComponent(props: Props) {
    const navigation = useNavigation();
    return (
        <Header
            containerStyle={{ backgroundColor: "transparent", borderBottomWidth: 0, height: SCREEN_HEIGHT * 0.1 }}
            leftComponent={<AppButton
                onPress={navigation.goBack}
                type="clear"
                icon={<Icon
                    name="keyboard-arrow-left"
                    color='white'
                    size={35}
                />}
            />}
            centerComponent={{ text: props.title, style: { color: 'white', fontWeight: 'bold', fontSize: 18, fontFamily: "Raleway-Regular" } }}
            rightComponent={<AppButton
                onPress={props.onAddNote}
                type="clear"
                icon={<Icon
                    name="message-plus"
                    type="material-community"
                    size={24}
                    color="white"
                />}
            />}
        />
    );
}

const NotesDiaryComponent: React.FC = () => {
    const notes: Array<Note | StateOfMind> = useSelector(state => state.notes)
    const navigation = useNavigation()
    const [showNoteInput, setShowNoteInput] = useState(false)
    const [loading, setLoading] = useState(false)
    const SOMs: Array<StateOfMind> = useSelector(state => state.SOMs)
    const newNote = useRef('');
    const dispatch = useDispatch();
    return (
        <View style={{ flex: 1 }}>
            <HeaderComponent
                title={translate("diary")}
                onAddNote={() => setShowNoteInput(true)}
            />
            <View style={{}}>
                {showNoteInput &&
                    <>
                        <View style={{ flexDirection: 'row', justifyContent: "flex-end", paddingRight: 8, paddingBottom: 4 }}>
                            <TouchableOpacity
                                onPress={() => setShowNoteInput(false)}
                            >
                                <Icon
                                    name="circle-with-cross"
                                    type="entypo"
                                    size={24}
                                    color="lightsalmon"
                                />
                            </TouchableOpacity>
                            {
                                loading ?
                                    <ActivityIndicator color="white" />
                                    :
                                    <TouchableOpacity
                                        style={{ marginHorizontal: 8 }}
                                        onPress={() => {
                                            setLoading(true);
                                            ParseServer.addNote({
                                                content: newNote.current,
                                                type: "free",
                                                tags: [],
                                                timestamp: new Date().toISOString()
                                            })
                                                .then(note => {
                                                    dispatch({ type: ADD_NOTE, payload: note });
                                                    setShowNoteInput(false);
                                                })
                                                .catch(error => Toast.show("Impossible d'ajouter la note: " + error, Toast.LONG))
                                                .finally(() => setLoading(false))
                                        }
                                        }
                                    >
                                        <Icon
                                            name="check-circle"
                                            type="font-awesome"
                                            size={24}
                                            color="white"
                                        />
                                    </TouchableOpacity>
                            }
                        </View>
                        <AppInput
                            inputContainerStyle={theme.darkInputContainer}
                            inputStyle={theme.darkInput}
                            multiline={true}
                            placeholderTextColor='white'
                            returnKeyType="done"
                            autoCapitalize="none"
                            textAlignVertical='top'
                            placeholder="Ajouter une note..."
                            onChangeText={text => newNote.current = text}
                        />
                    </>
                }
                {notes.length > 0 ?
                    <>
                        <FlatList
                            data={notes.concat(SOMs).sort((a, b) => (a.timestamp < b.timestamp))}
                            renderItem={({ item }) =>
                                <View style={{marginBottom:16}}>
                                    <NoteCardComponent {...item} />
                                </View>
                            }
                            showsVerticalScrollIndicator={false}
                            style={{ maxHeight: SCREEN_HEIGHT * 0.9 }}
                            contentContainerStyle={{ width: "90%", alignSelf: "center", paddingBottom: 32 }}
                            keyExtractor={item => (item.timestamp)}
                        />
                    </>
                    :
                    <>
                        <AppText h3 style={[theme.headerText, { color: 'white', textAlign: "center" }]}>
                            Le journal est vide!
            </AppText>
                        <AppButton
                            containerStyle={{ alignSelf: 'center' }}
                            buttonStyle={{ paddingHorizontal: 16, paddingVertical: 8, marginTop: 32, borderColor: 'white' }}
                            titleStyle={{ color: 'white' }}
                            type="outline"
                            title={"hop hop"}
                            icon={<Icon
                                name="chevron-left"
                                type="feather"
                                size={24}
                                color="white"
                            />
                            }
                            onPress={() => navigation.navigate("Home")}
                        />
                    </>
                }
            </View>
        </View>
    );
}
export default NotesDiaryComponent

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        justifyContent: 'center',
        backgroundColor: 'transparent'
    }

});