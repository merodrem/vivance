
import React, { ReactChildren } from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { ModalBaseProps } from 'src/models/modalBaseProps.model';


type Props = {
    children?: ReactChildren
} & ModalBaseProps

function InfoModalComponent(props: Props) {
    return (
        <Modal deviceWidth={SCREEN_WIDTH}
            isVisible={props.isVisible}
            deviceHeight={SCREEN_HEIGHT}
            backdropColor="black"
            onBackButtonPress={props.onHide}
            onBackdropPress={props.onHide}
            hideModalContentWhileAnimating={true}
            >
            <View style={{backgroundColor:"white", borderRadius:15, padding:16}}>
                {props.children}
            </View>
        </Modal>
    );
}



export default InfoModalComponent