
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { ModalBaseProps } from 'src/models/modalBaseProps.model';
import theme from 'src/theme/theme';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { AppButton } from '../util/app-button.component';
import { AppText } from '../util/app-text.component';


type Props = {
    onRedirect: (screen: string) => void,
} & ModalBaseProps

function RedirectModalComponent(props: Props) {
    return (
        <Modal deviceWidth={SCREEN_WIDTH}
            isVisible={props.isVisible}
            deviceHeight={SCREEN_HEIGHT}
            backdropColor="black"
            hideModalContentWhileAnimating={true}
            >
            <View style={{}}>
                <View style={[theme.panelContainer, {padding:16}]}>
                    <AppText style={{color:'black'}}>Comment te sens tu à présent ?</AppText>
                    <AppText style={{ paddingBottom: 8, color:'black' }}>As tu besoin dʼautre chose pour te sentir apaisé ?</AppText>
                    <AppText style={{color:'black'}}>Tu trouvera des exercices adaptés à ta situation pour
aller plus loin, si tu en ressens le besoin.</AppText>
                </View>
                <View style={{flexDirection:'row', justifyContent:"space-around"}}>
                <AppButton
                    title="Voir les exercices"
                    containerStyle={{ borderRadius: 5 }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 0, }}
                    onPress={()=>props.onRedirect("Exercises")}
                />
                <AppButton
                    title="Tableau de bord"
                    containerStyle={{ borderRadius: 5 }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 0, }}
                    onPress={()=>props.onRedirect("Dashboard")}
                />
                </View>
            </View>
        </Modal>
    );
}



export default RedirectModalComponent