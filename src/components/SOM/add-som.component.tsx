import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useRef, useState } from 'react';
import { FlatList, ImageBackground, LayoutAnimation, ScrollView, StyleSheet, TouchableOpacity, UIManager, View } from "react-native";
import { Icon, Image } from 'react-native-elements';
import Toast from 'react-native-simple-toast';
import { useDispatch } from 'react-redux';
import { BG_IMAGE, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { CNV_EMOTIONS, CNV_NEEDS, CNV_SITUATIONS, EMOTION_CATEGORIES, NEED_CATEGORIES } from 'src/assets/data/cnv.data';
import { Feeling } from 'src/models/state-of-mind.model';
import { getColor } from 'src/services/color.service';
import { translate } from 'src/services/i18n.service';
import { intensityTranslation } from 'src/services/need.service';
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from 'src/services/string.service';
import { ADD_SOM } from 'src/store/reducers/StatesOfMind/states-of-mind.action';
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from 'src/theme/variables';
import { INCREMENT_SOM_COUNT, INCREMENT_TAG_COUNT } from '../../store/reducers/user/user.action';
import { AppButton } from '../util/app-button.component';
import { AppInput } from '../util/app-input.component';
import { AppText } from '../util/app-text.component';
import GenericHeaderComponent from '../util/generic-header.component';
import { IntensitySliderComponent } from '../util/intensity-slider.component';
import { IntensityTextComponent } from '../util/intensity-text';
import InfoModalComponent from './info-modal.component';
import RedirectModalComponent from './redirect-modal.component';



function getSituationIcon(type: string) {
    switch (type) {
        case "work":
            return {
                name: "work",
                type: "material"
            }
        case "school":
            return {
                name: "book",
                type: "entypo"
            }
        case "nature":
            return {
                name: "trees",
                type: "foundation"
            }
        case "home":
            return {
                name: "home",
                type: "material"
            }
        case "relation":
            return {
                name: "handshake",
                type: "font-awesome-5"
            }
        case "family":
            return {
                name: "family-restroom",
                type: "material-icons"
            }
        case "fellow":
            return {
                name: "group",
                type: "font-awesome"
            }
        case "medias":
            return {
                name: "newspaper-outline",
                type: "ionicon"
            }
        case "music":
            return {
                name: "music",
                type: "foundation"
            }
        case "selfTime":
            return {
                name: "user-clock",
                type: "font-awesome-5"
            }
        case "sleep":
            return {
                name: "bed",
                type: "font-awesome"
            }
        case "sport":
            return {
                name: "running",
                type: "font-awesome-5"
            }
        case "health":
            return {
                name: "heartbeat",
                type: "font-awesome"
            }
        case "food":
            return {
                name: "food-apple",
                type: "material-community"
            }
        case "activity":
            return {
                name: "activity",
                type: "feather"
            }
        case "other":
            return {
                name: "questioncircle",
                type: "antdesign"
            }
        default:
            return {
                name: "questioncircle",
                type: "antdesign"
            };
    }
}

function getIcon(cat: string) {
    switch (cat) {
        case "fear":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/fear.png")}
            />
        case "surprise":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/surprise.png")}
            />
        case "disgust":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/disgust.png")}
            />
        case "anger":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/anger.png")}
            />
        case "sadness":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/sadness.png")}
            />
        case "vigilence":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/vigilence.png")}
            />
        case "joy":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/joy.png")}
            />
        case "peace":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/peace.png")}
            />
        case "survival":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/survival.png")}
            />
        case "rea":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/rea.png")}
            />
        case "com":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/com.png")}
            />
        case "wellbeing":
            return <Image
                style={{ width: 28, height: 28 }}
                source={require("src/assets/img/wellbeing.png")}
            />
        case "rel":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/rel.png")}
            />
        case "spi":
            return <Image
                style={{ width: 24, height: 24 }}
                source={require("src/assets/img/spi.png")}
            />
    }
}


const MAX_EMO_LENGTH = 2

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

type Props = {
    onDelete?: () => void,
    onPress?: () => void,
    selected?: boolean,
    type: 'emotion' | 'need',
} & Feeling
function IntensityBubbleComponent(props: Props) {
    return (
        <View style={{ flexDirection: 'row', justifyContent: "space-around", alignItems: "center" }}>
            {
                props.type === 'emotion' ?
                    <>
                        <TouchableOpacity
                            onPress={props.onPress}
                            style={[theme.darkPanel, { width: "80%", marginVertical: 2 }, props.selected && { backgroundColor: 'rgba(255,255,255,0.5)' }]}>
                            <AppText
                                key={props.name}>
                                Je me sens
            <IntensityTextComponent
                                    intensity={Math.round((props.intensity / 100) * (intensityTranslation.length - 1))}
                                >
                                    {intensityTranslation[Math.round((props.intensity / 100) * (intensityTranslation.length - 1))].label.toLowerCase()}
                                </IntensityTextComponent>
                                {translate(props.name).toLowerCase()}
                            </AppText>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={props.onDelete}
                        >
                            <Icon
                                name="trash"
                                type="font-awesome-5"
                                color="white"
                            />
                        </TouchableOpacity>
                    </>
                    :
                    <View style={theme.darkPanel}>
                        <AppText style={{ paddingVertical: 2 }}>
                            Car j'ai
                    <IntensityTextComponent
                                intensity={Math.round((props.intensity / 100) * (intensityTranslation.length - 1))}
                            >
                                {intensityTranslation[Math.round((props.intensity / 100) * (intensityTranslation.length - 1))].label.toLowerCase()}
                            </IntensityTextComponent>
                        besoin {translate(props.name).match('^[aieouAIEOU].*') ? 'd\'' : 'de '}{translate(props.name).toLowerCase()}
                        </AppText>
                    </View>
            }
        </View>
    );
}

const AddSOMComponent: React.FC = () => {
    const [selectedSituation, setSelectedSituation] = useState<string>('')
    const [selectedEmotionCategory, setSelectedEmotionCategory] = useState<string>('')
    const [selectedEmotions, setSelectedEmotions] = useState<Feeling[]>([])
    const [selectedNeed, setSelectedNeed] = useState<Feeling | undefined>()
    const [selectedNeedCategory, setSelectedNeedCategory] = useState<string>('')
    const [isRedirectModalVisible, setIsRedirectModalVisible] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [emotionPointer, setEmotionPointer] = useState<string | undefined>(undefined)//name of the emotion the slider is pointing to

    const detail = useRef<string>('')
    const note = useRef<string>('')
    const [showSituationInfo, setShowSituationInfo] = useState(false)
    const [showEmotionInfo, setShowEmotionInfo] = useState(false)
    const [showNeedInfo, setShowNeedInfo] = useState(false)
    const [showObservationsInfo, setShowObservationsInfo] = useState(false)

    const dispatch = useDispatch();
    const navigation = useNavigation();

    useEffect(() => {
        LayoutAnimation.easeInEaseOut();
    }, []);
    return (
        <ImageBackground
            source={BG_IMAGE}
            style={theme.bgImage}
        >
            <InfoModalComponent
                onHide={() => setShowSituationInfo(false)}
                isVisible={showSituationInfo}
            >
                <AppText style={{ color: 'black', paddingBottom: 8 }}>Tu vis une situation difficile ? Ne tʼen fais pas, cʼest OK.
                Ce petit exercice dʼintrospection va tʼaider à prendre
                conscience de ton émotion.
                </AppText>
                <AppText style={{ color: 'black', paddingBottom: 8 }}>Ici, quelle est la situation vécue ? Se positionner en
                observateur de la situation que tu vis va te permettre de
prendre du recul.</AppText>
                <AppText style={{ color: 'black', paddingBottom: 8 }}>Tu peux pour se faire la décrire la manière objective, en
décrivant les fait et les actions uniquement.</AppText>
            </InfoModalComponent>
            <InfoModalComponent
                onHide={() => setShowEmotionInfo(false)}
                isVisible={showEmotionInfo}
            >
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    L'émotion que tu vis actuellement est une information envoyée par ton "ordinateur interne". Elle est créée par ton cerveau pour attirer ton attention sur quelque chose d'important.
                </AppText>
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    Ferme les yeux et ressens l'émotion qui te traverse. Quelle est-elle ? Pour t'aider à écouter cette émotion, tu peux la décrire plus précisément et lui donner une intensité.
                </AppText>

            </InfoModalComponent>
            <InfoModalComponent
                onHide={() => setShowNeedInfo(false)}
                isVisible={showNeedInfo}
            >
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    Un besoin est un élément essentiel à ton être. Comme l'eau, le sommeil, l'attention, le respect ou l'estime de soi. L'émotion que tu ressens vient t'alerter qu'un de tes besoins est comblé menacé. 
                </AppText>
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    Peux tu ressentir à l'intérieur de toi ce que tu aurais aimé vivre dans cette situation ? 
                </AppText>
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    Peux tu ressentir à l'intérieur de toi à quel point il est important pour toi de vivre ce besoin ?
                </AppText>
            </InfoModalComponent>
            <InfoModalComponent
                onHide={() => setShowObservationsInfo(false)}
                isVisible={showObservationsInfo}
            >
                <AppText style={{ color: 'black', paddingBottom: 8 }}>
                    Ici tu peux ajouter des notes, pensées, ou tout ce que tu as besoin d'exprimer. Tu peux voir ça comme un journal intime. Tu retrouveras tes notes dans le journal.
                </AppText>

            </InfoModalComponent>
            <GenericHeaderComponent
                title="Ajouter une humeur"
            />
            <ScrollView
                nestedScrollEnabled
                showsVerticalScrollIndicator={false}
            >
                <View style={[styles.borderedComponent, { flexDirection: 'row', alignItems: 'center' }]}>
                    <AppText h4 style={{ fontWeight: 'bold' }}>Situation</AppText>
                    <TouchableOpacity
                        style={{ height: 32, width: 32, alignItems: "center", justifyContent: "center" }}
                        onPress={() => setShowSituationInfo(true)}
                    >
                        <Icon
                            containerStyle={{ paddingLeft: 16 }}
                            name="questioncircle"
                            type="antdesign"
                            color="white"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal
                >
                    {
                        CNV_SITUATIONS.map(situation =>
                            <TouchableOpacity
                                key={situation}
                                style={{ margin: 8, alignItems: 'center' }}
                                onPress={() => setSelectedSituation(situation)}
                            >
                                <Icon
                                    reverse
                                    iconStyle={{ color: situation == selectedSituation ? 'white' : 'black' }}
                                    name={getSituationIcon(situation).name}
                                    type={getSituationIcon(situation).type}
                                    color={situation == selectedSituation ? PRIMARY_COLOR : 'white'}
                                />
                                <AppText>{translate(situation)}</AppText>
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
                <AppInput
                    inputContainerStyle={[styles.borderedComponent, theme.darkInputContainer]}
                    inputStyle={theme.darkInput}
                    multiline={true}
                    placeholderTextColor='white'
                    returnKeyType="done"
                    autoCapitalize="none"
                    textAlignVertical='top'
                    placeholder="Ajouter des détails..."
                    onChangeText={text => detail.current = text}
                />
                <View style={[styles.borderedComponent, { flexDirection: 'row', alignItems: 'center' }]}>
                    <AppText h4 style={{ fontWeight: 'bold', marginBottom: 4 }}>Émotions ({selectedEmotions.length}/2)</AppText>
                    <TouchableOpacity
                        style={{ height: 32, width: 32, alignItems: "center", justifyContent: "center" }}
                        onPress={() => setShowEmotionInfo(true)}
                    >
                        <Icon
                            containerStyle={{ paddingLeft: 16 }}
                            name="questioncircle"
                            type="antdesign"
                            color="white"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal
                >
                    {
                        EMOTION_CATEGORIES.map(cat =>
                            <TouchableOpacity
                                key={cat}
                                style={{ margin: 8, alignItems: 'center' }}
                                onPress={() => setSelectedEmotionCategory(cat)}
                            >
                                <View
                                    style={{ width: 50, height: 50, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: (cat == selectedEmotionCategory) ? getColor(cat) : 'white' }}
                                >
                                    {getIcon(cat)}
                                </View>
                                <AppText>{capitalizeFirstLetter(translate(cat))}</AppText>
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
                {
                    selectedEmotions.map(emotion =>
                        <IntensityBubbleComponent
                            key={emotion.name}
                            {...emotion}
                            type="emotion"
                            onPress={() => setEmotionPointer(emotion.name)}
                            selected={emotion.name == emotionPointer}
                            onDelete={() => {
                                const newSelecetedEmotions = selectedEmotions.filter(emo => emo.name != emotion.name)
                                setSelectedEmotions(newSelecetedEmotions)

                                if (emotion.name == emotionPointer && newSelecetedEmotions.length > 0) {
                                    setEmotionPointer(newSelecetedEmotions[newSelecetedEmotions.length - 1].name);
                                }
                            }
                            }
                        />
                    )
                }
                <FlatList
                    scrollEnabled={false}
                    contentContainerStyle={{ width: "90%", alignSelf: "center" }}
                    columnWrapperStyle={{ justifyContent: 'space-around', alignItems: 'flex-end' }}
                    extraData={setSelectedEmotions}
                    data={CNV_EMOTIONS.filter(emo => emo.category == selectedEmotionCategory)}
                    numColumns={2}
                    renderItem={({ item }) =>
                        <AppButton
                            title={translate(item.emotion)}
                            titleStyle={{ color: (selectedEmotions.map(emo => emo.name).includes(item.emotion)) ? PRIMARY_COLOR : 'white', fontSize: 14 }}
                            buttonStyle={{ width: SCREEN_WIDTH / 3, height: 40, backgroundColor: (selectedEmotions.map(emo => emo.name).includes(item.emotion)) ? 'white' : PRIMARY_COLOR, marginVertical: 2 }}
                            onPress={() => {
                                if (selectedEmotions.map(emo => emo.name).includes(item.emotion)) {
                                    const newSelecetedEmotions = selectedEmotions.filter(emo => emo.name != item.emotion);
                                    setSelectedEmotions(newSelecetedEmotions)

                                    if (item.emotion == emotionPointer && newSelecetedEmotions.length > 0) {
                                        setEmotionPointer(newSelecetedEmotions[newSelecetedEmotions.length - 1].name);
                                    }
                                }
                                else if (selectedEmotions.length < MAX_EMO_LENGTH) {
                                    const newEmotions = [...selectedEmotions];
                                    newEmotions.push({ name: item.emotion, category: selectedEmotionCategory ?? "N.C.", intensity: 10 });
                                    setSelectedEmotions(newEmotions)
                                    setEmotionPointer(item.emotion)
                                }
                            }
                            }
                        />
                    }
                    keyExtractor={item => item.emotion}
                    ListFooterComponent={
                        <>
                            {
                                selectedEmotions.length > 0 &&
                                <View style={{ alignItems: "center", justifyContent: "space-around" }}>
                                    <IntensitySliderComponent
                                        type='emotion'
                                        intensity={(selectedEmotions.find(emo => emo.name == emotionPointer).intensity - 10) / 90}
                                        onChange={value => {
                                            const roundedIntensity = 10 + Math.round(value * 90);
                                            const indexOfPointer = selectedEmotions.findIndex(emo => emo.name == emotionPointer)
                                            if (roundedIntensity !== selectedEmotions[indexOfPointer].intensity) {
                                                const newSelectedEmotions = [...selectedEmotions]
                                                newSelectedEmotions[indexOfPointer].intensity = roundedIntensity
                                                setSelectedEmotions(newSelectedEmotions)
                                            }

                                        }
                                        }
                                    />
                                </View>
                            }
                        </>
                    }
                />


                <View style={[styles.borderedComponent, { flexDirection: 'row', alignItems: 'center', marginTop: 16 }]}>
                    <AppText h4 style={{ fontWeight: 'bold', marginBottom: 4 }}>Besoin</AppText>
                    <TouchableOpacity
                        style={{ height: 32, width: 32, alignItems: "center", justifyContent: "center" }}
                        onPress={() => setShowNeedInfo(true)}
                    >
                        <Icon
                            containerStyle={{ paddingLeft: 16 }}
                            name="questioncircle"
                            type="antdesign"
                            color="white"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>


                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal
                >
                    {
                        NEED_CATEGORIES.map(cat =>
                            <TouchableOpacity
                                key={cat}
                                style={{ margin: 8, alignItems: 'center' }}
                                onPress={() => setSelectedNeedCategory(cat)}
                            >
                                <View
                                    style={{ width: 50, height: 50, borderRadius: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: (cat == selectedEmotionCategory) ? getColor(cat) : 'white' }}
                                >
                                    {getIcon(cat)}
                                </View>
                                <AppText >{translate(cat)}</AppText>
                            </TouchableOpacity>
                        )
                    }
                </ScrollView>
                {
                    selectedNeed &&
                    <IntensityBubbleComponent
                        type='need'
                        {...selectedNeed}

                    />
                }
                <FlatList
                    scrollEnabled={false}
                    contentContainerStyle={{ width: "90%", alignSelf: "center" }}
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    extraData={setSelectedEmotions}
                    data={CNV_NEEDS.filter(need => need.category == selectedNeedCategory)}
                    numColumns={2}
                    renderItem={({ item }) =>
                        <AppButton
                            title={translate(item.need)}
                            titleStyle={{ color: (selectedNeed?.name === item.need) ? PRIMARY_COLOR : 'white', fontSize: 14 }}
                            buttonStyle={{ width: SCREEN_WIDTH * 0.4, height: 40, backgroundColor: (selectedNeed?.name === item.need) ? 'white' : PRIMARY_COLOR, marginVertical: 2 }}
                            onPress={() => {
                                if (selectedNeed?.name !== item.need) {
                                    setSelectedNeed({ name: item.need, category: selectedNeedCategory ?? "N.C.", intensity: 10 })
                                }
                                else {
                                    setSelectedNeed(undefined)
                                }
                            }
                            }
                        />
                    }
                    keyExtractor={item => item.need}
                    ListFooterComponent={
                        <>
                            {
                                selectedNeed &&
                                <View style={{ width: "100%", alignItems: "center", justifyContent: "space-around", }}>
                                    <IntensitySliderComponent
                                        intensity={(selectedNeed.intensity - 10) / 90}
                                        onChange={value => {
                                            const roundedIntensity = 10 + Math.round(value * 90);
                                            if (roundedIntensity !== selectedNeed.intensity) {
                                                const newSelectedneed = { ...selectedNeed }
                                                newSelectedneed.intensity = roundedIntensity
                                                setSelectedNeed(newSelectedneed)
                                            }

                                        }
                                        }
                                        thumbTintColor={PRIMARY_COLOR}
                                    />
                                </View>
                            }
                        </>
                    }
                />




                <View style={[styles.borderedComponent, { flexDirection: 'row', alignItems: 'center', marginTop: 16 }]}>
                    <AppText h4 style={{ fontWeight: 'bold', marginBottom: 12 }}>Observations</AppText>
                    <TouchableOpacity
                        style={{ height: 32, width: 32, alignItems: "center", justifyContent: "center" }}
                        onPress={() => setShowObservationsInfo(true)}
                    >
                        <Icon
                            containerStyle={{ paddingLeft: 16 }}
                            name="questioncircle"
                            type="antdesign"
                            color="white"
                            size={16}
                        />
                    </TouchableOpacity>
                </View>
                <AppInput
                    inputContainerStyle={[styles.borderedComponent, theme.darkInputContainer]}
                    inputStyle={theme.darkInput}
                    multiline={true}
                    placeholderTextColor='white'
                    returnKeyType="done"
                    autoCapitalize="none"
                    textAlignVertical='top'
                    placeholder="Ajouter une note à cette humeur..."
                    onChangeText={text => note.current = text}
                />

                <AppButton
                    title="Terminer"
                    containerStyle={[styles.borderedComponent, styles.nextbuttonContainer]}
                    buttonStyle={styles.outlineButton}
                    loading={isLoading}
                    titleStyle={{ color: 'white' }}
                    disabled={!selectedNeed || !selectedSituation || selectedEmotions.length == 0 || selectedEmotions.length > MAX_EMO_LENGTH}
                    onPress={() => {
                        setLoading(true);
                        ParseServer.addStateOfMind({
                            situation: {
                                type: selectedSituation,
                                details: detail.current
                            },
                            note: note.current,
                            emotions: selectedEmotions,
                            need: selectedNeed,
                            timestamp: new Date().toISOString(),
                            tags: [...new Set(selectedEmotions.map(emo => emo.category)), selectedNeed.category]
                        })
                            .then((som) => {
                                dispatch({ type: ADD_SOM, payload: som });
                                setIsRedirectModalVisible(true)
                                setSelectedSituation('')
                                setSelectedEmotionCategory('')
                                setSelectedEmotions([])
                                setSelectedNeed(undefined)
                                setSelectedNeedCategory('')
                                setEmotionPointer(undefined)
                                dispatch({ type: INCREMENT_SOM_COUNT})
                                dispatch({ type: INCREMENT_TAG_COUNT, payload: som.tags.length})
                                detail.current = '';
                                note.current = '';
                            })
                            .catch(error => {
                                Toast.show("Impossible de sauvegarder l'état d'esprit: " + error.message, Toast.LONG)
                            })
                            .finally(() => setLoading(false))

                    }}
                />
                <RedirectModalComponent
                    isVisible={isRedirectModalVisible}
                    onHide={() => setIsRedirectModalVisible(false)}
                    onRedirect={(screen) => {
                        setIsRedirectModalVisible(false);
                        navigation.navigate(screen)
                    }
                    }
                />
            </ScrollView>
        </ImageBackground >
    );
}

export default AddSOMComponent

const styles = StyleSheet.create({
    outlineButton: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderRadius: 50,
        backgroundColor: PRIMARY_COLOR
    },
    nextbuttonContainer: {
        marginBottom: 16
    },
    borderedComponent: {
        width: "90%",
        alignSelf: "center"
    }
});