import { View, StyleSheet, ImageBackground, ScrollView } from "react-native";
import React from 'react';
import GenericHeaderComponent from "../util/generic-header.component";
import { AppText } from "../util/app-text.component";
import { translate } from "src/services/i18n.service";
import { useRoute } from "@react-navigation/native";
import { BG_IMAGE } from "src/assets/data/app.data";
import theme from "../../theme/theme";

const TEXT = {
    "rejection": {
        "bubble1":
`Ta blessure principale semble être une blessure de Rejet. Il est possible que tu portes le masque du Fuyant. Ton masque est un automatisme mental que tu as développé pour te protéger d’une situation vécue comme un rejet dans ton enfance. 

Lorsque ta blessure est activée, tu aura tendance à vouloir fuir la situation ou la personne que tu pensera être à l’origine du rejet, par peur de paniquer, de perdre le contrôle, de te sentir impuissant(e).

Ce masque peut aussi te convaincre d’être le plus invisible possible en te retirant à l’intérieur de toi même, en restant passif, ne disant ou ne faisant rien qui puisse créer d’avantage de rejet par l’autre. 

Ce masque te fait croire que tu n’es pasassez important pour prendre ta place, et que tu n’as pas le droit d’exister au même titre que les autres.`,
        "bubble2": `Si tu souffres d'une blessure de rejet, tu alimentes et entretiens ta blessure à chaque fois que tu fuis une situation qui te dérange ou qui te mets mal à l'aise. De la même manière, à chaque fois que tu te rabaisses, te considère comme pas à la hauteur ou nul. 

Tu aura tendance à te donner l'impression que tu t'occupes bien des autres et de toi même pour ne pas voir les différents rejets que tu traverses. `,
        "bubble3": `Ta blessure de rejet et ton masque de fuyant est en voie de guérison à chaque fois que tu oses prendre ta place et que tu t'imposes dans l'espace, que tu oses t'affirmer et prendre des risques. 

Tu deviens capable de percevoir et de te donner de la valeur. Quand bien même quelqu'un semble te rejeter, tu n'es pas émotionnellement impacté et tu sais faire la part des choses entre ce qui est de ton fait et ce qui est de son éxpérience. 

Peu importe la situation, tu restes ancré et connecté à qui tu es.`
    },
    "abandonment": {
        "bubble1": `Ta blessure principale semble être une blessure d'Abandon. Ainsi, tu portes le masque du dépendant. Ce masque est un shéma mental que tu as développé pour te protéger d'une situation vécue comme un abandon dans ton enfance. 

Lorsque ta blessure se manifeste, tu redeviendra un enfant qui a besoin d'attention et de soutien. Pour l'obtenir, tu auras tendance à utiliser le chagrin, à te plaindre ou à te positionner en tant que victime face à une situation. 

Tu ne penses pas être capable d'arriver seul à surmonter les épreuves de la vie. Ce masque te fera tout fait pour avoir d'avantage d'attention, ou éviter d'être laissé de coté par les autres. Il t'est même possible de créer des maladies ou de te créer des problèmes pour obtenir le support ou le soutien que tu recherches. `,
        "bubble2": `Si tu souffres d'une blessure d'Abandon, tu alimentes ta blessure à chaque fois que tu laisses tomber un projet qui te tient à coeur par manque d'investisement envers toi même. 

De la même, à chaque fois que tu ne prends pas assez soin de toi, que tu ne te donnes pas l'attention dont tu as besoin ou que tu n'écoutes pas tes besoins, tu entretiens le shéma de la dépendance vis à vis des autres. 

Lorsque tu crées des relations de dépendances, qu'elles soient de nature amoureuse ou amicale, tu entres à nouveau dans ta blessure et te prépares à un prochain abandon. `,
        "bubble3": `Ta blessure d'abandon et ton masque de dépendant est en voie de guérison à chaque fois que tu prends du temps pour écouter et répondre à tes besoins. 

Lorsque tu te sens bien avec toi même et que tu n'as pas besoin de compenser une solutide par de la nourriture ou toute autre dépendance. Tu verras la vie de manière moins dramatique et aura plus d'énergie pour entreprendre des projets qui te parlent, même sans le soutien des autres. `,
    },
    "injustice": {
        "bubble1": `Ta blessure principale semble être une blessure d'Injustice. Ainsi, tu portes le masque du rigide. Ce masque est un shéma mental que tu as développé pour te protéger d'une situation vécue comme une injustice dans ton enfance. 

Lorsque ta blessure est activée, tu deviens une personne froide et distante, aussi bien sur la gestuelle que dans la manière de t'exprimer. 

Ta blessure te pousse à être perfectionniste et à ressentir de la colère, de l'impatience ou à te montrer très exigeant envert les autres. Tu as aussi tendance à  te contrôler, te retenir de t'exprimer pleinement face à certaines situations et être très exigeant envers toi même. `,
        "bubble2": `Si tu souffres d'une blessure d'injustice, tu alimentes ta blessure à chaque fois que tu te montres trop exigeant envers toi même ou envers les autres. Tu as tendance à ne pas respecter tes limites et à te créer beaucoup de stress en étant dans l'action. 

Lorsque tu es injuste envers toi même sans t'en rendre compte, lorsque tu ressens une émotion négative quand un résultat n'est pas comme tu l'espérais ou que tu te focalises sur les erreurs qui ont été comises plutôt que sur le positif, tu permets à ta blessure de continuer à exister`,
        "bubble3": `Ta blessure d'injustice et ton masque de rigide est en voie de guérison lorsque tu te permets d'être moins perfectionniste et moins regardant sur les détails. 

Tu es capable de voir tes erreurs et celle des autres sans réagir émotionnellement et sans envie de les rendre "juste". Tu as également plus de facilité à montrer tes émotions et ta sensibilité sans te juger et sans avoir peur du jugement d'autrui. 

Lorsque tu es face à un évènemet que tu estimes "injuste", tu as la capacité de l'exprimer simplement sans te mettre en colère.`,
    },
    "humiliation": {
        "bubble1": `Ta blessure principale semble être une blessure d'Humiliation. Ainsi, tu portes le masque du masochiste. Ce masque est un shéma mental que tu as développé pour te protéger d'une situation vécue comme une humiliation dans ton enfance. 

Lorsque ta blessure est activée, tu oubliera totalement tes propres besoins pour devenir une bonne personne, généreuse, toujours prête à rendre service, même au dela de tes propres limites. 

Tu fera tout pour te rendre utile, pour ne pas te sentir rabaissé. Ainsi, tu t'arrangera pour ne pas être libre, alors qu'il s'agit pourtant de quelque chose d'important pour toi. Dans ta blessure, tes actions sont motivées par la peur d'avoir honte ou de se sentir humilié, de ne pas être à la hauteur. `,
        "bubble2": `Si tu souffres d'une blessure d'humiliation, tu alimentes ta blessure à chaque fois que tu te rabaisses, que tu te compares aux autres ou que tu émets un jugement envers toi même. 

Peut-être aura tu tendance à te dire que tout ce que tu fais pour les autres te fait plaisir, néanmoins en faisant ainsi, tu n'es pas dans l'écoute de tes besoins profonds. 

Lorsque tu prends la responsabilité des autres sur ton dos, tu te prives de liberté et de temps pour toi même. `,
        "bubble3": `Ta blessure d'humilliation et ton masque de masochiste est en bonne voie de guérison lorsqu'avant de répondre à une demande de quelqu'un, tu pends le temps de vérifier tes besoins personnels avant de dire oui à l'autre. 

Tu es également capable de dire non aux autres sans te sentir coupable de ne pas l'aider. Tu es par ailleurs capable de demander des choses aux autres sans ressentir de la culpabilité ou de la gène. `,
    },
    "betrayal": {
        "bubble1": `Ta blessure principale semble être une blessure de Trahison. Ainsi, tu portes le masque du controlant. Ce masque est un shéma mental que tu as développé pour te protéger d'une situation vécue comme une trahison dans ton enfance. 

Lorsque ta blessure se manifeste, tu deviens méfiant, sceptique, sur tes gardes ou autoritaire en raison de tes attentes envers les autres. Tu feras tout pour montrer que tu es une personne forte; capable et compétente, pour ne pas montrer ta vulnérabilité, jusqu'a mentir pour ne pas perdre la face. 

Tu as tendance à oublier tes besoins pour aider les autres de sorte qu'ils te reconnaissent comme une personne fiable, sur laquelle ils peuvent compter, en laquelle on peut avoir confiance. 

Tu projetes l'image d'une personne sûre d'elle, quand bien même tu n'as pas confiance en toi et que tu as tendance à remettre en question tes décisions et tes actions. `,
        "bubble2": `Si tu souffres d'une blessure de trahison, tu entretiens ta blessure à chaque fois que tu n'es pas totalement honnète avec toi même ou avec les autres. 

Lorsque tu ne tiens pas tes engagement envers toi même et tu ne t'en rends pas compte ou que tu souhaites tout réaliser par toi même pour ne pas risquer d'être décu ou trahi par les autres. 

S'il t'arrive de déléguer aux autres, à chaque fois que tu vérifies ou contrôle leurs actions, tu permets à ta blessure de prendre plus de place en toi. `,
        "bubble3": `Ta blessure de trahison et ton masque de contrôlant est en voie de guérison lorsque tu cesses de chercher à contrôler les réactions des autres et leurs actes. 

Ne ressens plus d'émotion aussi forte lorsque quelque chose ou quelqu'un vient déranger les plans que tu as imaginé. Tu entres dans le lacher prise et l'accueil de ce qui vient à toi. 

Par ailleurs, tu ne cherches plus à être le centre d'attention et à montrer que tu es capable. Lorsque tu réalises quelque chose, tu es fier de toi même sans l'approbation des personnes que tu estimes. `,
    },
}
function WoundDetailComponent() {
    const route = useRoute();
    const wound = route.params.wound;

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

            <GenericHeaderComponent
                title={translate(`${wound}Title`)}
            />
            <ScrollView style={{ width: "90%", alignSelf: "center" }}>
                <AppText h4 style={{ fontWeight: "bold" }}>Caractère & comportement</AppText>
                <View style={[theme.panelContainer, {marginBottom:24}]}>
                    <AppText style={{ color: "black" }}>
                        {TEXT[wound].bubble1}
                    </AppText>
                </View>
                <AppText h4 style={{ fontWeight: "bold" }}>Comment entretenir la blessure</AppText>
                <View style={[theme.panelContainer, {marginBottom:24}]}>
                    <AppText style={{ color: "black" }}>
                        {TEXT[wound].bubble2}
                    </AppText>
                </View>
                <AppText h4 style={{ fontWeight: "bold" }}>Comment guérir la blessure</AppText>
                <View style={[theme.panelContainer, {marginBottom:24}]}>
                    <AppText style={{ color: "black" }}>
                        {TEXT[wound].bubble3}
                    </AppText>
                </View>
            </ScrollView>
        </ImageBackground>
    );
}


export default WoundDetailComponent

const styles = StyleSheet.create({

});
