
import Modal from 'react-native-modal';
import { View } from 'react-native';
import React, { useState } from 'react';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { ModalBaseProps } from 'src/models/modalBaseProps.model';
import { AppButton } from '../util/app-button.component';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { translate } from 'src/services/i18n.service';
import { ButtonCheckComponent } from '../util/button-check.component';
import { getColor } from '../../services/color.service';


type Props = {
    PARAM_LIST: string[],
    unique?: boolean,
    onSubmit: (a: string[] | string) => void,
} & ModalBaseProps

function ParamModalComponent(props: Props) {
    const [param, setParams] = useState<string[] | string>([])

    return (
        <Modal
            deviceWidth={SCREEN_WIDTH}
            deviceHeight={SCREEN_HEIGHT}
            isVisible={props.isVisible}
            hideModalContentWhileAnimating={true}
            onBackdropPress={props.onHide}
            onBackButtonPress={props.onHide}
        >
            <>
                <View style={{ flexDirection: "row", flexWrap: "wrap", width: "90%", alignSelf: "center" }}>
                    {
                        props.PARAM_LIST.map(item =>
                            <ButtonCheckComponent
                                key={item}
                                color={getColor(item) ?? 'white'}
                                titleStyle={param.includes(item) && { color: 'black' }}
                                label={translate(item)}
                                containerStyle={{ width: "50%", padding: 4 }
                                }
                                isChecked={param.includes(item)}
                                onPress={() => {
                                    if (props.unique) {
                                        setParams(item)
                                    }
                                    else {

                                        if (param.includes(item)) {
                                            setParams(param.filter(tag => tag !== item))
                                        }
                                        else if (param.length < 5) {
                                            setParams([...param, item])
                                        }
                                    }
                                }}
                            />
                        )
                    }

                </View>

                <AppButton
                    title={translate("validate")}
                    disabled={!(param.length > 0)}
                    containerStyle={{ margin: 8, alignSelf: "center" }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 25, paddingHorizontal: 16 }}
                    disabledStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 25, paddingHorizontal: 16 }}
                    onPress={() => props.onSubmit(param)}
                />
            </>
        </Modal>
    );
}



export default ParamModalComponent