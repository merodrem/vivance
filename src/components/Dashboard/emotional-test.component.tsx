import { View, StyleSheet, ImageBackground, FlatList } from "react-native";
import React, { useState, useCallback } from 'react';
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import GenericHeaderComponent from "../util/generic-header.component";
import { AppText } from "../util/app-text.component";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AppButton } from "../util/app-button.component";
import { SoulWounds } from "src/models/soul-wounds.model";
import { useDispatch } from "react-redux";
import { SET_SOULWOUNDS } from "src/store/reducers/user/user.action";
import { useNavigation } from "@react-navigation/native";
import { BG_IMAGE } from "src/assets/data/app.data";
import { shuffleArray } from "src/services/array.service";
import * as ParseServer from 'src/services/parse/parse.service'
import Toast from 'react-native-simple-toast';


const MAX_SCORE = 8 * 4;

const TEST_QUESTIONS = shuffleArray([
    {
        type: "abandonment",
        question: "Tu as des grands yeux,  souvent le regard triste"
    },
    {
        type: "injustice",
        question: "Tu as le regard vivant, les yeux brillants"
    },
    {
        type: "rejection",
        question: "Tes yeux sont petits et expriment la peur"
    },
    {
        type: "humiliation",
        question: "Tu as des yeux grands, le regard innocent d'un enfant"
    },
    {
        type: "betrayal",
        question: "Ton regard est intense et séducteur"
    },
    {
        type: "rejection",
        question: "Ton corps est plutôt de forme mince, étroite, effacé"
    },
    {
        type: "humiliation",
        question: "Tu as quelques formes, avec un visage rond et ouvert"
    },
    {
        type: "injustice",
        question: "Ton corps est très droit, bien proportionné, rigide"
    },
    {
        type: "betrayal",
        question: "Ton corps est bien bati, les épaules larges et le torse bombé"
    },
    {
        type: "abandonment",
        question: "Tu as un corps long, qui manque de tonus, le dos courbé"
    },
    {
        type: "rejection",
        question: "Tu utilise souvent les mots suivants : Nul, Rien, Disparaître"
    },
    {
        type: "abandonment",
        question: "Tu utilise souvent les mots suivants : Absent, Seul, Je ne supporte pas"
    },
    {
        type: "injustice",
        question: "Tu utilise souvent les mots suivants : Pas de problème, Toujours, Exactement"
    },
    {
        type: "humiliation",
        question: "Tu utilise souvent les mots suivants : Être digne, Petit, Gros"
    },
    {
        type: "betrayal",
        question: "Tu utilise souvent les mots suivants : Je suis capable, Je le savais, Fais-moi confiance"
    },
    {
        type: "injustice",
        question: "Tu aimes manger Salé, croustillant. Tu dois te controller pour ne pas trop manger"
    },
    {
        type: "rejection",
        question: "Tu manges par petite portion. Ton apétit est souvent coupé par tes émotions. "
    },
    {
        type: "abandonment",
        question: "Tu manges lentement mais beaucoup plus que nécessaire"
    },
    {
        type: "betrayal",
        question: "Tu manges très rapidement, avec beaucoup de sel et d'épices"
    },
    {
        type: "humiliation",
        question: "Tu aimes manger gras, riche et sucré. Tu manges plus que de nécessaire"
    },
    {
        type: "betrayal",
        question: "Tu te considère comme quelqu'un de responsable. Tu as beaucoup d'attentes envers les autres"
    },
    {
        type: "humiliation",
        question: "Tu as souvent honte de toi et fait beaucoup pour les autres."
    },
    {
        type: "injustice",
        question: "Tu aimes l'ordre et le rangement. Tu recherche souvent la perfection et t'en demande beaucoup. "
    },
    {
        type: "abandonment",
        question: "Tu te sens souvent seul, à besoin de soutien et de contact physique. Tu pleures facilement. "
    },
    {
        type: "rejection",
        question: "Tu cherche souvent la solitude. Tu te sens incompris ou inexistant. "
    },
    {
        type: "injustice",
        question: "Tu as souvent des problème de gorge, de jambes, de pieds, de coeur"
    },
    {
        type: "humiliation",
        question: "Tu as régulièrement une ou plusieurs des maladies suivantes : Spasmophilie, problème digestif, maladie inflammatoire, herpès buccal"
    },
    {
        type: "betrayal",
        question: "Tu as régulièrement une ou plusieurs des maladies suivantes : Anorgasme, Éjaculation précoce, Impuissance, Torticolis, Crampe, Constipation, Burn out, Insomnie"
    },
    {
        type: "abandonment",
        question: "Tu as régulièrement une ou plusieurs des maladies suivantes : Problème de peau, allergie, dépression, diarrhée fréquente"
    },
    {
        type: "rejection",
        question: "Tu as régulièrement une ou plusieurs des maladies suivantes : Asthme, migraine, agoraphobie, hystérie, maladie rare"
    },
    {
        type: "rejection",
        question: "Ta plus grande peur est de te retrouver dans une situation de panique"
    },
    {
        type: "abandonment",
        question: "Ta plus grande peur est de te retrouver seul"
    },
    {
        type: "humiliation",
        question: "Ta plus grande peur est de te retrouver libre"
    },
    {
        type: "betrayal",
        question: "Ta plus grande peur est d'être séparé des personnes autour de toi "
    },
    {
        type: "injustice",
        question: "Ta plus grande peur est la froideur, l'indifférence"
    },
    {
        type: "abandonment",
        question: "Ta plus grande qualité est l'indépendance : tu n'as pas de problème à te retrouver seul"
    },
    {
        type: "betrayal",
        question: "Ta plus grande qualité est l'altruisme : tu as une grande capacité à prendre soin des autres "
    },
    {
        type: "humiliation",
        question: "Ta plus grande qualité est l'honneteté : tu tiens toujours parole envers les autres et toi même"
    },
    {
        type: "injustice",
        question: "Ta plus grande qualité est l'équité : tu es juste peu importe la situation "
    },
    {
        type: "rejection",
        question: "Ta plus grande qualité est l'autonomie : tu sais prendre soin de toi"
    }
]);

type Props = {
    question: string,
    selectedIndex: number | undefined
    onVote: (value: number) => void
}

function areEqual(prevProps: Props, nextProps: Props) {
    return prevProps.selectedIndex == nextProps.selectedIndex;
}
const QuestionCardComponent = React.memo(function QuestionCardComponent(props: Props) {
    return (
        <View style={theme.panelContainer}>
            <AppText style={{ padding: 8, color: "black" }}>{props.question}</AppText>
            <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                <AppText style={{ padding: 8, color: "black" }}>Pas d'accord</AppText>
                {
                    [0, 1, 2, 3, 4].map(index =>
                        <TouchableOpacity
                            key={index}
                            onPress={() => props.onVote(index)}
                            style={[styles.radioButton, { backgroundColor: props.selectedIndex == index ? PRIMARY_COLOR : "white" }]} />
                    )
                }
                <AppText style={{ padding: 8, color: "black", fontSize: 12 }}>D'accord</AppText>
            </View>
        </View>
    );
}, areEqual);

function EmotionalTestComponent() {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [scores, setScores] = useState(Array(TEST_QUESTIONS.length).fill(undefined));
    const setScore = useCallback(function (score, index) {
        setScores(scores => {
            let newScores = [...scores];
            newScores[index] = score;
            return newScores;
        })

    }, [])
    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

            <GenericHeaderComponent
                title="Test des blessures émotionnelles"
            />
            <View style={{ flex: 1, alignSelf: "center", justifyContent: "center", width: "90%", }}>
                <FlatList
                    data={TEST_QUESTIONS}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }) =>
                        <QuestionCardComponent
                            key={item.question}
                            question={item.question}
                            selectedIndex={scores[index]}
                            onVote={(score) => setScore(score, index)}
                        />
                    }
                    keyExtractor={item => item.question}
                    ListFooterComponent={
                        <AppButton
                            title="Valider!"
                            disabled={scores.some(score => score === undefined)}//disabled if one question remain unanswered
                            containerStyle={{ alignSelf: "center", borderRadius: 10, marginVertical: 8 }}
                            buttonStyle={{ backgroundColor: PRIMARY_COLOR, paddingHorizontal: 32 }}
                            onPress={() => {
                                const soulWounds: SoulWounds = TEST_QUESTIONS.reduce((accu, question, index) => {
                                    accu[question.type] += (scores[index] / (MAX_SCORE)) * 100;
                                    return accu;
                                }, {
                                    rejection: 0,
                                    abandonment: 0,
                                    injustice: 0,
                                    humiliation: 0,
                                    betrayal: 0
                                });
                                ParseServer.changeUser({ soulWounds: soulWounds })
                                    .then(() => {
                                        dispatch({ type: SET_SOULWOUNDS, payload: soulWounds });
                                        navigation.goBack();
                                    })
                                    .catch(error => Toast.show("Impossible d'enregistrer le test: " + error, Toast.LONG))

                            }
                            }
                        />
                    }
                />

            </View>
        </ImageBackground>

    );
}
export default EmotionalTestComponent

const styles = StyleSheet.create({
    radioButton: {
        borderColor: PRIMARY_COLOR,
        width: 24,
        height: 24,
        borderRadius: 50,
        borderWidth: 1,
        marginHorizontal: 2
    }
});
