import { StyleSheet, processColor, ScrollView } from "react-native";
import React, { useState } from 'react';
import { RadarChart, LineChart } from 'react-native-charts-wrapper';
import { useSelector } from "react-redux";
import { StateOfMind } from "../../models/state-of-mind.model";
import { View } from "react-native-animatable";
import { Icon, Slider } from "react-native-elements";
import { PRIMARY_COLOR } from "../../theme/variables";
import moment from "moment";
import { SCREEN_HEIGHT } from "../../assets/data/app.data";
import { getColor } from "../../services/color.service";
import { AppText } from "../util/app-text.component";
import theme from "../../theme/theme";
import { CNV_SITUATIONS, EMOTION_CATEGORIES, NEED_CATEGORIES } from "../../assets/data/cnv.data";
import { AppButton } from "../util/app-button.component";
import { UserState } from "src/models/user.model";
import { useNavigation } from "@react-navigation/native";
import { translate } from "../../services/i18n.service";
import { TouchableOpacity } from "react-native-gesture-handler";
import ParamModalComponent from "./param-modal.component";

const DAYS_OF_WEEK = Object.freeze(["lun", "mar", "mer", "jeu", "ven", "sam", "dim"]);
const MONTHS = Object.freeze(["jan", "fév", "mars", "avr", "mai", "juin", "juil", "aout", "sep", "oct", "nov", "déc"] as const);


const WOUND_COLOR = {
  "rejection": "#396266",
  "abandonment": "#384d66",
  "injustice": "#384d66",
  "humiliation": "#394366",
  "betrayal": "#1f9b8f"
}

const DashboardComponent: React.FC = (props) => {

  const SOMs: Array<StateOfMind> = useSelector(state => state.SOMs)
  const user: UserState = useSelector(state => state.user)

  const today = moment()
  const navigation = useNavigation();
  const [startingDate, setStartingDate] = useState(moment().startOf("isoWeek"));
  const [endingDate, setEndingDate] = useState(today);
  const [selectedSituation, setSelectedSituation] = useState("allSituations")
  const [selectedNeed, setSelectedNeed] = useState("allNeeds")
  const [selectedEmotionCategories, setSelectedEmotionCategories] = useState(EMOTION_CATEGORIES)
  const [showSituationModal, setShowSituationModal] = useState(false);
  const [showEmotionModal, setShowEmotionModal] = useState(false);
  const [showNeedModal, setShowNeedModal] = useState(false);
  function _previousWeek() {
    const newStart = startingDate.clone().subtract(7, 'days');
    const newEnd = newStart.clone().endOf("isoWeek");
    setStartingDate(newStart)
    setEndingDate(newEnd)
  }

  function _nextWeek() {
    const newStart = startingDate.clone().add(7, 'days')
    const newEnd = (newStart.isSame(today, 'week')) ? today : newStart.clone().endOf("isoWeek");
    setStartingDate(newStart)
    setEndingDate(newEnd)
  }

  function lineDataSets(lineData) {
    const datasets = [];
    selectedEmotionCategories.forEach(category => {
      datasets.push({
        values: lineData[category].map(dayValues => dayValues.length > 0 ? dayValues.reduce((a, b) => a + b) / dayValues.length : 0).map((val, index) => ({ x: index, y: val, marker: translate(category) })),
        label: translate(category),
        config: {
          drawValues: false,
          lineWidth: 2,
          mode: "CUBIC_BEZIER",
          drawCircles: true,
          circleColor: processColor(getColor(category)),
          drawCircleHole: true,
          circleRadius: 4,
          highlightColor: processColor("transparent"),
          color: processColor(getColor(category)),
          drawFilled: true,
          fillColor: processColor(getColor(category)),
        }
      })
    })
    return datasets;
  }

  function _renderLineChart() {
    const lineData = {
      "fear": Array(7).fill([]),
      "surprise": Array(7).fill([]),
      "disgust": Array(7).fill([]),
      "anger": Array(7).fill([]),
      "sadness": Array(7).fill([]),
      "vigilence": Array(7).fill([]),
      "joy": Array(7).fill([]),
      "peace": Array(7).fill([])
    };


    SOMs.filter(som =>
      moment(som.timestamp).isSameOrAfter(startingDate, 'day') && moment(som.timestamp).isSameOrBefore(endingDate, 'day')
      && (selectedSituation === "allSituations" || som.situation.type == selectedSituation)
      && (selectedNeed === "allNeeds" || som.need?.category == selectedNeed)
    )
      .forEach(som => {
        const dayIndex = (moment(som.timestamp).day() + 6) % 7 // mapping 0->monday,...
        som.emotions.forEach(emotion => {
          lineData[emotion.category][dayIndex] = [...lineData[emotion.category][dayIndex], emotion.intensity]
        });
      });

    return (<LineChart
      data={{
        dataSets: lineDataSets(lineData),
      }}
      xAxis={
        {
          valueFormatter: DAYS_OF_WEEK,
          granularity: 1,
          position: 'BOTTOM',
          drawAxisLines: false,
          drawGridLines: true
        }
      }
      yAxis={
        {
          left: {
            granularityEnabled: true,
            granularity: 1,
            drawAxisLines: false,
            drawLabels: true,
            axisMinimum: 0,
            axisMaximum: 100,
            drawGridLines: true
          },
          right: {
            drawLabels: false,
            drawAxisLines: false,
            drawGridLines: false

          }

        }
      }
      marker={{ enabled: false }}
      highlightFullBarEnabled={false}
      style={styles.chart}
      legend={{ enabled: false }}
      chartDescription={{ text: "" }}
      scaleEnabled={false}
      touchEnabled={false}
    />);
  }

  function _renderRadarChart() {
    const needs: Array<[string, number]> = SOMs.map(som => [som.need.category, som.need.intensity])
    const needLengths = {
      survival: needs.filter(need => need[0] == "survival").length,
      wellbeing: needs.filter(need => need[0] == "wellbeing").length,
      rea: needs.filter(need => need[0] == "rea").length,
      com: needs.filter(need => need[0] == "com").length,
      rel: needs.filter(need => need[0] == "rel").length,
      spi: needs.filter(need => need[0] == "spi").length,
    };
    const needIntensities = Object.entries(
      needs.reduce((accu, need) => (accu[need[0]] += (need[1] / needLengths[need[0]]), accu), { survival: 50, wellbeing: 50, rea: 50, com: 50, rel: 50, spi: 50 })
    );

    return (
      <>
        <AppText style={{ color: "black", textAlign: "center" }}>Sécurité</AppText>
        <AppText style={{ color: "black", zIndex: 1, position: "absolute", top: "30%", paddingLeft: 8 }}>{translate("spi")}</AppText>
        <AppText style={{ color: "black", zIndex: 1, position: "absolute", bottom: "30%", paddingLeft: 9 }}>{translate("rel")}</AppText>
        <AppText style={{ color: "black", zIndex: 1, position: "absolute", top: "30%", right: 0, paddingRight: 8 }}>{translate("wellbeing")}</AppText>
        <AppText style={{ color: "black", zIndex: 1, position: "absolute", bottom: "30%", right: 0, paddingRight: 8 }}>{translate("rea")}</AppText>
        <RadarChart
          style={styles.chart}
          marker={{ enabled: false }}
          minOffset={20}
          data={{
            dataSets: [
              {
                values: needIntensities.map(need => ({ value: need[1] })),
                label: 'intensité',
                config: {
                  color: processColor('darkblue'),
                  drawValues: false,
                  drawFilled: true,
                  fillColor: processColor('darkblue')
                }
              }],
          }}
          xAxis={{
            valueFormatter: needIntensities.map(need => translate(need[0])),
            textSize: 8,
            drawLabels: false,
            drawAxisLines: false
          }}
          yAxis={{
            drawLabels: false,
            drawGridLines: false,
            axisMaximum: 150,
            axisMinimum: 0,
            enabled: false
          }}
          chartDescription={{ text: '' }}
          legend={{
            enabled: false,
            textSize: 14,
            form: 'CIRCLE',
            wordWrapEnabled: true
          }}
          drawWeb={true}
          webLineWidthInner={1}
          webAlpha={255}
          webColor={processColor("transparent")}
          webColorInner={processColor("lightgrey")}
          skipWebLineCount={0}

          touchEnabled={false}
          onChange={(event) => console.log(event.nativeEvent)}
        />
        <AppText style={{ color: "black", textAlign: "center" }}>{translate("com")}</AppText>
      </>
    );
  }

  return (
    <>
      <ParamModalComponent
        PARAM_LIST={["allSituations", ...CNV_SITUATIONS]}
        unique
        isVisible={showSituationModal}
        onHide={() => setShowSituationModal(false)}
        onSubmit={(situation) => {
          setShowSituationModal(false);
          setSelectedSituation(situation)
        }}
      />
      <ParamModalComponent
        PARAM_LIST={EMOTION_CATEGORIES}
        isVisible={showEmotionModal}
        onHide={() => setShowEmotionModal(false)}
        onSubmit={(emotions) => {
          setShowEmotionModal(false);
          setSelectedEmotionCategories(emotions)
        }}
      />
      <ParamModalComponent
        PARAM_LIST={["allNeeds", ...NEED_CATEGORIES]}
        unique
        onHide={() => setShowNeedModal(false)}
        isVisible={showNeedModal}
        onSubmit={(need) => {
          setShowNeedModal(false);
          setSelectedNeed(need);
        }}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ width: "90%", alignSelf: "center" }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <AppText h4 style={{ marginVertical: 8 }}>Mes émotions</AppText>
          <View style={{ flexDirection: 'row', alignSelf: "center", alignItems: "center" }}>
            {
              SOMs.length > 0 && startingDate.diff(moment(SOMs[0].timestamp), "days") > 0 &&
              <Icon
                name="arrow-left"
                type="feather"
                size={24}
                onPress={_previousWeek}
                color={"white"}
              />
            }
            <AppText>du {startingDate.date()}{startingDate.date() > endingDate.date() ? " " + MONTHS[startingDate.month()] : null} au {endingDate.date()} {MONTHS[endingDate.month()]}</AppText>
            {
              !(endingDate.diff(today, "days") == 0) &&
              <Icon
                name="arrow-right"
                type="feather"
                size={24}
                onPress={_nextWeek}
                color={"white"}
              />}
          </View>
        </View>
        <View style={styles.container}>
          {_renderLineChart()}
        </View>
        <AppButton
          title={translate(selectedSituation)}
          buttonStyle={{ backgroundColor: 'rgba(10,31,57,0.5)', borderWidth: 0, justifyContent: "space-between" }}
          containerStyle={{ height: 40, paddingHorizontal: 2, }}
          onPress={() => setShowSituationModal(true)}
          titleStyle={{ fontSize: 12 }}
          iconRight
          icon={
            <Icon
              name="edit"
              color="white"
              containerStyle={{ marginLeft: 4 }}
              size={12}
            />
          }
        />
        <TouchableOpacity
          style={styles.emotionsButton}
          onPress={() => setShowEmotionModal(true)}
        >
          <View style={{ width: '80%', flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              selectedEmotionCategories.map(cat => <AppText key={cat} style={{ color: getColor(cat), fontSize: 12 }}>{translate(cat)}, </AppText>)
            }
          </View>
          <Icon
            name="edit"
            color="white"
            containerStyle={{ marginLeft: 4 }}
            size={12}
          />
        </TouchableOpacity>
        <AppButton
          title={translate(selectedNeed)}
          buttonStyle={{ backgroundColor: 'rgba(10,31,57,0.5)', borderWidth: 0, justifyContent: "space-between" }}
          containerStyle={{ height: 40, paddingHorizontal: 2, }}
          onPress={() => setShowNeedModal(true)}
          titleStyle={{ fontSize: 12 }}
          iconRight
          icon={
            <Icon
              name="edit"
              color="white"
              containerStyle={{ marginLeft: 4 }}
              size={12}
            />
          }
        />
        <AppText h4 style={{ marginTop: 32, marginBottom: 8 }}>Mes besoins</AppText>

        <View style={styles.container}>
          {_renderRadarChart()}
        </View>
        <AppText h4 style={{ marginTop: 32, marginBottom: 8 }}>Mes blessures émotionnelles</AppText>
        <View style={[theme.panelContainer, { padding: 16 }]}>
          {
            user.soulWounds ?
              <View>
                {
                  Object.entries(user.soulWounds).map((wound, index) =>
                    <TouchableOpacity
                      key={index}
                      style={{ flexDirection: "row", alignItems: "center", width: "100%", justifyContent: "space-between" }}
                      onPress={() => navigation.navigate("WoundDetail", { wound: wound[0] })}
                    >
                      <View style={[{ width: '85%' }]}>
                        <AppText style={{ color: "black" }}>{translate(`${wound[0]}Title`)}</AppText>
                        <Slider
                          maximumValue={100}
                          minimumTrackTintColor={WOUND_COLOR[wound[0]]}
                          disabled
                          trackStyle={{ height: 10, backgroundColor: 'red' }}
                          value={wound[1]}
                          thumbTintColor={"transparent"}
                        />
                      </View>
                      <Icon
                        name="chevron-right"
                        type="feather"
                        color="grey"
                        size={30}
                      />
                    </TouchableOpacity>
                  )
                }
              </View>
              :
              <>
                <AppText style={{ color: "black", marginBottom: 8 }}>Dʼaprès les travaux de recherches du psychiatre
                américain John Pierrakos et Sigmund Freud, nous
                avons tous des blessures émotionnelles issues de
notre enfance.</AppText>
                <AppText style={{ color: "black", marginBottom: 8 }}>Ces souffrances existentielles sʼexpriment sous forme
                de shémas mentaux inconscients qui influent sur
notre comportement et notre rapport aux autres.</AppText>
                <AppText style={{ color: "black", marginBottom: 8 }}>Découvre et conscientise tes blessures internes
grace à ce test de quelques minutes.</AppText>
              </>
          }
          <AppButton
            title={
              (!user.isPremium && !user.isAdmin) ?
                "Faire le test (premium)"
                : user.soulWounds ?
                  "Refaire le test"
                  : "Démarrer le test"}
            disabled={!user.isPremium && !user.isAdmin}
            containerStyle={{ alignSelf: "center", borderRadius: 10 }}
            buttonStyle={{ backgroundColor: PRIMARY_COLOR, paddingHorizontal: 32 }}
            onPress={() => navigation.navigate("EmotionalTest")}
          />
        </View>

      </ScrollView>
    </>
  );
}
export default DashboardComponent

const styles = StyleSheet.create({
  container: {
    ...theme.panelContainer,
    height: SCREEN_HEIGHT * 0.45,
    width: '95%',
    alignSelf: "center",
  },
  chart: {
    flex: 1,
  },
  emotionsButton: {
    height: 40,
    paddingHorizontal: 8,
    marginBottom: 8,
    borderRadius: 5,
    backgroundColor: 'rgba(10,31,57,0.5)',
    borderWidth: 0,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row"
  }
});

