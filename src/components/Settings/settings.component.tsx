import { useNavigation } from "@react-navigation/native";
import React, { useState } from 'react';
import { ImageBackground, Linking, StyleSheet, TouchableOpacity, View } from "react-native";
import { Divider, Header, Icon } from "react-native-elements";
import { FlatList } from "react-native-gesture-handler";
import Toast from 'react-native-simple-toast';
import { useDispatch } from "react-redux";
import { BG_IMAGE } from "src/assets/data/app.data";
import { sendEmail } from "src/services/email.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { TOGGLE_LOGIN } from "src/store/reducers/user/user.action";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { AppListItem } from "../util/app-list-item.component";
import { AppText } from "../util/app-text.component";
import ConfirmModalComponent from "../util/confirm-modal.component";


const SettingsComponent: React.FC = ({ }) => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const [showConfirmModal, setShowConfirmModal] = useState(false);

    const linkItems = Object.freeze([
        {
            label: "Compte",
            onPress: () => navigation.navigate("UserSettings")
        },
        {
            label: "Abonnement",
            onPress: () => navigation.navigate("PremiumSettings")
        },
        // {
        //     label: "Couleurs du thème - en développement",
        //     onPress: () => { }
        // },
        // {
        //     label: "Noter lʼapplication",
        //     link: ""
        // },
        {
            label: "Contact",
            onPress: () => sendEmail(
                'contact@justhere-app.com',
                "[Vivance]",
                ''
            )
        },
        {
            label: "Conditions générales d’utilisation",
            onPress: () => Linking.openURL("http://vivance-app.fr/conditions-generales-dutilisation").catch(err => console.log('An error occurred', err))
        },
        {
            label: "Se déconnecter",
            onPress: () => setShowConfirmModal(true)
        },
    ]);

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <ConfirmModalComponent
                text="Êtes-vous sûr de vouloir vous déconnecter?"
                onHide={() => setShowConfirmModal(false)}
                isVisible={showConfirmModal}
                onConfirm={() => {
                    ParseServer.logout()
                        .then(() => dispatch({ type: TOGGLE_LOGIN }))
                        .catch(error => Toast.show("Impossible de se déconnecter: " + error, Toast.LONG))
                }
                }
            />
            <Header
                    containerStyle={{ backgroundColor: "transparent", borderBottomWidth:0 }}
                    leftComponent={<Icon
                        name="settings"
                        type="material"
                        size={48}
                        color="white"
                    />}
                    centerComponent={<AppText h4>Paramètres</AppText>}
            />
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Divider style={{ backgroundColor: PRIMARY_COLOR, height: 2 }} />
                <FlatList
                    keyExtractor={item => item.label}
                    data={linkItems}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            onPress={item.onPress}
                        >
                            <AppListItem 
                            title={item.label}
                            />
                        </TouchableOpacity>
                    }
                />
            </View>
        </ImageBackground>
    );
}
export default SettingsComponent


const styles = StyleSheet.create({
});
