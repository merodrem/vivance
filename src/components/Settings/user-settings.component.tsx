import DateTimePicker from '@react-native-community/datetimepicker';
import { useNavigation } from "@react-navigation/native";
import React, { MutableRefObject, useRef, useState } from 'react';
import { ActivityIndicator, GestureResponderEvent, ImageBackground, StyleSheet, TouchableOpacity, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { Icon, Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from "react-redux";
import { BG_IMAGE, SCREEN_HEIGHT } from "src/assets/data/app.data";
import { formatDate } from "src/services/date.service";
import { validateEmail } from "src/services/email.service";
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from "src/services/string.service";
import theme from "src/theme/theme";
import { ACCENT_COLOR, PRIMARY_COLOR } from "src/theme/variables";
import { UserState } from "../../models/user.model";
import { SET_USER } from "../../store/reducers/user/user.action";
import { AppButton } from "../util/app-button.component";
import { AppInput } from "../util/app-input.component";
import { AppText } from "../util/app-text.component";
import GenericHeaderComponent from "../util/generic-header.component";


const UserSettingsComponent: React.FC = () => {
    const navigation = useNavigation();
    const user: UserState = useSelector(state => state.user)

    const [editEmail, setEditEmail] = useState(false);
    const [emailUploading, setEmailUploading] = useState(false);
    const [isEmailValid, setIsEmailValid] = useState(true);
    const emailInput: MutableRefObject<Input | null> = useRef(null);
    const emailEntry = useRef(user.email);

    const [editUsername, setEditUsername] = useState(false);
    const [usernameUploading, setUsernameUploading] = useState(false);
    const [isUsernameValid, setIsUsernameValid] = useState(true)
    const usernameEntry = useRef(user.username);
    const usernameInput: MutableRefObject<Input | null> = useRef(null);

    const [editCountry, setEditCountry] = useState(false);
    const [countryUploading, setCountryUploading] = useState(false);
    const countryEntry = useRef(user.location?.country);

    const [editCity, setEditCity] = useState(false);
    const [cityUploading, setCityUploading] = useState(false);
    const cityEntry = useRef(user.location?.city);

    const [editPresentation, setEditPresentation] = useState(false);
    const [presentationUploading, setPresentationUploading] = useState(false);
    const presentationEntry = useRef(user.presentation);

    const [editGender, setEditGender] = useState(false);
    const [genderUploading, setGenderUploading] = useState(false);
    const genderEntry = useRef(user.gender);

    const [editDateOfBirth, setEditDateOfBirth] = useState(false);
    const [dateOfBirthUploading, setDateOfBirthUploading] = useState(false);


    const [editPassword, setEditPassword] = useState(false);
    const [passwordUploading, setPasswordUploading] = useState(false);

    const dispatch = useDispatch()

    function _renderConfirmButton(onPressFunction: (event: GestureResponderEvent) => void) {
        return (
            <Icon
                reverse
                name='check'
                type='entypo'
                color={PRIMARY_COLOR}
                size={10}
                onPress={onPressFunction} />
        );
    }

    function _renderCancelButton(onPressFunction: (event: GestureResponderEvent) => void) {
        return (
            <Icon
                reverse
                name='cross'
                type='entypo'
                color='#FFA07A'
                size={10}
                onPress={onPressFunction} />
        );
    }

    function _changeEmail() {
        if (validateEmail(emailEntry.current)) {
            setEmailUploading(true);
            setIsEmailValid(true)
            ParseServer.changeUser({ email: emailEntry.current })
                .then((user: UserState) => {
                    Toast.show("Adresse email changée!", Toast.LONG)
                    dispatch({ type: SET_USER, payload: { email: user.email } })

                    setEditEmail(false);
                })
                .finally(() => setEmailUploading(false))
        }
        else {
            setIsEmailValid(false);
            emailInput.current?.shake();
        }
    }

    function _changeUsername() {
        if (usernameEntry.current.length > 1) {
            setUsernameUploading(true);
            setIsUsernameValid(true)
            ParseServer.changeUser({ username: usernameEntry.current })
                .then((user: UserState) => {
                    Toast.show("Nom d'utilisateur changé!", Toast.LONG)
                    dispatch({ type: SET_USER, payload: { username: user.username } })
                    setEditUsername(false);
                })
                .catch((error) => {
                    switch (error.code) {
                        case 202:
                            Toast.show("l'utilisateur existe déjà!", Toast.LONG)
                            break;
                        default:
                            console.log(error.message);
                            break;
                    }
                })
                .finally(() => setUsernameUploading(false))
        }
        else {
            setIsUsernameValid(false);
            usernameInput.current?.shake();
        }
    }

    function _changeGender() {
        setGenderUploading(true);
        ParseServer.changeUser({ gender: genderEntry.current })
            .then((user:UserState) => {
                Toast.show("Genre changé!", Toast.LONG)
                dispatch({ type: SET_USER, payload: { gender: user.gender } })
                setEditGender(false);
            })
            .catch(error => console.log(error.code))
            .finally(() => setGenderUploading(false))
    }

    function _changeCountry() {
        setCountryUploading(true);
        ParseServer.changeUser({ location: { ...user.location, country: countryEntry.current } })
            .then((user: UserState) => {
                Toast.show("Pays changé!", Toast.LONG)
                dispatch({ type: SET_USER, payload: { location: user.location } })
                setEditCountry(false);
            })
            .catch(error => console.log(error.code))
            .finally(() => setCountryUploading(false))
    }

    function _changeCity() {
        setCityUploading(true);
        ParseServer.changeUser({ location: { ...user.location, city: cityEntry.current } })
            .then((user:UserState) => {
                Toast.show("Ville changée!", Toast.LONG)
                dispatch({ type: SET_USER, payload: { location: user.location } })
                setEditCity(false);
            })
            .catch(error => console.log(error.code))
            .finally(() => setCityUploading(false))
    }

    function _changePresentation() {
        setPresentationUploading(true);
        ParseServer.changeUser({ presentation: presentationEntry.current })
            .then((user:UserState) => {
                Toast.show("Présentation changée!", Toast.LONG)
                dispatch({ type: SET_USER, payload: { presentation: user.presentation } })
                setEditPresentation(false);
            })
            .catch(error => console.log(error.code))
            .finally(() => setPresentationUploading(false))
    }

    function _changeDateOfBirth(date: Date) {
        setDateOfBirthUploading(true);
        ParseServer.changeUser({ dateOfBirth: date.toISOString() })
            .then((user:UserState) => {
                Toast.show("Date de naissance changée!", Toast.LONG)
                dispatch({ type: SET_USER, payload: { dateOfBirth: user.dateOfBirth } })
                setEditDateOfBirth(false);
            })
            .finally(() => setDateOfBirthUploading(false))

    }

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <GenericHeaderComponent
                title="Compte" />
            <View style={{ flex: 1, backgroundColor: 'white' }}>

                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    keyboardShouldPersistTaps='handled'
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '30%' }}>Email</AppText>
                        {editEmail ?
                            <View style={{ flexDirection: 'row', width: "70%", }}>
                                <AppInput
                                    keyboardAppearance="light"
                                    autoCorrect={false}
                                    keyboardType="email-address"
                                    returnKeyType={'done'}
                                    containerStyle={{
                                        borderBottomColor: 'rgba(0, 0, 0, 0.38)', width: "70%",
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={{ fontSize: 14, paddingBottom: 0 }}
                                    inputContainerStyle={{ margin: 0 }}
                                    ref={emailInput}
                                    maxLength={50}
                                    defaultValue={user.email}
                                    onSubmitEditing={_changeEmail}
                                    onChangeText={(text) => emailEntry.current = text}
                                    errorMessage={
                                        !isEmailValid && translate("emailInvalid")
                                    }
                                />

                                <View style={{ flexDirection: 'row', width: "30%", alignItems: "center" }}>
                                    {_renderCancelButton(() => setEditEmail(false))}
                                    {emailUploading ?
                                        <ActivityIndicator color={PRIMARY_COLOR} />
                                        :
                                        _renderConfirmButton(_changeEmail)
                                    }
                                </View>
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditEmail(true)}
                                style={{ width: "70%" }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: "right" }}>{user.email}</AppText>
                            </TouchableOpacity>

                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '40%' }}>Mot de passe</AppText>
                        {editPassword ?
                            <View style={{ flexDirection: 'row', width: "60%", alignItems: "center" }}>
                                <AppButton
                                    title="Annuler"
                                    type="clear"
                                    buttonStyle={{ padding: 2 }}
                                    containerStyle={{ width: "40%" }}
                                    titleStyle={{ color: 'red' }}
                                    onPress={() => setEditPassword(false)}
                                />
                                <AppButton
                                    title="Réinitialiser le mot de passe"
                                    containerStyle={{ width: "60%" }}
                                    titleStyle={{ color: PRIMARY_COLOR }}
                                    buttonStyle={{ padding: 2 }}
                                    type="clear"
                                    loading={passwordUploading}
                                    loadingProps={{ color: PRIMARY_COLOR }}
                                    onPress={() => {
                                        setPasswordUploading(true)
                                        ParseServer.changePassword(user.email)
                                            .then(() => {
                                                Toast.show("Un email vous a été envoyé pour réinitialiser le mot de passe", Toast.LONG)

                                                setEditPassword(false)
                                            }
                                            )
                                            .finally(() => setPasswordUploading(false))
                                    }}
                                />
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditPassword(true)}
                                style={{ width: "60%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>*********</AppText>
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '45%' }}>{capitalizeFirstLetter(translate("username"))}</AppText>
                        {editUsername ?
                            <View style={{ flexDirection: 'row', width: "55%", }}>
                                <AppInput
                                    keyboardAppearance="light"
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType={'done'}
                                    containerStyle={{
                                        borderBottomColor: 'rgba(0, 0, 0, 0.38)', width: "60%",
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={{ fontSize: 16, paddingBottom: 0 }}
                                    inputContainerStyle={{ margin: 0 }}
                                    ref={usernameInput}
                                    maxLength={50}
                                    defaultValue={user.username}
                                    onSubmitEditing={_changeUsername}
                                    onChangeText={(text) => usernameEntry.current = text}
                                    errorMessage={
                                        !isUsernameValid && translate("usernameInvalid")
                                    }
                                />

                                <View style={{ flexDirection: 'row', width: "30%", alignItems: "center" }}>
                                    {_renderCancelButton(() => setEditUsername(false))}
                                    {usernameUploading ?
                                        <ActivityIndicator color={PRIMARY_COLOR} />
                                        :
                                        _renderConfirmButton(_changeUsername)
                                    }
                                </View>
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditUsername(true)}
                                style={{ width: "55%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{user.username}</AppText>
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '45%' }}>Presentation</AppText>
                        {!editPresentation &&
                            <TouchableOpacity
                                onPress={() => setEditPresentation(true)}
                                style={{ width: "55%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{user.presentation ?? "Aucune"}</AppText>
                            </TouchableOpacity>
                        }
                    </View>
                    {editPresentation &&

                        <View style={{ alignItems: "center", width: "90%", alignSelf: "center" }}>
                            <AppInput
                                keyboardAppearance="light"
                                autoCorrect={false}
                                keyboardType="default"
                                containerStyle={{
                                    borderBottomColor: 'rgba(0, 0, 0, 0.38)',
                                    paddingHorizontal: 0,
                                    paddingVertical: 0,
                                    height: SCREEN_HEIGHT / 3
                                }}
                                inputStyle={theme.darkInput}
                                inputContainerStyle={[theme.darkInputContainer, { height: "100%" }]}
                                maxLength={800}
                                multiline
                                textAlignVertical='top'
                                defaultValue={user.presentation}
                                onChangeText={(text) => presentationEntry.current = text}
                            />
                            <View style={{ flexDirection: 'row', width: "20%", alignItems: "center" }}>
                                {_renderCancelButton(() => setEditPresentation(false))}
                                {presentationUploading ?
                                    <ActivityIndicator color={PRIMARY_COLOR} />
                                    :
                                    _renderConfirmButton(_changePresentation)
                                }
                            </View>
                        </View>
                    }
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '35%' }}>{capitalizeFirstLetter(translate("gender"))}</AppText>
                        {editGender ?
                            <View style={{ flexDirection: 'row', width: "65%", }}>
                                <DropDownPicker
                                    items={["otherGender", "female", "male"].map(gender => ({ label: capitalizeFirstLetter(translate(gender)), value: gender }))}
                                    defaultValue={user.gender}
                                    containerStyle={{ flex: 1, height: 40, paddingHorizontal: 2 }}
                                    style={{ backgroundColor: 'rgba(10,31,57,0.5)', borderWidth: 0 }}
                                    dropDownStyle={{ backgroundColor: 'rgba(10,31,57,0.5)', marginHorizontal: 2, borderWidth: 0 }}
                                    labelStyle={{ color: "white" }}
                                    arrowColor="white"

                                    itemStyle={{
                                        justifyContent: 'flex-start'
                                    }}
                                    onChangeItem={({ label, value }) => { genderEntry.current = value }}
                                />

                                <View style={{ flexDirection: 'row', width: "30%", alignItems: "center" }}>
                                    {_renderCancelButton(() => setEditGender(false))}
                                    {genderUploading ?
                                        <ActivityIndicator color={PRIMARY_COLOR} />
                                        :
                                        _renderConfirmButton(_changeGender)
                                    }
                                </View>
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditGender(true)}
                                style={{ width: "65%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{capitalizeFirstLetter(translate(user.gender))}</AppText>
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '45%' }}>{capitalizeFirstLetter(translate("dateOfBirth"))}</AppText>
                        {editDateOfBirth &&
                            <DateTimePicker
                                value={user.dateOfBirth ? new Date(user.dateOfBirth) : new Date()}
                                mode='date'
                                display="spinner"
                                onChange={(event, date) => {
                                    setEditDateOfBirth(false);
                                    if (date) {
                                        if (event.type == 'set') {
                                            _changeDateOfBirth(date)
                                        }
                                    }
                                }}
                            />
                        }
                        <TouchableOpacity
                            onPress={() => setEditDateOfBirth(true)}
                            style={{ width: "55%", }}
                        >
                            <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{user.dateOfBirth ? formatDate(new Date(user.dateOfBirth)) : "Non défini"}</AppText>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '45%' }}>{capitalizeFirstLetter(translate("country"))}</AppText>
                        {editCountry ?
                            <View style={{ flexDirection: 'row', width: "55%", }}>
                                <AppInput
                                    keyboardAppearance="light"
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType={'done'}
                                    containerStyle={{
                                        borderBottomColor: 'rgba(0, 0, 0, 0.38)', width: "60%",
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={{ fontSize: 16, paddingBottom: 0 }}
                                    inputContainerStyle={{ margin: 0 }}
                                    maxLength={50}
                                    defaultValue={user.location.country}
                                    onSubmitEditing={_changeCountry}
                                    onChangeText={(text) => countryEntry.current = text}
                                />

                                <View style={{ flexDirection: 'row', width: "30%", alignItems: "center" }}>
                                    {_renderCancelButton(() => setEditCountry(false))}
                                    {countryUploading ?
                                        <ActivityIndicator color={PRIMARY_COLOR} />
                                        :
                                        _renderConfirmButton(_changeCountry)
                                    }
                                </View>
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditCountry(true)}
                                style={{ width: "55%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{user.location.country ?? "Non défini"}</AppText>
                            </TouchableOpacity>
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16, width: '45%' }}>{capitalizeFirstLetter(translate("city"))}</AppText>
                        {editCity ?
                            <View style={{ flexDirection: 'row', width: "55%", }}>
                                <AppInput
                                    keyboardAppearance="light"
                                    autoCorrect={false}
                                    keyboardType="default"
                                    returnKeyType={'done'}
                                    containerStyle={{
                                        borderBottomColor: 'rgba(0, 0, 0, 0.38)', width: "60%",
                                        paddingHorizontal: 0,
                                    }}
                                    inputStyle={{ fontSize: 16, paddingBottom: 0 }}
                                    inputContainerStyle={{ margin: 0 }}
                                    maxLength={50}
                                    defaultValue={user.location.city}
                                    onSubmitEditing={_changeCity}
                                    onChangeText={(text) => cityEntry.current = text}
                                />

                                <View style={{ flexDirection: 'row', width: "30%", alignItems: "center" }}>
                                    {_renderCancelButton(() => setEditCity(false))}
                                    {cityUploading ?
                                        <ActivityIndicator color={PRIMARY_COLOR} />
                                        :
                                        _renderConfirmButton(_changeCity)
                                    }
                                </View>
                            </View>
                            :
                            <TouchableOpacity
                                onPress={() => setEditCity(true)}
                                style={{ width: "55%", }}
                            >
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: 'right' }}>{user.location.city ?? "Non défini"}</AppText>
                            </TouchableOpacity>
                        }
                    </View>

                </KeyboardAwareScrollView>
            </View>
        </ImageBackground>
    );
}
export default UserSettingsComponent


const styles = StyleSheet.create({
});
