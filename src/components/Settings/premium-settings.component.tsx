import { useFocusEffect } from '@react-navigation/core';
import React, { useState } from 'react';
import { ImageBackground, Linking, Platform, StyleSheet, View } from "react-native";
import { useSelector } from 'react-redux';
import { BG_IMAGE } from "src/assets/data/app.data";
import theme from "src/theme/theme";
import { ACCENT_COLOR } from "src/theme/variables";
import { UserState } from '../../models/user.model';
import { AppButton } from "../util/app-button.component";
import { AppText } from "../util/app-text.component";
import GenericHeaderComponent from "../util/generic-header.component";
import RNIap, { ProductPurchase, SubscriptionPurchase } from 'react-native-iap'
import { Divider } from 'react-native-elements';
import { translate } from '../../services/i18n.service';



const PremiumSettingsComponent: React.FC = () => {
    const user: UserState = useSelector(state => state.user)
    const [subscriptions, setSubscriptions] = useState<(ProductPurchase | SubscriptionPurchase)[]>([]);
    useFocusEffect(
        React.useCallback(() => {
            RNIap.getAvailablePurchases().then(result => setSubscriptions(result))
        }, [])
    );
    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <GenericHeaderComponent
                title="Gérer mon abonnement" />
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                    <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16 }}>Formule actuelle</AppText>
                    <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: "right" }}>{user.isPremium ? 'Premium' : 'Compte gratuit'}</AppText>
                </View>
                {
                    subscriptions.map(sub =>
                        <View key={sub.productId}>
                            <Divider />
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                                <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16 }}>Type d'abonnement</AppText>
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: "right" }}>{translate(sub.productId)}</AppText>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "space-between" }}>
                                <AppText style={{ color: 'black', paddingLeft: 16, paddingVertical: 16 }}>Renouvellement automatique?</AppText>
                                <AppText style={{ color: ACCENT_COLOR, paddingRight: 32, paddingVertical: 16, textAlign: "right" }}>{sub.autoRenewingAndroid ? 'Oui' : "Non"}</AppText>
                            </View>
                        </View>
                    )}
                <AppButton
                    type="clear"
                    containerStyle={{ marginTop: 32 }}
                    title="Gérer les abonnements / se désabonner"
                    onPress={() => {
                        Platform.OS === 'android' ?
                            Linking.openURL('https://play.google.com/store/account/subscriptions')
                            :
                            Linking.openURL('https://apps.apple.com/account/subscriptions')
                    }}
                />
            </View>
        </ImageBackground >
    );
}
export default PremiumSettingsComponent


const styles = StyleSheet.create({

});
