import React from 'react';
import { StyleSheet, View } from "react-native";
import { Icon } from "react-native-elements";
import { sendEmail } from "src/services/email.service";
import { ACCENT_COLOR } from "src/theme/variables";
import { AppButton } from "../util/app-button.component";
import GenericHeaderComponent from "../util/generic-header.component";


type Props = {

}

const ContactComponent: React.FC = () => {
    return (
        <>
            <GenericHeaderComponent title="Contact" />
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white' }}>
                <AppButton
                    containerStyle={styles.buttonContainer}
                    buttonStyle={styles.button}
                    title="contact"
                    icon={
                        <Icon
                            name="mail"
                            size={25}
                            type='entypo'
                            color="black"
                            containerStyle={{ marginRight: 8 }}
                        />
                    }
                    onPress={() => sendEmail(
                        'contact@justhere-app.com',
                        "[Vivance]",
                        ''
                    )}
                />
            </View>
        </>
    );
}
export default ContactComponent


const styles = StyleSheet.create({
    userSettingsButton: {
        margin: 8,
    },
    buttonContainer: {
        paddingHorizontal: 16,
        paddingVertical: 4,
        width: '80%'
    },
    button: {
        backgroundColor: ACCENT_COLOR,
        elevation: 1,
    },
});
