import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Image, ImageBackground, StyleSheet } from 'react-native';
import { View } from 'react-native-animatable';
import { Divider, Header, Icon } from 'react-native-elements';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { BG_IMAGE, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { translate } from 'src/services/i18n.service';
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from 'src/services/string.service';
import { SET_EXERCISES } from 'src/store/reducers/exercises/exercises.action';
import theme from 'src/theme/theme';
import { ACCENT_COLOR, PRIMARY_COLOR } from 'src/theme/variables';
import DashboardComponent from '../Dashboard/dashboard.component';
import EmotionalTestComponent from '../Dashboard/emotional-test.component';
import WoundDetailComponent from '../Dashboard/wound-detail.component';
import NotesDiaryComponent from '../Diary/notes-diary.component';
import AddExerciseComponent from '../Exercises/add-exercise.component';
import ExerciseListComponent from '../Exercises/exercise-list.component';
import ExerciseComponent from '../Exercises/exercise.component';
import ExerciseHomeComponent from '../Exercises/exercises-home.component';
import FavoritesComponent from '../Favorites/favorites.component';
import { HomeComponent } from '../Home/home.component';
import PremiumComponent from '../Premium/premium.component';
import ProfileComponent from '../Profile/profile.component';
import UserProfileComponent from '../Profile/user-profile.component';
import ContactComponent from '../Settings/contact.component';
import PremiumSettingsComponent from '../Settings/premium-settings.component';
import SettingsComponent from '../Settings/settings.component';
import UserSettingsComponent from '../Settings/user-settings.component';
import ResetPasswordComponent from '../Signing/resetPassword.component';
import SigningComponent from '../Signing/signing.component';
import AddSOMComponent from '../SOM/add-som.component';
import { AppButton } from '../util/app-button.component';
import { AppText } from '../util/app-text.component';


const TabNavigation = createMaterialTopTabNavigator()


const AppTabs: React.FC = (props) => {
    const user: string = useSelector(state => state.user)
    const dispatch = useDispatch()
    ParseServer.getExercisesFromUser().then(exercises => {
        dispatch({ type: SET_EXERCISES, payload: exercises })
    })
        .catch(error => {
            Toast.show("Impossible de rafraichir les exercices: " + error.message, Toast.LONG)
        })

    return (<>
        <Header
            containerStyle={{ backgroundColor: 'transparent', borderBottomWidth: 0 }}

            rightComponent={<AppButton
                onPress={props.navigation.toggleDrawer}
                type="clear"
                icon={<Icon
                    name="menu"
                    size={35}
                    color="white"
                />}
            />}
        />
        <TabNavigation.Navigator
            tabBarPosition={"bottom"}
            swipeEnabled={false}
            lazy={true}
            initialRouteName={"Home"}
            sceneContainerStyle={
                {
                    backgroundColor: 'transparent'
                }
            }
            tabBarOptions={{
                showIcon: true,
                showLabel: false,
                activeTintColor: ACCENT_COLOR,
                inactiveTintColor: 'white',
                iconStyle: {
                    width: "100%",
                    height: "100%",
                },
                tabStyle: {
                    paddingHorizontal: 0,
                    height: 60
                },
                style: {
                    backgroundColor: PRIMARY_COLOR,
                },
                renderIndicator: () => null
                // indicatorStyle: {
                //     backgroundColor: 'white'
                // }
            }}>
            <TabNavigation.Screen
                options={{
                    tabBarLabel: "Accueil",
                    tabBarIcon: ({ focused }) => <Icon
                        name="home"
                        type="font-awesome-5"
                        size={30}
                        color={focused ? ACCENT_COLOR : 'white'}
                    />
                }}
                name="Home" component={HomeComponent} />
            <TabNavigation.Screen
                options={{
                    tabBarLabel: "Exercises",
                    tabBarIcon: ({ focused }) => <Icon
                        name="running"
                        type="font-awesome-5"
                        size={30}
                        color={focused ? ACCENT_COLOR : 'white'}
                    />
                }}
                name="Exercises" component={ExerciseHomeComponent} />
            <TabNavigation.Screen
                options={{
                    tabBarLabel: "Tableau de bord",
                    tabBarIcon: ({ focused }) => <Icon
                        name="graph"
                        type="octicon"
                        size={30}
                        color={focused ? ACCENT_COLOR : 'white'}
                    />
                }}
                name="Dashboard" component={DashboardComponent} />
            <TabNavigation.Screen
                options={{
                    tabBarLabel: "Profil",
                    tabBarIcon: ({ focused }) => <Icon
                        name="user"
                        type="font-awesome"
                        size={30}
                        color={focused ? ACCENT_COLOR : 'white'}
                    />
                }}
                initialParams={{ profile: user }}
                name="UserProfile" component={UserProfileComponent} />

        </TabNavigation.Navigator>
    </>
    )
}
const AppStack: React.FC = (props) => {

    return (
        <Stack.Navigator headerMode="none">
            <Stack.Screen name="Tabs" options={
                {
                    cardStyle: { backgroundColor: 'transparent' }
                }
            }
                component={AppTabs} />
            <Stack.Screen name="Favorites" component={FavoritesComponent} />
            <Stack.Screen name="Exercise" component={ExerciseComponent} />
            <Stack.Screen name="Profile" component={ProfileComponent} />
            <Stack.Screen name="ExerciseList" component={ExerciseListComponent} />
            <Stack.Screen name="Contact" component={ContactComponent} />
            <Stack.Screen name="AddExercise" component={AddExerciseComponent} />
            <Stack.Screen name="WoundDetail" component={WoundDetailComponent} />
            <Stack.Screen name="Settings" component={SettingsComponent} />
            <Stack.Screen name="UserSettings" component={UserSettingsComponent} />
            <Stack.Screen name="PremiumSettings" component={PremiumSettingsComponent} />
            <Stack.Screen name="EmotionalTest" component={EmotionalTestComponent} />
        </Stack.Navigator>
    )
}

function CustomDrawerContent(props) {
    const hasRights = props.isPremium || props.isAdmin;
    return (<>
        <View style={{ flexDirection: 'row', backgroundColor: PRIMARY_COLOR, height: 120, alignItems: "center", justifyContent: "space-around" }}>
            <Image source={require('src/assets/img/logo-notext.png')} style={styles.image} /><AppText style={{ fontSize: 38, marginHorizontal: 16 }}>VIVANCE</AppText>
        </View>
        <DrawerContentScrollView {...props}>
            <DrawerItem label={capitalizeFirstLetter(translate("questionsDrawer"))} onPress={() => props.navigation.navigate("AddSOM")} />
            <DrawerItem labelStyle={!hasRights && { color: "lightgrey" }} label={capitalizeFirstLetter(translate("addExerciseDrawer") + ' (premium)')} onPress={() => {
                if (hasRights) {
                    props.navigation.navigate("AddExercise")
                }
            }
            } />
            <Divider />
            <DrawerItem label={capitalizeFirstLetter(translate("premiumDrawer"))} onPress={() => props.navigation.navigate("Premium")} />
            <DrawerItem label={capitalizeFirstLetter(translate("diaryDrawer"))} onPress={() => props.navigation.navigate("EmotionsDiary")} />
            <DrawerItem label={capitalizeFirstLetter(translate("favoritesDrawer"))} onPress={() => props.navigation.navigate("Favorites")} />
            <DrawerItem labelStyle={styles.disabledDrawerLabel} label={capitalizeFirstLetter(translate("ressourcesDrawer"))} onPress={() => { }} />
            <Divider />
            <DrawerItem labelStyle={styles.disabledDrawerLabel} label={capitalizeFirstLetter(translate("supportDrawer"))} onPress={() => { }} />
            <DrawerItem labelStyle={styles.disabledDrawerLabel} label={capitalizeFirstLetter(translate("notificationsDrawer"))} onPress={() => { }} />
            <DrawerItem label={capitalizeFirstLetter(translate("settingsDrawer"))} onPress={() => props.navigation.navigate("Settings")} />
        </DrawerContentScrollView>
    </>
    );
}

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator()


const Navigation: React.FC = () => {
    const logged: boolean = useSelector(state => state.user.logged)
    const isPremium: boolean = useSelector(state => state.user.isPremium)
    const isAdmin: boolean = useSelector(state => state.user.isAdmin)

    return (
        <NavigationContainer>
            <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
                {
                    logged ? (
                        <Drawer.Navigator
                            sceneContainerStyle={{ backgroundColor: 'transparent' }}
                            edgeWidth={SCREEN_WIDTH / 10}

                            drawerContent={(props) => <CustomDrawerContent {...props} isAdmin={isAdmin} isPremium={isPremium}></CustomDrawerContent>}
                        >
                            <Drawer.Screen name="AppStack" component={AppStack} />
                            <Drawer.Screen name="AddSOM" component={AddSOMComponent} />
                            <Drawer.Screen name="Premium" component={PremiumComponent} />
                            <Drawer.Screen name="Diary" component={AddExerciseComponent} />
                            <Drawer.Screen name="EmotionsDiary" component={NotesDiaryComponent} />
                        </Drawer.Navigator>

                    ) : (
                        <Stack.Navigator headerMode="none">
                            <Stack.Screen name="SignIn" component={SigningComponent} />
                            <Stack.Screen name="ResetPassword" component={ResetPasswordComponent} />
                        </Stack.Navigator>
                    )
                }
            </ImageBackground>
        </NavigationContainer>
    );
}


export default Navigation;

const styles = StyleSheet.create({
    disabledDrawerLabel: {
        color: 'lightgrey'
    },
    image: {
        maxWidth: 50,
        maxHeight: 79
    },
});
