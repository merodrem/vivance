import React from 'react';
import { useSelector } from "react-redux";
import { UserState } from "src/models/user.model";
import BaseProfileComponent from "./base-profile.component";

const UserProfileComponent: React.FC = () => {
    const user: UserState = useSelector(state => state.user)
    return (
        <BaseProfileComponent profile={user}/>
    );
}
export default UserProfileComponent


