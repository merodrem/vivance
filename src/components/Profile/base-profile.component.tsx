import { useNavigation } from "@react-navigation/native";
import React, { useState } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { useSelector } from "react-redux";
import Avatar from 'src/components/util/avatar.component';
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { getCompletion } from "src/services/reputation.service";
import { capitalizeFirstLetter } from "src/services/string.service";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { SCREEN_WIDTH } from "../../assets/data/app.data";
import { UserState } from "../../models/user.model";
import { AppText } from "../util/app-text.component";
import CircularProgressComponent from "../util/circular-progress.component";

const BaseProfileComponent: React.FC = (props: UserState) => {
    const navigation = useNavigation();
    const [profileLoading, setProfileLoading] = useState(false);
    let profile = props.profile;
    const user: UserState = useSelector(state => state.user)

    if (profile.objectId !== user.objectId) {
        ParseServer.viewProfile(profile.objectId);
    }

    return (
        profileLoading ?
            <ActivityIndicator
                color="white"
                style={{ flex: 1, alignSelf: "center" }}
                size={64}
            />
            :
                <ScrollView contentContainerStyle={{ width: "90%", alignSelf: "center", justifyContent: "center" }}>
                    <Avatar
                        avatarBase64={profile.avatar}
                        style={{ alignSelf: "center", zIndex: 1 }}
                    />
                    <View style={[theme.panelContainer, { alignItems: "center", top: -30, padding: 16, paddingTop: 20 }]}>
                        <TouchableOpacity
                            style={{ padding: 12, position: "absolute", right: 0, top: 0 }}
                            onPress={() => navigation.navigate("UserSettings")}
                        >
                            <Icon
                                name="edit"
                                color="black"
                            />
                        </TouchableOpacity>
                        <AppText h2 style={{ fontWeight: "bold", color: "black" }}>{profile.username}</AppText>
                        <AppText style={{ color: "black" }}>{profile.presentation}</AppText>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Icon
                                name="location-pin"
                                type="entypo"
                                color="skyblue"
                            />
                            <AppText style={{ color: "skyblue" }}>{profile.location ? (profile.location.city ?? "Ville inconnue") + ", " + profile.location.country : translate("addressNotFound")}</AppText>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <AppText>{capitalizeFirstLetter(translate("reputation"))}: <AppText style={{ fontWeight: "bold" }}>{profile.reputation} </AppText></AppText>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <AppText style={{ fontWeight: "bold" }}>{profile.goldenBadges}</AppText>
                            <Icon
                                name="trophy"
                                type="font-awesome"
                                size={14}
                                color="gold"
                                containerStyle={{ marginRight: 8 }}
                            />
                            <AppText style={{ fontWeight: "bold" }}>{profile.silverBadges}</AppText>
                            <Icon
                                name="trophy"
                                type="font-awesome"
                                size={14}
                                color="grey"
                                containerStyle={{ marginRight: 8 }}
                            />
                            <AppText style={{ fontWeight: "bold" }}>{profile.bronzeBadges}</AppText>
                            <Icon
                                name="trophy"
                                type="font-awesome"
                                size={14}
                                color="orange"
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={(SCREEN_WIDTH * 0.8 / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.activeDays, "activeDays")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.activeDays}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>Jours actifs</AppText>
                        </View>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={((SCREEN_WIDTH * 0.8) / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.statesOfMindCount, "statesOfMind")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.statesOfMindCount}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>Humeurs</AppText>
                        </View>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={(SCREEN_WIDTH * 0.8 / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.exercisesCount, "exercises")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.exercisesCount}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>Exercices</AppText>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={(SCREEN_WIDTH * 0.8 / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.profileViewers.length, "profileViewers")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.profileViewers.length}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>Vues du profil</AppText>
                        </View>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={(SCREEN_WIDTH * 0.8 / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.impactedUsers, "impactedUsers")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.impactedUsers}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>Personnes impactées</AppText>
                        </View>
                        <View style={[theme.panelContainer, { width: (SCREEN_WIDTH * 0.9 / 3) - 4, alignItems: "center" }]}>
                            <CircularProgressComponent
                                size={(SCREEN_WIDTH * 0.8 / 3) - 20}
                                strokeWidth={2}
                                progressPercent={getCompletion(profile.tagCount, "tags")}
                                color={PRIMARY_COLOR}
                                textColor={"black"}
                                textSize={20}
                                text={profile.tagCount}
                            />
                            <AppText style={[theme.infoText, styles.statPanel]}>{translate("tags").toUpperCase()}</AppText>
                        </View>
                    </View>

                </ScrollView>
    );
}
export default BaseProfileComponent


const styles = StyleSheet.create({
    statPanel: {
        textAlign: "center",
        paddingBottom: 4,
        color: "black"
    }
});
