import { useRoute } from "@react-navigation/native";
import React from 'react';
import { ImageBackground } from "react-native";
import theme from "src/theme/theme";
import { BG_IMAGE } from "../../assets/data/app.data";
import GenericHeaderComponent from "../util/generic-header.component";
import BaseProfileComponent from "./base-profile.component";

const ProfileComponent: React.FC = () => {
    const route = useRoute();
    let { profile } = route.params;
    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

            <GenericHeaderComponent
                title={profile.username}
            />
            <BaseProfileComponent profile={profile} />
        </ImageBackground>
    );
}
export default ProfileComponent


