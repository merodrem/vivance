import { useNavigation } from "@react-navigation/native";
import React, { MutableRefObject, useRef, useState } from 'react';
import { ImageBackground, Keyboard, KeyboardAvoidingView, StyleSheet } from "react-native";
import { View } from "react-native-animatable";
import { Icon, Input } from "react-native-elements";
import { BG_IMAGE } from "src/assets/data/app.data";
import { validateEmail } from "src/services/email.service";
import * as ParseServer from "src/services/parse/parse.service";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { AppButton } from "../util/app-button.component";
import { AppInput } from "../util/app-input.component";
import { AppText } from "../util/app-text.component";
import ErrorMessageComponent from "../util/errorMessage.component";

const ResetPasswordComponent: React.FC = (props) => {
    const [isLoading, setLoading] = useState(false);
    const [isEmailSent, setEmailSent] = useState(false);
    const [isEmailValid, setEmailValid] = useState(true);
    const [emailInput, setEmailInput] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const emailRef: MutableRefObject<Input | null> = useRef(null);

    const navigation = useNavigation();

    function onSubmit(){
        Keyboard.dismiss()
        setEmailValid(validateEmail(emailInput) || emailRef.current.shake());
        if (validateEmail(emailInput)) {
            setLoading(true)
            ParseServer.changePassword(emailInput)
                .then(() => setEmailSent(true))
                .catch(error => setErrorMessage(error.message))
                .finally(() => setLoading(false))
        }

    }
    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>   
            <View style={{ flex: 1, paddingHorizontal: "5%", justifyContent: "center" }}>
                <AppButton
                    title={"Retour à l'écran de connexion"}
                    containerStyle={{ position: "absolute", top: 0 }}
                    type="clear"
                    icon={<Icon
                        name="chevron-left"
                        type="feather"
                        size={24}
                        color="white"
                    />
                    }
                    loadingProps={{ color: PRIMARY_COLOR }}
                    titleStyle={{ color: "white" }}
                    onPress={() => navigation.goBack()}
                />
                <AppText h3 style={{ color: 'white', textAlign: "center", marginBottom: 16 }}>Réinitialiser le mot de passe</AppText>
                <AppText style={{ color: 'white', textAlign: "center" }}>Entrez votre adresse email afin qu'un lien pour réinitialiser votre mot de passe puisse vous être envoyé.</AppText>
                <KeyboardAvoidingView>
                <AppInput
                    containerStyle={{ paddingHorizontal: 0 }}
                    inputContainerStyle={styles.inputContainer}
                    inputStyle={styles.input}
                    placeholderTextColor='rgba(255,255,255,0.4)'
                    keyboardType="email-address"
                    returnKeyType="done"
                    autoCapitalize="none"
                    placeholder="Email"
                    ref={emailRef}
                    onChangeText={text => setEmailInput(text)}
                    errorStyle={{ marginTop: 0 }}
                    onSubmitEditing={onSubmit}
                    errorMessage={isEmailValid ? "" : "Entrez une adresse email valide"}
                />
                <AppButton
                    title={isEmailSent ? 'Email envoyé ' : 'Envoyer'}
                    containerStyle={{ marginVertical: 8 }}
                    loading={isLoading}
                    disabled={isEmailSent}
                    loadingProps={{ color: PRIMARY_COLOR }}
                    buttonStyle={{ backgroundColor: 'white', borderRadius: 5 }}
                    titleStyle={{ color: PRIMARY_COLOR }}
                    onPress={onSubmit}
                    iconRight
                    icon={isEmailSent &&
                        <Icon
                            name="md-happy"
                            type="ionicon"
                            size={24}
                            color="grey"
                        />
                    }
                />
                {errorMessage.length > 0 &&
                    <ErrorMessageComponent message={errorMessage} />
                }
                </KeyboardAvoidingView>
            </View>
        </ImageBackground>
    );
}
export default ResetPasswordComponent

const styles = StyleSheet.create({
    inputContainer: {
        borderBottomWidth: 1,
        borderBottomColor: "white",
        width: "90%",
        alignSelf: "center"
    },
    input: {
        marginHorizontal: 0,
        color: 'white',
    },

});