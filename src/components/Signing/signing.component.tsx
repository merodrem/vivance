import { useNavigation } from "@react-navigation/native";
import React, { MutableRefObject, useEffect, useRef, useState } from 'react';
import { Image, ImageBackground, Keyboard, Linking, StyleSheet, UIManager, View } from "react-native";
import { CheckBox, Icon, Input } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import { BG_IMAGE, SCREEN_HEIGHT, SCREEN_WIDTH } from "src/assets/data/app.data";
import { validateEmail } from "src/services/email.service";
import { translate } from "src/services/i18n.service";
import * as ParseServer from "src/services/parse/parse.service";
import { capitalizeFirstLetter } from "src/services/string.service";
import { SET_NOTES } from "src/store/reducers/notes/notes.action";
import { SET_SOMS } from "src/store/reducers/StatesOfMind/states-of-mind.action";
import { TOGGLE_LOGIN } from "src/store/reducers/user/user.action";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { SET_FAVS } from "../../store/reducers/Favorites/favorites.action";
import { AppButton } from "../util/app-button.component";
import { AppInput } from "../util/app-input.component";
import { AppText } from "../util/app-text.component";
import ErrorMessageComponent from "../util/errorMessage.component";

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const CAROUSEL_DATA = [
    {
        title: "",
        text: "",
        image: require("src/assets/img/logo.png")
    },
    {
        title: "Apprendre à gérer ses émotions",
        text: "Bienvenue dans ton espace intérieur. Ici, tu entres dans un lieu qui nʼappartient quʼà toi. Un lieu calme et apaisant pour venir y déposer tes émotions difficiles, et retrouver un bien être intérieur.\n\nGrace à un parcours guidé dʼintrospection, tu va apprendre à mettre des mots sur les émotions qui te traversent. En leur apportantune présence empathique, tu pourra les vivre avec plus de paix.",
    },
    {
        title: "Comprendre son fonctionnement intérieur",
        text: "Nous avons tous un fonctionnement intérieur inconscient. Entre nos croyances, nos blessures émotionnelles ou encore notre manièrede réfléchir, il est parfois difficile dʼy voir clair. \n\nA lʼaide dʼoutils et de test de personalité, tu pourra identifier tesshémas mentaux pour comprendre comment dépasser tes limites.",
    },
    {
        title: "Etre un peu meilleur chaque jour",
        text: "Le chemin du bien être peut être long et difficle à traverser seul. Ici, nous te proposons de nombreux exercices et activités pour développer tes capacités ou simplement te détendre.\n\n Tu peux également partager tes propres routines sur lʼapplication et tʼessayer aux routines proposées par dʼautres.",
    },
]
const SigningComponent: React.FC = (props) => {

    const [activeSlide, setActiveSlide] = useState(0);

    const [gucAccepted, setGucAccepted] = useState(false)

    const [register, setRegister] = useState(true);
    const [isLoading, setLoading] = useState(false);
    const [isUsernameValid, setUsernameValid] = useState(true);
    const [isEmailValid, setEmailValid] = useState(true);
    const [isPasswordValid, setPasswordValid] = useState(true);
    const [isPasswordConfirmValid, setPasswordConfirmValid] = useState(true);
    const [usernameInput, setUsernameInput] = useState("");
    const [emailInput, setEmailInput] = useState("");
    const [passwordInput, setPasswordInput] = useState("");
    const [passwordConfirmInput, setPasswordConfirmInput] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const dispatch = useDispatch();

    const usernameRef: MutableRefObject<Input | null> = useRef(null);
    const emailRef: MutableRefObject<Input | null> = useRef(null);
    const passwordRef: MutableRefObject<Input | null> = useRef(null);
    const passwordConfirmRef: MutableRefObject<Input | null> = useRef(null);

    const navigation = useNavigation();

    useEffect(() => {
        dispatch({ type: 'APP_OPENED' });
    }, [])

    function _renderCarousel(item) {
        return (
            <View style={{ alignSelf: "center", padding: 32, justifyContent: "center" }}>
                {
                    item.image ?
                        <Image source={require('src/assets/img/logo.png')} style={styles.image} />
                        :
                        <View>
                            <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                <Icon
                                    name="check"
                                    type="font-awesome"
                                    reverse
                                    size={16}
                                    reverseColor="white"
                                    color={PRIMARY_COLOR}
                                />
                                <AppText style={{ fontWeight: "bold" }}>{item.title}</AppText>
                            </View>
                            <AppText>{item.text}</AppText>
                        </View>
                }
            </View>
        );
    }

    async function _signIn() {
        setLoading(true);

        if (register) {
            ParseServer.signUp(usernameInput, emailInput, passwordInput)
                .then(user => {
                    ParseServer.signIn(emailInput, passwordInput)
                        .then(user => dispatch({ type: TOGGLE_LOGIN, payload: user }))
                })
                .catch(error => {
                    switch (error.code) {
                        default:
                            setErrorMessage(error.message)
                            break;
                    }
                })
                .finally(() => setLoading(false))
        }
        else {
            try {

                const user = await ParseServer.signIn(emailInput, passwordInput);
                let avatar = undefined;
                try {
                    const image = await RNFetchBlob.config({ fileCache: true }).fetch('GET', user.avatar.url);
                    avatar = await image.readFile('base64')
                } catch (error) {
                    avatar = undefined;
                }
                const [SOMs, notes, favorites] = await Promise.all([ParseServer.loadUserStatesOfMind(), ParseServer.getNotes(), ParseServer.getFavoriteExercises(user.favoriteExercises)])
                
                dispatch({ type: SET_SOMS, payload: SOMs })
                dispatch({ type: SET_FAVS, payload: favorites })
                dispatch({ type: SET_NOTES, payload: notes })
                dispatch({ type: TOGGLE_LOGIN, payload: {...user, avatar} })

            } catch (error) {
                switch (error.code) {
                    case 101:
                        setErrorMessage("impossible de se connecter. Email ou mot de passe invalides.")
                        break;
                    default:
                        setErrorMessage(error.message)
                        break;
                }
            } finally {
                setLoading(false)
            }
        }

    }

    function onSubmit() {
        Keyboard.dismiss()
        setEmailValid(validateEmail(emailInput) || emailRef.current.shake());
        setPasswordValid(passwordInput.length > 4 || passwordRef.current.shake());
        if (register) {
            setUsernameValid(usernameInput.length > 1 || usernameRef.current.shake())
            setPasswordConfirmValid(passwordInput == passwordConfirmInput || passwordConfirmRef.current.shake());
        }

        if (validateEmail(emailInput) && passwordInput.length > 4) {
            if (!register || (passwordInput == passwordConfirmInput && usernameInput.length > 1 && gucAccepted))
                _signIn();
        }

    }

    return (


        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <KeyboardAwareScrollView
            contentContainerStyle={{ minHeight:SCREEN_HEIGHT*0.95}}
                resetScrollToCoords={{ x: 0, y: 0 }}
            >
                <Carousel
                    data={CAROUSEL_DATA}
                    renderItem={({ item }) => _renderCarousel(item)}
                    onSnapToItem={(index: number) => setActiveSlide(index)}
                    sliderWidth={SCREEN_WIDTH}
                    itemWidth={SCREEN_WIDTH}
                    layout='default'
                />
                <Pagination
                    dotsLength={4}
                    activeDotIndex={activeSlide}
                    tappableDots
                    containerStyle={{ paddingTop: 0 }}
                    dotStyle={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        borderWidth: 1,
                        borderColor: 'white',
                        margin: 0,
                        padding: 0,
                        backgroundColor: 'white'
                    }}
                    inactiveDotStyle={{
                        opacity: 1,
                        backgroundColor: 'transparent'

                    }}
                    inactiveDotOpacity={1}
                    inactiveDotScale={0.6}
                />
                <View
                    style={{ width: "90%",flex:1, alignSelf: "center" }}
                >
                    {
                        register &&
                        <AppInput
                            containerStyle={{ paddingHorizontal: 0 }}
                            inputContainerStyle={styles.inputContainer}
                            inputStyle={styles.input}
                            placeholderTextColor='rgba(255,255,255,0.7)'
                            returnKeyType="next"
                            autoCapitalize="none"
                            placeholder={capitalizeFirstLetter(translate("username"))}
                            ref={usernameRef}
                            onChangeText={text => setUsernameInput(text)}
                            onSubmitEditing={() => emailRef.current?.focus()}
                            errorStyle={{ marginTop: 0 }}
                            errorMessage={isUsernameValid ? "" : "Votre nom doit faire minimum 2 charactères"}
                        />
                    }
                    <AppInput
                        containerStyle={{ paddingHorizontal: 0 }}
                        inputContainerStyle={styles.inputContainer}
                        inputStyle={styles.input}
                        placeholderTextColor='rgba(255,255,255,0.7)'
                        keyboardType="email-address"
                        returnKeyType="next"
                        autoCapitalize="none"
                        placeholder="Adresse email"
                        ref={emailRef}
                        onSubmitEditing={() => passwordRef.current?.focus()}
                        onChangeText={text => setEmailInput(text)}
                        errorStyle={{ marginTop: 0 }}
                        errorMessage={isEmailValid ? "" : "Entrez une adresse email valide"} />
                    <AppInput
                        containerStyle={{ paddingHorizontal: 0 }}
                        inputContainerStyle={styles.inputContainer}
                        inputStyle={styles.input}
                        placeholderTextColor='rgba(255,255,255,0.7)'
                        secureTextEntry={true}
                        returnKeyType={register ? 'next' : 'done'}
                        placeholder="Mot de passe"
                        autoCapitalize="none"
                        ref={passwordRef}
                        onChangeText={text => setPasswordInput(text)}
                        onSubmitEditing={() => register ? passwordConfirmRef.current?.focus() : onSubmit()}
                        errorStyle={{ marginTop: 0 }}
                        errorMessage={isPasswordValid ? "" : "Entrez au moins 5 charactères"}
                    />
                    {
                        register &&
                        <>
                            <AppInput
                                containerStyle={{ paddingHorizontal: 0 }}
                                inputContainerStyle={styles.inputContainer}
                                inputStyle={styles.input}
                                placeholderTextColor='rgba(255,255,255,0.7)'
                                secureTextEntry={true}
                                returnKeyType="done"
                                autoCapitalize="none"
                                placeholder="Confirmer le mot de passe"
                                ref={passwordConfirmRef}
                                onChangeText={text => setPasswordConfirmInput(text)}
                                onSubmitEditing={onSubmit}
                                errorStyle={{ marginTop: 0 }}
                                errorMessage={isPasswordConfirmValid ? "" : "Les mots de passe ne correspondent pas"}
                            />
                            <View style={theme.panelContainer}>
                                <AppText style={{ textAlign: "left", fontSize: 14, color: 'black' }}>
                                    Pour utiliser lʼapplication, merci de lire et dʼaccepter les Conditions Générales dʼUtilisation
                            </AppText>
                                <View style={{ flexDirection: "row", maxWidth: "100%", marginVertical: 4, alignItems: "center", flexWrap: "wrap" }}>
                                    <CheckBox
                                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
                                        checkedColor={PRIMARY_COLOR}
                                        checked={gucAccepted}
                                        wrapperStyle={{ width: 10 }}
                                        onIconPress={() => setGucAccepted(!gucAccepted)}
                                    />
                                    <View style={{ width: "80%" }}>
                                        <AppText
                                            style={{ fontSize: 14, color: "black" }}>{"Jʼai lu et jʼaccepte les "}
                                        </AppText>
                                        <TouchableOpacity
                                            onPress={() => Linking.openURL("http://vivance-app.fr/conditions-generales-dutilisation").catch(err => console.log('An error occurred', err))}
                                        >

                                            <AppText style={{ textDecorationLine: "underline", fontSize: 14, color: PRIMARY_COLOR }}
                                            >Conditions Générales dʼUtilisation.</AppText>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                        </>
                    }
                    <AppButton
                        title={register ? 'Créer le compte' : 'Connexion'}
                        containerStyle={{ marginVertical: 8, alignSelf: "center" }}
                        loading={isLoading}
                        disabled={register && !gucAccepted}
                        loadingProps={{ color: "white" }}
                        buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 5, paddingHorizontal: 16 }}
                        titleStyle={{ color: "white" }}
                        onPress={onSubmit}
                    />
                    
                    {errorMessage.length > 0 &&
                        <ErrorMessageComponent message={errorMessage} />
                    }
                </View>
                <View style={{flexDirection: 'row', justifyContent: "space-between", paddingTop: 32, paddingBottom:8, width:"90%", alignSelf:"center" }}>
                        <AppButton
                            type="clear"
                            titleStyle={{ color: 'white' }}
                            buttonStyle={{ padding: 0 }}
                            title="Mot de passe oublié?"
                            onPress={() => navigation.navigate("ResetPassword")}
                        />
                        <AppButton
                            type="clear"
                            buttonStyle={{ padding: 0 }}
                            titleStyle={{ color: 'white' }}
                            onPress={() => {
                                // LayoutAnimation.easeInEaseOut();
                                setRegister(!register)
                            }
                            }
                            title={register ? 'Déjà membre' : 'Créer un compte'}
                        />
                    </View>
            </KeyboardAwareScrollView>
        </ImageBackground >
    );
}
export default SigningComponent

const styles = StyleSheet.create({
    image: {
        alignSelf: 'center',
        maxWidth: 250,
        maxHeight: 250
    },
    inputContainer: {
        borderBottomWidth: 1,
        borderBottomColor: "white",
        width: "90%",
        alignSelf: "center"
    },
    input: {
        marginHorizontal: 0,
        color: 'white',
    },


});