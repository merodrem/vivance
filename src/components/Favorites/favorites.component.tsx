import { useNavigation } from "@react-navigation/native";
import React from 'react';
import { FlatList, ImageBackground, View } from "react-native";
import { Icon } from "react-native-elements";
import { useSelector } from "react-redux";
import { BG_IMAGE } from "src/assets/data/app.data";
import theme from "src/theme/theme";
import { Exercise } from "../../models/exercise.model";
import { AppButton } from "../util/app-button.component";
import { AppText } from "../util/app-text.component";
import { ExerciseListItemComponent } from "../util/exercise-list-item.component";
import GenericHeaderComponent from "../util/generic-header.component";


function FavoritesComponent() {
    const favorites: Array<Exercise> = useSelector(state => state.favs);
    const navigation = useNavigation();

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

                <GenericHeaderComponent title="Favoris" />
                {favorites.length > 0 ?
                    <>
                        <FlatList
                            data={favorites.reverse()}
                            extraData={favorites}
                            renderItem={({ item }) => {
                                return (
                                    <View style={{ width: "90%", alignSelf: "center" }}>
                                        <ExerciseListItemComponent
                                            {...item}
                                            key={item.objectId}
                                        />
                                    </View>
                                );
                            }}
                            keyExtractor={item => (item)}
                        />
                    </>
                    :
                    <View>
                        <AppText h3 style={[theme.headerText, { color: 'white', textAlign: 'center' }]}>
                            Aucun favori ajouté.
            </AppText>
                        <AppButton
                            containerStyle={{ alignSelf: 'center' }}
                            buttonStyle={{ paddingHorizontal: 16, paddingVertical: 8, marginTop: 32, borderColor: 'white' }}
                            titleStyle={{ color: 'white' }}
                            type="outline"
                            title={"hop hop"}
                            icon={<Icon
                                name="chevron-left"
                                type="feather"
                                size={24}
                                color="white"
                            />
                            }
                            onPress={() => navigation.navigate("Exercises")}
                        />
                    </View>
                }
            </ImageBackground>
        </View>
    );
}
export default FavoritesComponent

