import React, { useEffect, useState } from 'react';
import { EmitterSubscription, ImageBackground, LayoutAnimation, Linking, Platform, ScrollView, StyleSheet, UIManager, View } from "react-native";
import { Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import RNIap, {
    finishTransaction, InAppPurchase, ProductPurchase,
    PurchaseError, purchaseErrorListener,
    purchaseUpdatedListener,





    requestSubscription, SubscriptionPurchase
} from 'react-native-iap';
import { BG_IMAGE, SCREEN_HEIGHT, SCREEN_WIDTH } from "src/assets/data/app.data";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { IAP_SUBS } from "../../assets/data/app.data";
import { AppBadge } from "../util/app-badge.component";
import { AppText } from "../util/app-text.component";

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const GREEN = "#049f04"

const PremiumComponent: React.FC = () => {
    const [showMore, setShowMore] = useState(false);
    const [selectedSub, setSelectedSub] = useState("annual_subscription");
    let purchaseUpdateSubscription: EmitterSubscription | null = null;
    let purchaseErrorSubscription: EmitterSubscription | null = null;

    function _subscribe() {
        requestSubscription(selectedSub)
            .catch(error => console.log(error.message))
    }
    const _processNewPurchase = async (purchase: any) => {

        const { productId, transactionReceipt } = purchase;
    }

    useEffect(() => {
        RNIap.initConnection().then(() => {
            RNIap.getSubscriptions(IAP_SUBS)

            // we make sure that "ghost" pending payment are removed
            // (ghost = failed pending payment that are still marked as pending in Google's native Vending module cache)
            RNIap.flushFailedPurchasesCachedAsPendingAndroid().catch(() => {
                // exception can happen here if:
                // - there are pending purchases that are still pending (we can't consume a pending purchase)
                // in any case, you might not want to do anything special with the error

            }).then(() => {
                purchaseUpdateSubscription = purchaseUpdatedListener(async (purchase: InAppPurchase | SubscriptionPurchase | ProductPurchase) => {
                    const receipt = purchase.transactionReceipt


                    if (receipt) {
                        try {
                            await _processNewPurchase(purchase);
                            await finishTransaction(purchase);
                        } catch (ackErr) {
                            console.warn('ackErr', ackErr);
                        }
                    }
                },
                );

                purchaseErrorSubscription = purchaseErrorListener(
                    (error: PurchaseError) => {
                        console.log('purchaseErrorListener', error);
                    },
                );
            });
        })
        return (() => {
            if (purchaseUpdateSubscription) {
                purchaseUpdateSubscription.remove();
                purchaseUpdateSubscription = null;
            }
            if (purchaseErrorSubscription) {
                purchaseErrorSubscription.remove();
                purchaseErrorSubscription = null;
            }
        })
    }, []);

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <ScrollView style={{ width: '90%', alignSelf: "center" }}
                showsVerticalScrollIndicator={false}
            >
                <AppText h3 style={{ fontWeight: "bold", marginTop: 32 }}>OFFRE DE LANCEMENT</AppText>
                <AppText h4 style={{ fontWeight: "bold" }}>{"Débloquez toute les fonctionnalités.\nOffre à durée limitée !"}</AppText>
                <AppText h4 style={{ fontWeight: "bold", color: 'white', marginVertical: 16 }}>Choisissez le bien être intérieur</AppText>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Icon
                        name="check"
                        type="feather"
                        reverse
                        containerStyle={{ marginLeft: 0, marginRight: 16 }}
                        color={GREEN}
                        size={14}
                    />
                    <AppText style={{ fontWeight: "bold" }}>Plus de 120 exercices officiels</AppText>
                </View>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Icon
                        name="check"
                        type="feather"
                        reverse
                        containerStyle={{ marginLeft: 0, marginRight: 16 }}
                        color={GREEN}
                        size={14}
                    />
                    <AppText style={{ fontWeight: "bold" }}>Créez vos propres exercices</AppText>
                </View>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Icon
                        name="check"
                        type="feather"
                        reverse
                        containerStyle={{ marginLeft: 0, marginRight: 16 }}
                        color={GREEN}
                        size={14}
                    />
                    <AppText style={{ fontWeight: "bold" }}>Tests de personnalité et dʼintelligence</AppText>
                </View>
                <View style={{ flexDirection: 'row', alignItems: "center" }}>
                    <Icon
                        name="check"
                        type="feather"
                        reverse
                        containerStyle={{ marginLeft: 0, marginRight: 16 }}
                        color={GREEN}
                        size={14}
                    />
                    <AppText style={{ fontWeight: "bold" }}>Trophés exclusifs</AppText>
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <TouchableOpacity
                        onPress={() => setSelectedSub('monthly_subscription')}
                        style={[styles.subscriptionPanel, selectedSub === 'monthly_subscription' && { borderWidth: 4, borderColor: GREEN }]}
                    >

                        <AppText h4>PAR MOIS</AppText>
                        <AppText style={{ fontWeight: "bold" }}>4,99€/mois</AppText>
                        <AppText style={{ fontSize: 12, textAlign: "center" }}>Facturé mensuellement</AppText>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setSelectedSub('annual_subscription')}
                    >
                        <View style={[styles.subscriptionPanel, selectedSub === 'annual_subscription' && { borderWidth: 4, borderColor: GREEN }]}>
                            <AppBadge
                                value="Economisez 30%"
                                containerStyle={{ position: "absolute", top: -13, zIndex: 1, }}
                                badgeStyle={{ backgroundColor: GREEN, borderWidth: 0, height: 26 }}
                            />
                            <AppText h4>ANNUEL</AppText>
                            <AppText style={{ fontWeight: "bold" }}>3,5€/mois</AppText>
                            <AppText style={{ fontSize: 12, textAlign: "center" }}>42€ Facturés annuellement</AppText>
                        </View>
                    </TouchableOpacity>
                </View>
                <AppText style={{ textAlign: "center", fontSize: 14 }}>Facturation réccurente. Annulez à tout moment. En vous abonnant vous acceptez 
                <TouchableOpacity
                    onPress={() => Linking.openURL("http://vivance-app.fr/conditions-generales-dutilisation").catch(err => console.log('An error occurred', err))}>
                   <AppText style={{ textDecorationLine: "underline" }}>les conditions de ventes et d’utilisations.</AppText>
                </TouchableOpacity>
                </AppText>
                <TouchableOpacity
                style={{marginVertical:16}}
                    onPress={() => {
                        LayoutAnimation.easeInEaseOut();
                        setShowMore(!showMore)
                    }
                    }>
                    <AppText style={{ textAlign: "center" }}>{showMore ? "cacher" : "En savoir plus"}</AppText>
                    <Icon
                        name={showMore ? "chevron-up" : "chevron-down"}
                        type="entypo"
                        color="white"
                    />
                </TouchableOpacity>
                {
                    showMore &&
                    <>{
                        Platform.OS === 'android' ?
                            <AppText>{`
Votre compte Google Play sera dʼabord facturé après la fin de la période dʼessai gratuit. Lʼabonnement mensuel est de 4,99€ et se renouvelle automatiquement tout les mois. Lʼabonnement annuel, facturé comme un unique paiement initial, sʼélève à 42€ et se renouvelle automatiquement chaque année. 
Lʼabonnement sera renouvelé automatiquement dans les 24h précédent la fin de la période actuelle. Vous pouvez gérez lʼabonnement et le désactiver depuis les règlages de votre compte Google Play
                    `}</AppText>
                            :
                            <AppText>fuck l'iphone</AppText>
                    }</>
                }
                <TouchableOpacity
                    style={styles.trialButton}
                    activeOpacity={0.6}
                    onPress={_subscribe}
                >
                    <AppText h4 style={{ fontWeight: "bold", paddingBottom: 4 }}>Commencer lʼessai gratuit</AppText>
                    <AppText style={{ fontSize: 12 }}>Essayez pendant 11 jours, puis {selectedSub == 'monthly_subscription' ? '4,99' : '42'}€ par {selectedSub == 'monthly_subscription' ? 'mois' : 'an'}</AppText>
                </TouchableOpacity>
            </ScrollView>
        </ImageBackground >
    );
}
export default PremiumComponent


const styles = StyleSheet.create({
    subscriptionPanel: {
        ...theme.darkPanel,
        alignItems: "center",
        justifyContent: "space-between",
        width: SCREEN_WIDTH * 0.4,
        height: SCREEN_HEIGHT / 5,
        marginTop: 16
    },
    trialButton: {
        backgroundColor: GREEN,
        borderWidth: 2,
        borderColor: PRIMARY_COLOR,
        borderRadius: 10,
        padding: 16,
        alignItems: "center",
        marginBottom: 16
    }
});
