import React from "react";
import { Text, TextProps } from "react-native-elements";


type Props = {
    children?: string
} & TextProps

export function AppText(props: Props) {
    return (
        <Text style={[{ fontFamily: "Raleway-Regular", fontSize: props.h4 ? 18 : props.h3 ? 24 : props.h2 ? 28 : props.h1? 32 : 15, color:"white" }, props.style]}>{props.children}</Text>
    );
}
