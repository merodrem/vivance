import React from 'react';
import { GestureResponderEvent } from 'react-native';
import theme from 'src/theme/theme';
import { AppButton } from './app-button.component';


type Props = {
    disabled?: boolean
    title?: string
    onPress: (event: GestureResponderEvent) => void
}

export const ButtonNextComponent: React.FC<Props> = (props) => {
    return (
        <AppButton
            title={props.title || "suivant"}
            containerStyle={theme.nextbuttonContainer}
            buttonStyle={theme.outlineButton}
            titleStyle={{ color: 'white'}}
            disabled={props.disabled}
            onPress={ props.onPress}
        />
    );
}