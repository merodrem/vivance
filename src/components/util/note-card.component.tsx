import React, { useState } from "react";
import { View } from "react-native-animatable";
import { Divider, Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import Toast from 'react-native-simple-toast';
import { useDispatch } from "react-redux";
import { Note } from "src/models/note.model";
import { StateOfMind } from "src/models/state-of-mind.model";
import { getColor } from "src/services/color.service";
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from "src/services/string.service";
import { DELETE_NOTE, UPDATE_NOTE } from "src/store/reducers/notes/notes.action";
import theme from "src/theme/theme";
import { intensityTranslation } from "../../services/need.service";
import { AppBadge } from "./app-badge.component";
import { AppText } from "./app-text.component";
import ConfirmModalComponent from "./confirm-modal.component";
import InputModalComponent from "./input-modal.component";
import { IntensityTextComponent } from "./intensity-text";


const monthNames = ["Jan", "Fév", "Mar", "Avr", "Mai", "Juin",
    "Juil", "aout", "Sep", "Oct", "Nov", "Déc"
];

type Props = (Note | StateOfMind)
    &
{
    hideTags: boolean
}

export const NoteCardComponent: React.FC<Note> = (props: Props) => {
    const day = new Date(props.timestamp).getDate()
    const month = monthNames[new Date(props.timestamp).getMonth()]
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const [showInputModal, setShowInputModal] = useState(false);
    const hour = new Date(props.timestamp).getHours().toString()
    const minutes = new Date(props.timestamp).getUTCMinutes() < 10 ? `0${new Date(props.timestamp).getUTCMinutes().toString()}` : new Date(props.timestamp).getUTCMinutes().toString()
    const dispatch = useDispatch();
    const isSOM = !("type" in props);
    return (
        <>
            <ConfirmModalComponent
                text="Êtes-vous sûr de vouloir supprimer cette note?"
                onHide={() => setShowConfirmModal(false)}
                isVisible={showConfirmModal}
                onConfirm={() => {
                    ParseServer.deleteNote(props.objectId)
                        .then(() => {
                            dispatch({ type: DELETE_NOTE, payload: props.objectId })
                        })
                        .catch(error => Toast.show("Impossible de supprimer la note: " + error, Toast.LONG))
                        .finally(() => setShowConfirmModal(false))
                }
                }
            />
            <InputModalComponent
                isVisible={showInputModal}
                onHide={() => setShowInputModal(false)}
                defaultText={props.content}
                onSubmit={(content) => {
                    ParseServer.editNote(props.objectId, content)
                        .then((note: Note) => {
                            setShowInputModal(false);
                            dispatch({ type: UPDATE_NOTE, payload: note })
                        })
                        .catch(error => {
                            Toast.show("Impossible de mettre à jour la note: " + error.message, Toast.LONG)
                        }
                        )
                }}
            />
            {!isSOM &&
                <View style={{ flexDirection: 'row', paddingRight: 8, justifyContent: "flex-end" }}>
                    <TouchableOpacity
                        style={{ marginRight: 24 }}
                        onPress={() => setShowInputModal(true)}
                    >
                        <Icon
                            name="edit"
                            color="white"
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setShowConfirmModal(true)}
                    >
                        <Icon
                            name="trash"
                            type="font-awesome-5"
                            color="white"
                        />
                    </TouchableOpacity>
                </View>
            }
            <View style={[theme.darkPanel, { width: "100%", paddingBottom:4 }]}>
                <View style={{ flexDirection: "row", justifyContent: "space-between", flexWrap: "wrap" }}>
                    <AppText>{(isSOM ? capitalizeFirstLetter(translate("somNote")) : capitalizeFirstLetter(translate(`${props.type}Note`))) + ` - ${day} ${month} : ${hour}h${minutes}`}</AppText>

                </View>
                <Divider style={{ marginVertical: 8 }} />
                {!props.hideTags && <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {
                        props.tags.map(tag =>
                            <AppBadge
                                key={tag}
                                value={props.type === "exercise" ? tag : capitalizeFirstLetter(translate(tag))}
                                badgeStyle={[theme.badge, { backgroundColor: getColor(tag), paddingVertical: 12 }]}
                                textStyle={{ fontSize: 16, }}
                            />
                        )
                    }
                </View>}
                {
                    isSOM &&
                    <>
                        <View style={{ paddingBottom: 8 }}>
                            <AppText>Dans la situation: <AppText style={{ fontWeight: "bold" }}>{translate(props.situation.type)}</AppText></AppText>
                            {!!props.situation.details && <AppText style={{ fontStyle: "italic" }}>"{props.situation.details}"</AppText>}
                        </View>
                        {
                            props.emotions.map(emo =>
                                <AppText
                                    key={emo.name}>
                                    Je me suis senti(e)
                                    <IntensityTextComponent
                                        intensity={Math.round(((emo.intensity - 10) / 10) * (8 / 9))}//mapping [10;100] to [0;8]
                                    >
                                        {intensityTranslation[Math.round(((emo.intensity - 10) / 10) * (8 / 9))].label.toLowerCase()}
                                    </IntensityTextComponent>
                                    {translate(emo.name).toLowerCase()}
                                </AppText>
                            )
                        }
                        <AppText
                            style={{ paddingBottom: 8 }}>
                            Car j'ai
                            <IntensityTextComponent
                                intensity={Math.round(((props.need.intensity - 10) / 10) * (8 / 9))}//mapping [10;100] to [0;8]
                            >
                                {intensityTranslation[Math.round(((props.need.intensity - 10) / 10) * (8 / 9))].label.toLowerCase()}
                            </IntensityTextComponent>
                            besoin {translate(props.need.name).match('^[aieouAIEOU].*') ? 'd\'' : 'de '}{translate(props.need.name).toLowerCase()}
                        </AppText>
                        {!!props.note && <AppText style={{ fontStyle: "italic" }}>"{props.note}"</AppText>}

                    </>
                }
                <AppText>{props.content}</AppText>
            </View >

        </>
    );
}
