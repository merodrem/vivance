import { useNavigation } from "@react-navigation/native";
import React from 'react';
import { Header, Icon } from "react-native-elements";
import { AppButton } from './app-button.component';

function GenericHeaderComponent(props:{title:string}){
    const navigation = useNavigation();
    return (
        <Header
                    containerStyle={{ backgroundColor: "transparent", borderBottomWidth:0 }}
                    leftComponent={<AppButton
                        onPress={navigation.goBack}
                        type="clear"
                        icon={<Icon
                            name="keyboard-arrow-left"
                            color='white'
                            size={35}
                        />}
                    />}
                    centerComponent={{ text: props.title, style: { color: 'white', fontWeight: 'bold', fontSize: 18, fontFamily:"Raleway-Regular" } }}
                />
    );
}

export default GenericHeaderComponent