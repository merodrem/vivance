
import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { ModalBaseProps } from 'src/models/modalBaseProps.model';
import theme from 'src/theme/theme';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { AppButton } from './app-button.component';
import { AppText } from './app-text.component';


type Props = {
    text: string
    onConfirm: () => void,
} & ModalBaseProps

function ConfirmModalComponent(props: Props) {
    return (
        <Modal deviceWidth={SCREEN_WIDTH}
            isVisible={props.isVisible}
            deviceHeight={SCREEN_HEIGHT}
            backdropColor="black"
            hideModalContentWhileAnimating={true}
            >
            <View style={{}}>
                <View style={[theme.panelContainer, {padding:16}]}>
    <AppText style={{color:'black'}}>{props.text}</AppText>
                </View>
                <View style={{flexDirection:'row', justifyContent:"space-evenly"}}>
                <AppButton
                    title="Annuler"
                    containerStyle={{ borderRadius: 5 }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 0, }}
                    onPress={props.onHide}
                />
                <AppButton
                    title="Confirmer"
                    containerStyle={{ borderRadius: 5 }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 0, }}
                    onPress={props.onConfirm}
                />
                </View>
            </View>
        </Modal>
    );
}



export default ConfirmModalComponent