import { Picker } from "react-native";
import { createAnimatableComponent } from "react-native-animatable";
import { Text } from "react-native-elements";

export const AnimatedText = createAnimatableComponent(Text);
export const AnimatedPicker = createAnimatableComponent(Picker)
