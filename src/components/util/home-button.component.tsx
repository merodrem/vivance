import { useNavigation } from "@react-navigation/native";
import React from 'react';
import { Image, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const HomeButtonComponent: React.FC = () => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity
        style={styles.button}
            onPress={() => navigation.navigate("Home")}
        >
            <Image source={require('src/assets/img/logo.png')} style={{maxWidth:80,maxHeight:80}} />
        </TouchableOpacity>
    );
}
export default HomeButtonComponent

const styles = StyleSheet.create({
    button:{
        maxWidth:80,
        maxHeight:80,
    }
});