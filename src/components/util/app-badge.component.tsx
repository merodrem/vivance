import React from "react";
import { Badge, BadgeProps } from "react-native-elements";



export function AppBadge(props: BadgeProps) {
    return (
        <Badge {...props}
        textStyle={[props.textStyle, {fontFamily:"Raleway-Regular"}]}/>
    );
}
