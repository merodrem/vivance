import React from 'react';
import { View } from "react-native";
import { Slider } from 'react-native-elements';
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from 'src/theme/variables';
import { AppText } from './app-text.component';

type Props = {
    intensity: number,
    type: 'need' | 'emotion',
    onChange: (intensity: number) => void
}

export const IntensitySliderComponent: React.FC<Props> = (props) => {
    return (
        <View style={{flexDirection:'row', alignItems:'center', marginTop: 8}}>
            <AppText style={{marginRight: 8, fontSize:12}}>Intensité</AppText>
            <Slider
                style={[{ width: "70%" }]}
                thumbStyle={theme.sliderThumb}
                trackStyle={theme.sliderTrack}
                thumbTouchSize={{ width: 40, height: 40 }}
                allowTouchTrack
                value={props.intensity}
                maximumTrackTintColor="lightgray"
                minimumTrackTintColor="lightgray"
                onValueChange={val =>props.onChange(val)}
                thumbTintColor={PRIMARY_COLOR}
            />
            </View>
    );
}