import React from 'react';
import { intensityTranslation } from '../../services/need.service';
import { AppText } from './app-text.component';

type Props = {
    intensity: number,
}

export const IntensityTextComponent: React.FC<Props> = (props) => {
    return (
        <AppText style={ {
            textAlign: 'center', textShadowColor: 'black',
            fontSize:16,
            textShadowRadius: 2, color: intensityTranslation[props.intensity].color
        }}> {props.children} </AppText>
    );
}