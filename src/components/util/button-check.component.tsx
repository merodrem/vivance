import React from 'react';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { AppButton } from './app-button.component';


type Props = {
    color: string,
    label: string,
    isChecked: boolean,
    containerStyle ?: StyleProp<ViewStyle>
    buttonStyle ?: StyleProp<ViewStyle>
    titleStyle ?: StyleProp<TextStyle>
    onPress: (label: string) => void
}

export const ButtonCheckComponent: React.FC<Props> = (props) => {
    return (
        <AppButton
            title={props.label}
            containerStyle={props.containerStyle}
            buttonStyle={[{ backgroundColor: props.isChecked ? props.color : PRIMARY_COLOR }, props.buttonStyle]}
            titleStyle={[{ fontSize: 12 }, props.titleStyle]}
            onPress={() => props.onPress(props.label) }
        />
    );
}