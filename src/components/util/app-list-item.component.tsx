import React from "react";
import { ListItem, ListItemProps } from "react-native-elements";



export function AppListItem(props: ListItemProps) {
    return (
        <ListItem
            bottomDivider>
            <ListItem.Content>
                <ListItem.Title style={[props.titleStyle, { fontFamily: "Raleway-Regular" }]}>{props.title}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
        </ListItem>
    );
}
