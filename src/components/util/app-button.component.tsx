import React from "react";
import { Button, ButtonProps } from "react-native-elements";



export function AppButton(props:ButtonProps) {
    return (
        <Button {...props}
        titleStyle={[props.titleStyle, {fontFamily:"Raleway-Regular"}]}/>
    );
}
