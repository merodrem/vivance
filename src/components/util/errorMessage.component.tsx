import React from 'react';
import { StyleSheet } from "react-native";
import { AppText } from "./app-text.component";

type Props = {
    message: string
}
const ErrorMessageComponent: React.FC<Props> = (props: Props) => {
    return (
        <AppText
            style={styles.errorMessage}
        >
            {props.message}
        </AppText>
    );
}
export default ErrorMessageComponent

const styles = StyleSheet.create({
    errorMessage: {
        color: 'white',
        backgroundColor: 'salmon',
        textAlign: "center",
        padding: 8,
        marginTop: 8,
        alignSelf: "center"
    }
});