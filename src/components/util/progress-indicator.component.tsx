import React from 'react';
import { StyleSheet, View } from 'react-native';
import { PRIMARY_COLOR } from 'src/theme/variables';


type Props = {
    selected: boolean,
}

export const ProgressIndicatorComponent: React.FC<Props> = (props) => {
    return (
        <View style={[styles.container, props.selected && {backgroundColor: PRIMARY_COLOR}]}>

        </View>
    );
}

const styles= StyleSheet.create({
    container: {
        width: 8,
        marginHorizontal: 4,
        height: 8,
        borderRadius: 50,
        borderWidth:1,
        borderColor: PRIMARY_COLOR
    }
})