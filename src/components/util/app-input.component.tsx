import React from "react";
import { Input } from "react-native-elements";



export const AppInput = React.forwardRef((props, ref) => (
    <Input {...props}
        ref={ref}

        inputStyle={[props.inputStyle, { fontFamily: "Raleway-Regular", fontSize: 15 }]} />
));

