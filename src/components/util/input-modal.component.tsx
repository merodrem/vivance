import React, { useRef } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { View } from 'react-native-animatable';
import Modal from 'react-native-modal';
import theme from 'src/theme/theme';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { AppButton } from './app-button.component';
import { AppText } from './app-text.component';

type Props = {
    isVisible: boolean,
    defaultText?: string,
    loading: boolean,
    onHide: () => void,
    onSubmit: (text: string) => void
}
function InputModalComponent(props: Props) {

    let input = useRef(props.defaultText ?? "");

    return (
        <Modal isVisible={props.isVisible}
            onBackButtonPress={props.onHide}
        >
            <View style={styles.modalContainer}>
                <AppText h4 style={{ textAlign: 'center', margin: 8,  }}>
                    Dis nous tout
                    </AppText>
                <TextInput
                    defaultValue={props.defaultText ?? ""}
                    autoFocus
                    multiline={true}
                    textAlignVertical="top"
                    numberOfLines={4}
                    maxLength={500}
                    onChangeText={text => input.current = text}
                    style={theme.textInput}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <AppButton containerStyle={styles.modalButtonContainer} titleStyle={{ color: "lightsalmon" }} buttonStyle={{ borderColor: "lightsalmon" }} type="outline" title="annuler"
                        onPress={props.onHide} />
                    <AppButton containerStyle={styles.modalButtonContainer} title="confirmer"
                        buttonStyle={{ borderColor: "white", borderWidth:1, backgroundColor:"transparent" }}
                        loading={props.loading}
                        onPress={() => props.onSubmit(input.current)} />
                </View>
            </View>
        </Modal>
    );
}


const styles = StyleSheet.create({
    modalContainer: {
        backgroundColor: PRIMARY_COLOR,
        flex: 0,
        borderRadius: 20,
        padding: 16,
    },
    modalButtonContainer: {
        width: '50%',
        padding: 8
    }
});

export default InputModalComponent