import React from 'react';
import { StyleSheet, View } from "react-native";
import { Divider } from "react-native-elements";
import theme from "src/theme/theme";
import { AppText } from "./app-text.component";
import { ButtonNextComponent } from "./button-next.component";

type Props = {
    title: string,
    description?: string,
    buttonLabel: string,
    onPress: () => void
}

function LinkPanelComponent(props: Props) {
    return (
        <View style={theme.panelContainer}>
            <AppText style={theme.panelTitle}>{props.title}</AppText>
            <Divider />
            {props.description && <AppText style={[theme.commonText,{fontSize:13}]}>{props.description}</AppText>}
            <ButtonNextComponent
                title={props.buttonLabel}
                onPress={props.onPress}
            />
        </View>
    );
}
export default LinkPanelComponent

const styles = StyleSheet.create({
});
