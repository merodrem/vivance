import React from "react";
import { Circle, Svg, Text as SVGText } from 'react-native-svg';

type Props = {
    size: number|undefined, 
    strokeWidth: number, 
    text: string,
    progressPercent: number
    textSize: number|undefined,
    bgColor: string|undefined,
    color: string|undefined,
    textColor: string|undefined
}

const CircularProgressComponent = (props:Props) => {
  const { size, strokeWidth, text } = props;
  const radius = (size - strokeWidth) / 2;
  const circum = radius * 2 * Math.PI;
  const svgProgress = 100 - props.progressPercent;

  return (
      <Svg width={props.size? props.size : "100%"} height={size}>
        {/* Background Circle */}
        <Circle 
          stroke={props.bgColor ? props.bgColor : "lightgrey"}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={radius}
          {...{strokeWidth}}
        />
        
        {/* Progress Circle */}
        <Circle 
          stroke={props.color ? props.color : "#fff"}
          fill="none"
          cx={size / 2}
          cy={size / 2}
          r={radius}
          strokeDasharray={`${circum} ${circum}`}
          strokeDashoffset={radius * Math.PI * 2 * (svgProgress/100)}
          strokeLinecap="round"
          transform={`rotate(-90, ${size/2}, ${size/2})`}
          {...{strokeWidth}}
        />

        {/* Text */}
        <SVGText
          fontSize={props.textSize ? props.textSize : "10"}
          x={size / 2}
          y={size / 2 + (props.textSize ?  (props.textSize / 2) - 1 : 5)}
          textAnchor="middle"
          fill={props.textColor ? props.textColor : props.color}
        >
          {text}
        </SVGText>
      </Svg>
  )
}

export default CircularProgressComponent;