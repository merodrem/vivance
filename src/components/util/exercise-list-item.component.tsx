
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon, Image } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import RNFetchBlob from 'rn-fetch-blob';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { Exercise } from 'src/models/exercise.model';
import { translate } from 'src/services/i18n.service';
import theme from 'src/theme/theme';
import { AppText } from './app-text.component';


export const ExerciseListItemComponent = (props: Exercise) => {
    const navigation = useNavigation();
    const [imageLoading, setImageLoading] = useState(false);
    const [image, setImage] = useState(undefined);

    useEffect(() => {

        if (props.image.url) {
            setImageLoading(true);
            RNFetchBlob.config({ fileCache: true }).fetch('GET', props.image.url)
                .then(result => {
                    result.readFile('base64')
                        .then(base64Image => setImage({ filename: props.image.name, base64: base64Image }))
                }
                )
                .catch(error => console.log(error))
                .finally(() => setImageLoading(false));
        }
        else if (props.image.base64) {
            setImage(props.image)
        }
    }, []);

    return (
        <TouchableOpacity
            disabled={props.disabled}
            style={[theme.panelContainer, { width: SCREEN_WIDTH, height: SCREEN_HEIGHT / 8, alignSelf: "center", flexDirection: "row", padding: 0 }]}
            onPress={() => navigation.navigate("Exercise", { ...props, image: props.image && image })}
        >
            
            <Image
                resizeMode="cover"
                style={props.disabled ? {opacity:0.3} : {}}
                containerStyle={styles.image}
                source={(props.image && image) ? { uri: `data:image;base64,${image.base64}` } : require('src/assets/img/exerciseDefault.png')} /> 


            <AppText h4 style={{ color: "black", width: "50%", height: SCREEN_HEIGHT / 8, textAlign: "center", padding: 2 }}>{props.title + (props.disabled ? " (PREMIUM)" : "")}</AppText>

            <View style={{ width: "20%" }}>
                <View style={{ alignItems: "center", margin: 8, zIndex: 1, flexDirection: "row" }}>
                    <AppText style={{ fontWeight: "bold", color: "black" }}>{props.score} </AppText>
                    <Icon
                        name="trophy"
                        type="font-awesome"
                        size={14}
                        color="skyblue"
                    />
                </View>
                <AppText style={{ fontWeight: "bold", color: "black" }}>{translate(`duration${props.duration}`)}</AppText>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    image: {
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        height: SCREEN_HEIGHT / 8,
        width: "30%"
    }
});