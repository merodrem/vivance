import { StyleSheet, ImageBackground } from "react-native";
import React, { useState, useEffect } from 'react';
import {  Icon, Text, Image, Divider, Badge } from "react-native-elements";
import { PRIMARY_COLOR } from "src/theme/variables";
import GenericHeaderComponent from "../util/generic-header.component";
import { Exercise } from "src/models/exercise.model";
import ImagePicker from 'react-native-image-picker'
import * as ParseServer from 'src/services/parse/parse.service'
import theme from "src/theme/theme";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from 'react-native-simple-toast';
import { useDispatch } from "react-redux";
import { ADD_EXERCISE } from "src/store/reducers/exercises/exercises.action";
import { AppText } from "../util/app-text.component";
import { AppButton } from "../util/app-button.component";
import { View } from "react-native-animatable";
import { SCREEN_HEIGHT, SCREEN_WIDTH } from "src/assets/data/app.data";
import { Picker } from '@react-native-community/picker';
import { EXERCISE_CATEGORY } from "src/assets/data/cnv.data";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";
import { AppBadge } from "../util/app-badge.component";
import { useNavigation } from "@react-navigation/native";
import { INCREMENT_EXERCISE_COUNT } from "src/store/reducers/user/user.action";
import { translate } from "src/services/i18n.service";
import TagsModalComponent from "./tags-modal.component";
import { getColor } from "src/services/color.service";
import { AppInput } from "../util/app-input.component";
import { capitalizeFirstLetter } from "src/services/string.service";
import { BG_IMAGE } from "../../assets/data/app.data";
import { INCREMENT_TAG_COUNT } from "../../store/reducers/user/user.action";



const AddExerciseComponent: React.FC = () => {
    const navigation = useNavigation();
    const [isTagModalVisible, setIsTagModalVisible] = useState(false);
    const [isImageLoading, setIsImageLoading] = useState(false);
    const [isUploading, setIsUpLoading] = useState(false);
    const [exercise, setExercise] = useState<Exercise>({
        title: "",
        category: "",
        text: "",
        score: 0,
        duration: "",
        tags: [],
        isPremium: false,
    });
    const dispatch = useDispatch();

    const [height, setHeight] = useState(35);
    useEffect(() => {
        setHeight(40);
    }, []);

    function _onPicturePressed() {
        setIsImageLoading(true)
        const options = {
            maxWidth: 800,
            maxHeight: 600,
            quality: 0.8,
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            setIsImageLoading(false)
            if (response.error) {
                Toast.show(response.error, Toast.LONG)
            }
            else if (!response.didCancel) {
                if (response.fileSize < 3000000) { // compressed file smaller than 3 Mo
                    setExercise({ ...exercise, image: { filename: response.fileName, base64: response.data } })

                }
                else {
                    Toast.show("Même compressée, l'image est trop grande. Essayez une image moins lourde", Toast.LONG)
                }
            }
        })
    }

    function _submitExercise() {
        setIsUpLoading(true);
        ParseServer.addExercise(exercise)
            .then((exercice) => {
                Toast.show("Exercice ajouté!", Toast.LONG)
                dispatch({ type: ADD_EXERCISE, payload: exercise })
                dispatch({ type: INCREMENT_TAG_COUNT, payload: exercise.tags.length })
                dispatch({ type: INCREMENT_EXERCISE_COUNT })
                setExercise({
                    title: "",
                    category: "",
                    text: "",
                    score: 0,
                    duration: "",
                    tags: []
                });
                navigation.navigate('AppStack', { screen: 'Tabs', params: { screen: 'Exercises' } })
            })
            .catch(error => Toast.show(error.message, Toast.LONG))
            .finally(() => setIsUpLoading(false))
    }
    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

            <GenericHeaderComponent title="Ajouter un exercice" />
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                resetScrollToCoords={{ x: 0, y: 0 }}
                removeClippedSubviews={false}
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={[theme.panelContainer, { padding: 0, maxHeight: undefined, width: '90%', alignSelf: "center" }]}
            >

                <TouchableOpacity
                    style={styles.imagePanel}
                    onPress={_onPicturePressed}
                >
                    {
                        exercise.image ?
                            <Image
                                containerStyle={styles.imagePanel}
                                style={styles.image}
                                resizeMode="cover"
                                source={{ uri: `data:image;base64,${exercise.image.base64}` }} />
                            :
                            <>
                                <Icon
                                    name="image-plus"
                                    type="material-community"
                                    size={48}
                                />
                                <AppText style={{ color: 'black', paddingTop: 8 }}>Ajouter une image</AppText>
                            </>
                    }
                </TouchableOpacity>
                <View style={{ marginHorizontal: 8 }}>
                    <AppInput
                        placeholder="Titre"
                        maxLength={80}
                        onChangeText={text => setExercise({ ...exercise, title: text })}

                    />
                    <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                        <Picker
                            selectedValue={exercise.category}
                            style={{ width: '55%' }}
                            onValueChange={(itemValue, itemIndex) =>
                                setExercise({ ...exercise, category: itemValue })
                            }>
                            <Picker.Item label="Catégorie" value="" />
                            {
                                EXERCISE_CATEGORY.map(cat => <Picker.Item label={capitalizeFirstLetter(translate(cat))} value={cat} key={cat} />)
                            }
                        </Picker>
                        <Picker
                            selectedValue={exercise.duration}
                            style={{ width: '40%' }}
                            onValueChange={(itemValue, itemIndex) =>
                                setExercise({ ...exercise, duration: itemValue })
                            }>
                            <Picker.Item label="Durée" value="" />
                            {
                                [0, 1, 2, 3, 4, 5].map((i) => <Picker.Item key={i} label={translate(`duration${i}`)} value={i} />)
                            }
                        </Picker>
                    </View>
                    <Divider />
                    <TextInput
                        selectTextOnFocus={true}
                        placeholder="Ajouter une description(min 20 charactères)..."
                        multiline={true}
                        numberOfLines={5}
                        maxLength={800}
                        textAlignVertical="top"
                        onChangeText={text => setExercise({ ...exercise, text: text })}
                        style={styles.textInput}
                        returnKeyType="done"
                    />

                    <Text style={[theme.infoText, { color: 'black' }]}>TAGS</Text>
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>

                        <TagsModalComponent
                            isVisible={isTagModalVisible}
                            onHide={() => setIsTagModalVisible(false)}
                            onSubmit={(tags: string[]) => {
                                setIsTagModalVisible(false);
                                setExercise({ ...exercise, tags: tags });
                            }
                            }
                        />
                        {
                            exercise.tags.map((item) => <AppBadge
                                value={translate(item)}
                                key={item}
                                badgeStyle={[theme.badge, { backgroundColor: getColor(item) }]}
                                onPress={() => setExercise({ ...exercise, tags: exercise.tags.filter(tag => tag != item) })}
                            />
                            )
                        }
                        {exercise.tags.length < 5 &&
                            <AppBadge
                                value={capitalizeFirstLetter(translate("add"))}
                                badgeStyle={[theme.badge, { backgroundColor: 'transparent', borderColor: PRIMARY_COLOR }]}
                                textStyle={{ color: PRIMARY_COLOR }}
                                onPress={() => setIsTagModalVisible(true)}
                            />
                        }
                    </View>
                </View>
                <AppButton
                    title="Terminer"
                    onPress={_submitExercise}
                    loading={isUploading}
                    disabled={exercise.title.length < 3 || !exercise.category || exercise.text.length < 20 || !exercise.duration}
                    containerStyle={{ padding: 8, width: "80%", alignSelf: 'center' }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR }}
                />
            </KeyboardAwareScrollView>
        </ImageBackground>
    );
}
export default AddExerciseComponent


const styles = StyleSheet.create({
    imagePanel: {
        justifyContent: 'center',
        alignItems: 'center',
        height: SCREEN_HEIGHT / 4,
        backgroundColor: 'lightgrey',
        borderTopEndRadius: 15,
        borderTopStartRadius: 15
    },
    image: {
        width: SCREEN_WIDTH * 0.9,
        height: SCREEN_HEIGHT / 4,
    },
    textInput: {
        maxHeight: '60%',
    }
});



