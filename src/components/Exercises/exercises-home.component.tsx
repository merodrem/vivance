import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, LayoutAnimation, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Icon, Image } from "react-native-elements";
import { useSelector } from "react-redux";
import RNFetchBlob from 'rn-fetch-blob';
import { DEFAULT_EXERCISE_FILTER, SCREEN_HEIGHT, SCREEN_WIDTH } from "src/assets/data/app.data";
import { EXERCISE_CATEGORY } from "src/assets/data/cnv.data";
import { Exercise } from "src/models/exercise.model";
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { capitalizeFirstLetter } from "src/services/string.service";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { UserState } from "../../models/user.model";
import { AppText } from "../util/app-text.component";

const filters = Object.freeze(["new", "short", "top", "official"] as const)

const CARD_WIDTH = SCREEN_WIDTH / 2;
const CARD_HEIGHT = SCREEN_HEIGHT / 6;

//TODO i18n
const tempTranslation = {
    "new": "NOUVEAU",
    "short": "PETITS EXERCICES",
    "top": "MIEUX NOTÉS",
    "official": "OFFICIELS"
}


const ExerciseCardComponent: React.FC = (props: Exercise) => {
    const navigation = useNavigation();
    const [imageLoading, setImageLoading] = useState(false);
    const [image, setImage] = useState(undefined);
    useEffect(() => {
        if (props.image) {
            setImageLoading(true);
            RNFetchBlob.config({ fileCache: true }).fetch('GET', props.image.url)
                .then(result => result.readFile('base64')
                    .then(base64Image => setImage(base64Image))
                )
                .catch(error => console.log(error))
                .finally(() => setImageLoading(false));
        }
    }, []);

    return (
        <TouchableOpacity
            style={styles.exerciseCard}
            disabled={props.disabled}
            onPress={() => navigation.navigate("Exercise", { ...props, image: props.image && { filename: props.image.name, base64: image } })}
        >
            <View style={{ flexDirection: "row", alignItems: "center", margin: 8, zIndex: 1 }}>
                <AppText style={{ fontWeight: "bold", textShadowRadius: 10 }}>{props.score} </AppText>
                <Icon
                    name="trophy"
                    type="font-awesome"
                    size={14}
                    color="skyblue"
                />
            </View>
            <Image
                style={[styles.image, props.disabled &&{opacity:0.3}]}
                resizeMode="cover"
                containerStyle={{ borderRadius: 20, position: "absolute" }}
                source={(props.image && image) ? { uri: `data:image;base64,${image}` } : require('src/assets/img/exerciseDefault.png')}
            />
            {
                imageLoading &&
                <ActivityIndicator
                    size={24}
                    color={PRIMARY_COLOR}
                />

            }
            {
                props.disabled &&
                <View style={{ position: 'absolute', top: 10, left: 0, right: 0, justifyContent: 'flex-end', alignItems: 'center' }}>
                    <AppText style={{ color: "white", paddingBottom: 6, paddingLeft: 6, textShadowRadius: 10 }}>(PREMIUM)</AppText>
                </View>
            }
            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-end', alignItems: 'center' }}>
                <AppText style={{ color: "white", paddingBottom: 6, paddingLeft: 6, textShadowRadius: 10 }}>{props.title.length > 50 ? props.title.slice(0, 50) + '...' : props.title}</AppText>
            </View>
        </TouchableOpacity>
    );
}
const ExerciseHomeComponent: React.FC = () => {
    const [overview, setOverview] = useState({});
    const [isLoading, setLoading] = useState(true);
    const user: UserState = useSelector(state => state.user);

    useEffect(() => {
        LayoutAnimation.easeInEaseOut();
        ParseServer.getExercisesOverview(DEFAULT_EXERCISE_FILTER)
            .then(exercises => setOverview(exercises))
            .finally(() => setLoading(false))
    }, []);
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false} >
            <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                {
                    filters.map(f =>
                        <TouchableOpacity
                            key={f}
                            onPress={() => {
                                setLoading(true)
                                ParseServer.getExercisesOverview(f)
                                    .then(exercises => setOverview(exercises))
                                    .finally(() => setLoading(false))
                            }
                            }
                            style={[theme.darkPanel, { marginTop: 0 }]}>
                            <AppText>{tempTranslation[f]}</AppText>
                        </TouchableOpacity>
                    )
                }
            </ScrollView>
            <ScrollView>
                {
                    EXERCISE_CATEGORY.map(cat =>
                        <View
                            key={cat}
                        >
                            <AppText h4 style={{ paddingLeft: 8, marginTop: 16, marginBottom: 4, fontWeight: "bold" }}>{capitalizeFirstLetter(translate(cat))}</AppText>
                            {isLoading ?
                                <View style={{ height: CARD_HEIGHT, alignItems: "center", justifyContent: "center" }}>
                                    <ActivityIndicator size={30} color="white" />
                                </View>
                                :
                                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                    {
                                        (overview[cat] && overview[cat].length > 0) ?
                                            overview[cat].map((exercise: Exercise) =>
                                                <ExerciseCardComponent
                                                    key={exercise.objectId}
                                                    {...exercise}
                                                    disabled={exercise.isPremium && !user.isPremium}
                                                />
                                            )
                                            :
                                            <AppText style={{ marginBottom: 8, paddingLeft: 16, }}>Aucun exercice pour cette catégorie</AppText>
                                    }
                                    {
                                        (overview[cat] && overview[cat].length == 5) &&
                                        <TouchableOpacity
                                            style={[styles.exerciseCard, { justifyContent: "center", alignItems: "center" }]}
                                            onPress={() => navigation.navigate("ExerciseList", { category: cat })}
                                        >
                                            <Icon
                                                name="more-horizontal"
                                                type="feather"
                                                size={64}
                                                color="white"
                                            />
                                            <AppText>Voir tout</AppText>
                                        </TouchableOpacity>
                                    }

                                </ScrollView>
                            }
                        </View>
                    )
                }
            </ScrollView>
        </ScrollView >
    );
}
export default ExerciseHomeComponent

const styles = StyleSheet.create({
    image: {
        width: CARD_WIDTH,
        height: CARD_HEIGHT,
    },
    textInput: {
        maxHeight: '60%',
    },
    exerciseCard: {
        width: CARD_WIDTH,
        height: CARD_HEIGHT,
        backgroundColor: PRIMARY_COLOR,
        marginHorizontal: 8,
        borderRadius: 20
    }
});