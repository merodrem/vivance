
import React, { useState } from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from 'src/assets/data/app.data';
import { ModalBaseProps } from 'src/models/modalBaseProps.model';
import { getColor } from 'src/services/color.service';
import { translate } from 'src/services/i18n.service';
import { PRIMARY_COLOR } from 'src/theme/variables';
import { EMOTION_CATEGORIES, NEED_CATEGORIES } from '../../assets/data/cnv.data';
import { AppButton } from '../util/app-button.component';
import { AppText } from '../util/app-text.component';
import { ButtonCheckComponent } from '../util/button-check.component';

// function popPushInArray<K>([...arr]: K[], elem: K) {
//     const result = arr.includes(elem) ? arr.filter(val => val != elem) : (arr.push(elem), arr);
//     return result;
// }

type Props = {
    onSubmit: (a: string[]) => void,
} & ModalBaseProps

function TagsModalComponent(props: Props) {
    const [tags, setTags] = useState<string[]>([])
    return (
        <Modal deviceWidth={SCREEN_WIDTH}
            isVisible={props.isVisible}
            deviceHeight={SCREEN_HEIGHT}
            hideModalContentWhileAnimating={true}
            onBackdropPress={props.onHide}
            onBackButtonPress={props.onHide}>
            <View style={{}}>
                <AppText h4 style={{fontWeight:"bold",marginBottom:4, marginTop:8}}>Tags d'émotions</AppText>
                <View style={{ flexDirection: "row", flexWrap: "wrap", width:"90%", alignSelf:"center" }}>

                    {
                        EMOTION_CATEGORIES.map((item) => <ButtonCheckComponent
                            key={item}
                            color={getColor(item)}
                            label={translate(item)}
                            containerStyle={{ width: "50%", padding: 4 }
                            }
                            isChecked={tags.includes(item)}
                            onPress={() => {
                                if (tags.includes(item)) {
                                    setTags(tags.filter(tag => tag !== item))
                                }
                                else if (tags.length < 5) {
                                    setTags([...tags, item])
                                }
                            }}
                        />
                        )
                    }
                </View>
                <AppText h4 style={{fontWeight:"bold",marginBottom:4, marginTop:8}}>Tags de besoins</AppText>
                <View style={{ flexDirection: "row", flexWrap: "wrap", width:"90%", alignSelf:"center" }}>

                    {
                        NEED_CATEGORIES.map((item) => <ButtonCheckComponent
                            key={item}
                            color={getColor(item)}
                            label={translate(item)}
                            containerStyle={{ width: "50%", padding: 4 }
                            }
                            isChecked={tags.includes(item)}
                            onPress={() => {
                                if (tags.includes(item)) {
                                    setTags(tags.filter(tag => tag !== item))
                                }
                                else if (tags.length < 5) {
                                    setTags([...tags, item])
                                }
                            }}
                        />
                        )
                    }
                </View>
                <AppButton
                    title={translate("validate")}
                    containerStyle={{ margin: 8, alignSelf:"center" }}
                    buttonStyle={{ backgroundColor: PRIMARY_COLOR, borderRadius: 25, paddingHorizontal: 16 }}
                    onPress={() => props.onSubmit(tags)}
                />
            </View>
        </Modal>
    );
}



export default TagsModalComponent