import { useNavigation, useRoute } from "@react-navigation/native";
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ImageBackground, StyleSheet, TouchableOpacity, View } from "react-native";
import { Divider, Icon, Image } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from 'react-native-simple-toast';
import {
    shareOnFacebook
} from 'react-native-social-share';
import { useDispatch, useSelector } from "react-redux";
import RNFetchBlob from "rn-fetch-blob";
import { BG_IMAGE, SCREEN_HEIGHT } from "src/assets/data/app.data";
import { Note } from "src/models/note.model";
import { UserState } from "src/models/user.model";
import { getColor } from "src/services/color.service";
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import { ADD_NOTE } from "src/store/reducers/notes/notes.action";
import theme from "src/theme/theme";
import { PRIMARY_COLOR } from "src/theme/variables";
import { Exercise } from "../../models/exercise.model";
import { TOGGLE_FAVS } from "../../store/reducers/Favorites/favorites.action";
import { SET_USER } from "../../store/reducers/user/user.action";
import { AppBadge } from "../util/app-badge.component";
import { AppInput } from "../util/app-input.component";
import { AppText } from "../util/app-text.component";
import GenericHeaderComponent from "../util/generic-header.component";
import { NoteCardComponent } from "../util/note-card.component";


function ExerciseComponent() {
    const route = useRoute();
    const [note, setNote] = useState("");
    const [noteUploading, setNoteUploading] = useState(false)
    const [showNoteInput, setShowNoteInput] = useState(false)
    const [favoriteLoading, setFavoriteLoading] = useState(false)
    const [downVoteLoading, setDownVoteLoading] = useState(false)
    const [upVoteLoading, setUpVoteLoading] = useState(false)
    const [exercise, setExercise] = useState<Exercise>({ ...route.params })
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const notes: Note[] = useSelector(state => state.notes)
        .filter(note => note.exerciseId == exercise.objectId)
        .sort(function (a, b) {
            return a.timestamp < b.timestamp;
        });
    const favorites = useSelector(state => state.favs);
    
    const isInFavorites = favorites.find(ex => ex.objectId == exercise.objectId) !== undefined;

    const user: UserState = useSelector(state => state.user);
    const [isUpVoted, setIsUpvoted] = useState(user.upVotedExercises.includes(exercise.objectId))
    const [isDownVoted, setIsDownvoted] = useState(user.downVotedExercises.includes(exercise.objectId))

    useEffect(() => {
        if (exercise.originalPoster && exercise.originalPoster?.avatar?.url) {
            if (exercise.originalPoster.objectId != user.objectId) {
                RNFetchBlob.config({ fileCache: true }).fetch('GET', exercise.originalPoster?.avatar?.url)
                    .then(image => image.readFile('base64')
                        ?.then(avatar => {
                            setExercise({ ...exercise, originalPoster: { ...exercise.originalPoster, avatar: avatar } })
                        }
                        )
                    )
            }
            else {
                setExercise({ ...exercise, originalPoster: { ...exercise.originalPoster, avatar: user.avatar } })
            }
        }
    }, [])

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>

            <GenericHeaderComponent
                title={exercise.title}
            />
            <View style={{ flex: 1, alignSelf: "center", justifyContent: "center", width: "90%", }}>
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                >
                    <View style={[theme.panelContainer, { padding: 0 }]}>
                        {exercise.image &&
                            <Image
                                style={styles.image}
                                containerStyle={{ borderTopLeftRadius: 15, borderTopRightRadius: 15 }}
                                resizeMode="cover"
                                source={{ uri: `data:image;base64,${exercise.image.base64}` }} />
                        }
                        <View style={{ flexDirection: "row", justifyContent: "space-between", flexWrap: "wrap" }}>

                            <AppText style={[theme.panelTitle, { fontWeight: "bold" }]}>
                                {translate(exercise.category)}
                            </AppText>
                            <AppText style={[theme.panelTitle, { fontWeight: "bold" }]}>
                                Durée: {translate(`duration${exercise.duration}`)}
                            </AppText>
                        </View>
                        <Divider style={{ marginBottom: 8, backgroundColor: PRIMARY_COLOR }} />
                        <AppText style={{ color: "black", paddingLeft: 8, paddingBottom: 8 }}>
                            {exercise.text}
                        </AppText>
                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                            {
                                exercise.tags.map((item) => <AppBadge
                                    key={item}
                                    badgeStyle={{ backgroundColor: getColor(item), marginHorizontal: 4, paddingHorizontal: 8, paddingVertical: 10 }}
                                    textStyle={{ fontSize: 14, paddingVertical: 8 }}
                                    value={translate(item)}
                                />
                                )
                            }
                        </View>
                        <View style={{ flexDirection: "row", alignItems: "center", margin: 12 }}>
                            <Image
                                style={styles.avatar}
                                source={exercise.originalPoster?.avatar ? { uri: `data:image;base64,${exercise.originalPoster.avatar}` } : require('../../assets/img/default_avatar.png')}
                            />
                            <TouchableOpacity
                                disabled={!exercise.originalPoster}
                                onPress={async () => navigation.navigate("Profile", { profile: exercise.originalPoster })
                                }
                                style={{ alignItems: "center" }}
                            >
                                <AppText style={[theme.commonText, { textAlign: "left", color: "darkblue" }]}>
                                    {exercise.originalPoster?.username ?? "utilisateur supprimé"}
                                </AppText>
                                {exercise.originalPoster &&
                                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                                        <AppText style={{ color: "black", fontWeight: "bold" }}>{exercise.originalPoster.reputation}</AppText>
                                        <Icon
                                            name="star"
                                            iconStyle={{ marginRight: 4 }}
                                            size={16}
                                            color={PRIMARY_COLOR}
                                        />
                                        {
                                            exercise.originalPoster.goldenBadges > 0 &&
                                            <>
                                                <AppText style={{ color: "black" }}>{exercise.originalPoster.goldenBadges}</AppText>
                                                <Icon
                                                    name="trophy"
                                                    type="font-awesome"
                                                    size={16}
                                                    color="gold"
                                                    iconStyle={{ marginRight: 4 }}
                                                />
                                            </>

                                        }
                                        {
                                            exercise.originalPoster.silverBadges > 0 &&
                                            <>
                                                <AppText style={{ color: "black" }}>{exercise.originalPoster.silverBadges}</AppText>
                                                <Icon
                                                    name="trophy"
                                                    type="font-awesome"
                                                    size={16}
                                                    color="grey"
                                                    iconStyle={{ marginRight: 4 }}
                                                />
                                            </>
                                        }
                                        {
                                            exercise.originalPoster.bronzeBadges > 0 &&
                                            <>
                                                <AppText style={{ color: "black" }}>{exercise.originalPoster.bronzeBadges}</AppText>
                                                <Icon
                                                    name="trophy"
                                                    type="font-awesome"
                                                    size={16}
                                                    color="orange"
                                                />
                                            </>
                                        }
                                    </View>
                                }
                            </TouchableOpacity>
                        </View>

                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <View style={{ flexDirection: 'row', alignItems: "center" }}>
                            <AppText style={[theme.headerText, { color: "white" }]}>{exercise.score}</AppText>
                            <Icon
                                name="trophy"
                                type="font-awesome"
                                size={16}
                                style={{ marginRight: 8 }}
                                color="white"
                            />
                            <TouchableOpacity
                                style={{ marginHorizontal: 2 }}
                                disabled={exercise.score == 0}
                                onPress={() => {
                                    setDownVoteLoading(true)
                                    if (isDownVoted) {
                                        ParseServer.deleteExerciseVote(-1, exercise.objectId)
                                            .then((score) => {
                                                setIsDownvoted(false);
                                                dispatch({ type: SET_USER, payload: { downVotedExercises: user.downVotedExercises.filter(ex => ex != exercise.objectId) } })
                                                setExercise({ ...exercise, score: score })
                                            }
                                            )
                                            .catch(error => console.log(error))
                                            .finally(() => setDownVoteLoading(false))
                                    }
                                    else {
                                        ParseServer.voteForExercise(-1, exercise.objectId)
                                            .then(score => {
                                                setIsDownvoted(true);
                                                setIsUpvoted(false);
                                                dispatch({ type: SET_USER, payload: { downVotedExercises: [...user.downVotedExercises, exercise.objectId] } })
                                                setExercise({ ...exercise, score: score })
                                            }
                                            )
                                            .catch(error => console.log(error))
                                            .finally(() => setDownVoteLoading(false))
                                    }
                                }
                                }
                            >
                                {
                                    downVoteLoading ?
                                        <ActivityIndicator color='white' />
                                        :
                                        <Icon
                                            name="arrow-down"
                                            type="feather"
                                            size={24}
                                            disabled={exercise.score == 0}
                                            disabledStyle={{ backgroundColor: 'transparent' }}
                                            style={{ marginRight: 4 }}
                                            color={isDownVoted ? 'lightsalmon' : (exercise.score == 0) ? 'lightgrey' : 'white'}
                                        />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ marginHorizontal: 2 }}
                                onPress={() => {
                                    setUpVoteLoading(true);
                                    if (isUpVoted) {
                                        ParseServer.deleteExerciseVote(1, exercise.objectId)
                                            .then((score) => {
                                                dispatch({ type: SET_USER, payload: { upVotedExercises: user.upVotedExercises.filter(ex => ex != exercise.objectId) } })
                                                setIsUpvoted(false)
                                                setExercise({ ...exercise, score: score })
                                            }
                                            )
                                            .catch(error => console.log(error))
                                            .finally(() => setUpVoteLoading(false))
                                    }
                                    else {
                                        ParseServer.voteForExercise(1, exercise.objectId)
                                            .then(score => {
                                                setIsDownvoted(false);
                                                dispatch({ type: SET_USER, payload: { upVotedExercises: [...user.upVotedExercises, exercise.objectId] } })
                                                setIsUpvoted(true);
                                                setExercise({ ...exercise, score: score })
                                            }
                                            )
                                            .catch(error => console.log(error))
                                            .finally(() => setUpVoteLoading(false))
                                    }
                                }
                                }
                            >
                                {
                                    upVoteLoading ?
                                        <ActivityIndicator color='white' />
                                        :
                                        <Icon
                                            name="arrow-up"
                                            type="feather"
                                            size={24}
                                            color={isUpVoted ? "limegreen" : 'white'}
                                        />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                onPress={() => {
                                    setFavoriteLoading(true)

                                    if (!isInFavorites)
                                        ParseServer.addFavoriteExercise(exercise.objectId)
                                            .then(() => {
                                                Toast.show("Ajouté aux favoris!", Toast.LONG)
                                                dispatch({ type: TOGGLE_FAVS, payload: exercise });
                                            }
                                            )
                                            .catch(error =>
                                                Toast.show("Impossible de sauvegarder l'exercice dans les favoris: " + error.message, Toast.LONG)
                                            )
                                            .finally(() => setFavoriteLoading(false))
                                    else
                                        ParseServer.removeFavoriteExercise(exercise.objectId)
                                            .then(() => {
                                                Toast.show("Supprimé des favoris!", Toast.LONG)
                                                dispatch({ type: TOGGLE_FAVS, payload: exercise });
                                            }
                                            )
                                            .catch(error =>
                                                Toast.show("Impossible de supprimer l'exercice des favoris: " + error.message, Toast.LONG)
                                            )
                                            .finally(() => setFavoriteLoading(false))
                                }
                                }
                            >
                                {
                                    favoriteLoading ?
                                        <ActivityIndicator size={24} color="white" />
                                        :
                                        isInFavorites ?
                                            <Icon
                                                name="heart"
                                                type="antdesign"
                                                style={{ marginRight: 4 }}
                                                size={24}
                                                color="white"
                                            />
                                            :
                                            <Icon
                                                name="hearto"
                                                type="antdesign"
                                                size={24}
                                                color="white"
                                            />
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{ marginHorizontal: 8 }}
                            >
                                <Icon
                                    name="share"
                                    type="fontisto"
                                    size={24}
                                    color="white"
                                    onPress={() => shareOnFacebook({
                                        'text': exercise.title,
                                        'link': 'https://vivance-app.fr/application-gerer-ses-emotions',
                                    },
                                        (results) => {
                                            console.log(results);
                                        }
                                    )}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 16 }}>
                        <AppText h4 style={[theme.commonText, { textAlign: "left", color: "white" }]}>
                            Notes
                            </AppText>
                        {showNoteInput ?
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    onPress={() => setShowNoteInput(false)}
                                >
                                    <Icon
                                        name="circle-with-cross"
                                        type="entypo"
                                        style={{ marginRight: 8 }}
                                        size={24}
                                        color="lightsalmon"
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{ marginHorizontal: 8 }}
                                    onPress={() => {
                                        setNoteUploading(true);
                                        if (exercise.objectId) {
                                            ParseServer.addNote({ exerciseId: exercise.objectId, content: note, tags: [exercise.title], type: 'exercise', timestamp: new Date().toISOString() })
                                                .then(note => {
                                                    setShowNoteInput(false)
                                                    dispatch({ type: ADD_NOTE, payload: note })
                                                    Toast.show("Note ajoutée!", Toast.LONG)
                                                })
                                                .catch(error =>
                                                    Toast.show("Impossible d'uploader la note. " + error.message, Toast.LONG)
                                                )
                                                .finally(() => setNoteUploading(false))
                                        }
                                        else {
                                            Toast.show("cet exercice est introuvable dans la base de donnée. Veuillez relancer l'application", Toast.LONG)
                                        }
                                    }
                                    }
                                >
                                    {
                                        noteUploading ?
                                            <ActivityIndicator color="white" />
                                            :
                                            <Icon
                                                name="check-circle"
                                                type="font-awesome"
                                                size={24}
                                                color="white"
                                            />
                                    }
                                </TouchableOpacity>
                            </View>
                            :
                            <TouchableOpacity
                                style={{ marginHorizontal: 8 }}
                                onPress={() => setShowNoteInput(true)}
                            >
                                <Icon
                                    name="message-plus"
                                    type="material-community"
                                    size={24}
                                    color="white"
                                />
                            </TouchableOpacity>
                        }
                    </View>
                    {showNoteInput &&
                        <AppInput
                            multiline={true}
                            numberOfLines={3}
                            textAlignVertical="top"
                            inputContainerStyle={theme.darkInputContainer}
                            inputStyle={theme.darkInput}
                            maxLength={500}
                            onChangeText={text => setNote(text)}
                            placeholderTextColor="white"
                            placeholder={"Page de notes..."}
                        />}

                    {
                        notes.length > 0 ?
                            notes.map(note =>
                                <NoteCardComponent {...note}
                                    key={note.objectId}
                                    hideTags
                                />
                            )
                            :
                            <AppText style={[theme.commonText, { textAlign: "left", color: "white" }]}>
                                Aucune note ajoutée
                        </AppText>
                    }
                </KeyboardAwareScrollView>
            </View>
        </ImageBackground>

    );
}
export default ExerciseComponent

const styles = StyleSheet.create({
    image: {
        width: "100%",
        height: SCREEN_HEIGHT / 4
    },
    avatar: {
        width: 48,
        height: 48,
        borderRadius: 50,
        marginRight: 8,
        borderColor: '#9B9B9B',
    }
});
