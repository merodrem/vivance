import { useRoute } from "@react-navigation/native";
import React, { useEffect, useRef, useState } from 'react';
import { ActivityIndicator, ImageBackground, StyleSheet } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { useSelector } from "react-redux";
import { BG_IMAGE, DEFAULT_EXERCISE_FILTER, SCREEN_HEIGHT } from "src/assets/data/app.data";
import { Exercise } from "src/models/exercise.model";
import { translate } from "src/services/i18n.service";
import * as ParseServer from 'src/services/parse/parse.service';
import theme from "src/theme/theme";
import { UserState } from "../../models/user.model";
import { ExerciseListItemComponent } from "../util/exercise-list-item.component";
import GenericHeaderComponent from "../util/generic-header.component";




function ExerciseListComponent() {
    const route = useRoute();
    const category = route.params.category;
    const page = useRef(0);
    const [isLoading, setLoading] = useState(true);
    const [exercises, setExercises] = useState([] as Exercise[]);
    const user: UserState = useSelector(state => state.user);

    useEffect(() => {
        ParseServer.getExercises(DEFAULT_EXERCISE_FILTER, category, page.current)
            .then(exercises => setExercises(exercises))
            .finally(() => setLoading(false))
    }, []);

    return (
        <ImageBackground source={BG_IMAGE} style={theme.bgImage}>
            <GenericHeaderComponent title={translate(category)} />
            <FlatList
                data={exercises}
                ListFooterComponent={isLoading && <ActivityIndicator />}
                keyExtractor={item => item.objectId}
                renderItem={({ item }) =>
                    <ExerciseListItemComponent {...item}
                    disabled={item.isPremium && !user.isPremium}
                    />
                }
            />
        </ImageBackground>
    );
}
export default ExerciseListComponent

const styles = StyleSheet.create({
    image: {
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        height: SCREEN_HEIGHT / 8,
        width: "30%"
    }
});